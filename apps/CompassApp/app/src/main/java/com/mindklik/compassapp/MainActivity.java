package com.mindklik.compassapp;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private Float azimuthAngle;

    private SensorManager compassSensorManager;
    private Sensor accelerometer;
    private Sensor magnetometer;

    private TextView degreesText;
    private ImageView compassImage;

    private float[] accelReads;
    private float[] magReads;
    private float currentDeg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        compassSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        accelerometer = compassSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magnetometer = compassSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        degreesText = (TextView) findViewById(R.id.degreesText);
        compassImage = (ImageView) findViewById(R.id.compasImage);

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            accelReads = event.values;
        }
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            magReads = event.values;
        }

        if (accelReads != null && magReads != null) {
            float R[] = new float[9];
            float I[] = new float[9];
            boolean isRead = SensorManager.getRotationMatrix(R, I, accelReads, magReads);

            if (isRead) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                azimuthAngle = orientation[0];
                float degrees = (float) ((azimuthAngle * 180.0f) / Math.PI);
                int degreesInt = Math.round(degrees);
                degreesText.setText(Integer.toString(degreesInt) + (char) 0x00B0 + " to north.");

                RotateAnimation rotateAnimation = new RotateAnimation(currentDeg, -degreesInt,
                        Animation.RELATIVE_TO_SELF, 0.5f,
                        Animation.RELATIVE_TO_SELF, 0.5f);
                rotateAnimation.setDuration(100);
                rotateAnimation.setFillAfter(true);

                compassImage.startAnimation(rotateAnimation);
                currentDeg = -degreesInt;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        compassSensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
        compassSensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onPause() {
        super.onPause();
        compassSensorManager.unregisterListener(this);
    }
}
