package com.mindklik.diceroller;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    Random random;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        random = new Random();

        Button rollButton = getButton(R.id.rollButton);

        setButtonListener(rollButton);
    }

    private Button getButton(int id) {

        return (Button) findViewById(id);
    }

    private void setButtonListener(Button button) {

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int diceRoll = random.nextInt(6) + 1;

                ImageView image = (ImageView) findViewById(R.id.diceImage);

                switch (diceRoll) {
                    case 1:
                        image.setImageResource(R.drawable.roll1);
                        break;
                    case 2:
                        image.setImageResource(R.drawable.roll2);
                        break;
                    case 3:
                        image.setImageResource(R.drawable.roll3);
                        break;
                    case 4:
                        image.setImageResource(R.drawable.roll4);
                        break;
                    case 5:
                        image.setImageResource(R.drawable.roll5);
                        break;
                    case 6:
                        image.setImageResource(R.drawable.roll6);
                        break;
                }
            }
        });
    }
}
