package com.mindklik.sosmessage;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private LocationManager locationManager;
    private GPSSReceiver gpsReceiver;

    private double latitudeGPS;
    private double longituteGPS;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        gpsReceiver = new GPSSReceiver();

        ButtonListener();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1);

        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.SEND_SMS},
                    1);

        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1.0f, gpsReceiver);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(getApplicationContext(), "granted", Toast.LENGTH_SHORT).show();

                } else {

                    Toast.makeText(getApplicationContext(), "not granted", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            case 2: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(getApplicationContext(), "granted", Toast.LENGTH_SHORT).show();

                } else {

                    Toast.makeText(getApplicationContext(), "not granted", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }

    }

    private void ButtonListener() {

        Button sosButton = (Button) findViewById(R.id.SOSButton);

        sosButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String phoneNumber = "793919322";
                String message = "Yo\nmy longitute: " + Double.toString(longituteGPS) + "\nmy latitude: " + Double.toString(latitudeGPS);
                try {
                    SmsManager.getDefault().sendTextMessage(phoneNumber, null, message, null, null);
                    Toast.makeText(getApplicationContext(), "SOS msg sent", Toast.LENGTH_SHORT).show();
                    System.out.println("yesyesyes");
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "could send the msg", Toast.LENGTH_SHORT);
                    System.out.println("nonono");
                    e.printStackTrace();

                }
            }
        });
    }

    public class GPSSReceiver implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {

            if (location != null) {
                latitudeGPS = location.getLatitude();
                longituteGPS = location.getLongitude();
                Toast.makeText(getApplicationContext(), "Ready to send", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Not ready to send", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

            Toast.makeText(getApplicationContext(), "GPS ENABLED", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onProviderDisabled(String provider) {
            Toast.makeText(getApplicationContext(), "GPS DISABLED", Toast.LENGTH_LONG).show();
        }
    }

}
