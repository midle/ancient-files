﻿using UnityEngine;
using UnityEngine.UI;

public class ShopController : MonoBehaviour
{

    public Text pointsText;

    private Vector2 inputUp, inputDown;

    [SerializeField]
    private float checkSwipeLength;

    public Image i1, i2, i3, i4;


    const int chargeRate = 15;

    private void Start()
    {
        pointsText.text = "Points: " + PlayerPrefs.GetInt("points");
        ChangeColor(11);
    }

    private void Update()
    {
        CheckSwipes();
    }

    private void CheckSwipes()
    {
        //compile only for the right platform
#if (UNITY_IOS || UNITY_ANDROID || UNITY_WP8)
        TouchController();
#else
        MouseController();
        DanceMatController();
#endif
    }

    private void MouseController()
    {
        if (Input.GetMouseButtonDown(0))
        {
            inputDown = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }

        if (Input.GetMouseButtonUp(0))
        {
            inputUp = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            GlobalControl();
        }
    }
    private void DanceMatController()
    {
        if (Input.GetKeyDown(KeyCode.Joystick1Button1))//right
        {
            ActionOnTriangle(2);
        }
        else if (Input.GetKeyDown(KeyCode.Joystick1Button3))//left
        {
            ActionOnTriangle(0);
        }
        else if (Input.GetKeyDown(KeyCode.Joystick1Button4))//up
        {
            ActionOnTriangle(1);
        }
        else if (Input.GetKeyDown(KeyCode.Joystick1Button0))//down
        {
            ActionOnTriangle(3);
        }
    }

    private void TouchController()
    {
        if (Input.touchCount > 0)
        {
            Touch myTouch = Input.touches[0];

            if (myTouch.phase == TouchPhase.Began)
            {
                inputDown = new Vector2(myTouch.position.x, myTouch.position.y);
            }

            if (myTouch.phase == TouchPhase.Ended)
            {
                inputUp = new Vector2(myTouch.position.x, myTouch.position.y);

                GlobalControl();
            }
        }
    }

    private void GlobalControl()
    {
        float tempSwipeX = Mathf.Abs(inputDown.x - inputUp.x);
        float tempSwipeY = Mathf.Abs(inputDown.y - inputUp.y);

        Vector3 swipe = Camera.main.ScreenToWorldPoint(inputUp) - Camera.main.ScreenToWorldPoint(inputDown);
        swipe.z = 0;

        if (swipe.magnitude > checkSwipeLength)
        {
            if (inputDown.x < inputUp.x && tempSwipeX > tempSwipeY)//right
            {
                ActionOnTriangle(2);
            }
            else if (inputDown.x > inputUp.x && tempSwipeX > tempSwipeY)//left
            {
                ActionOnTriangle(0);
            }
            else if (inputDown.y < inputUp.y && tempSwipeX < tempSwipeY)//up
            {
                ActionOnTriangle(1);
            }
            else if (inputDown.y > inputUp.y && tempSwipeX < tempSwipeY)//down
            {
                ActionOnTriangle(3);
            }
        }
    }

    private void ActionOnTriangle(int triangle)
    {
        if (PlayerPrefs.GetInt("points") >= chargeRate)
        {
            switch (triangle)
            {
                case 0:
                    PlayerPrefs.SetFloat("triangle1R", Random.Range(0.35f, 1));
                    PlayerPrefs.SetFloat("triangle1G", 0);
                    PlayerPrefs.SetFloat("triangle1B", 0);
                    ChargePoints();
                    ChangeColor(triangle + 1);
                    break;
                case 1:
                    PlayerPrefs.SetFloat("triangle2R", 0);
                    PlayerPrefs.SetFloat("triangle2G", Random.Range(0.35f, 1));
                    PlayerPrefs.SetFloat("triangle2B", 0);
                    ChargePoints();
                    ChangeColor(triangle + 1);
                    break;
                case 2:
                    PlayerPrefs.SetFloat("triangle3R", 0);
                    PlayerPrefs.SetFloat("triangle3G", 0);
                    PlayerPrefs.SetFloat("triangle3B", Random.Range(0.35f, 1));
                    ChargePoints();
                    ChangeColor(triangle + 1);
                    break;
                case 3:
                    float x = Random.Range(0.35f, 1);
                    PlayerPrefs.SetFloat("triangle4R", x);
                    PlayerPrefs.SetFloat("triangle4G", (x / 4 * 3));
                    PlayerPrefs.SetFloat("triangle4B", 0);
                    ChargePoints();
                    ChangeColor(triangle + 1);
                    break;
            }
        }
    }

    private void ChangeColor(int triangle)
    {
        switch (triangle)
        {
            case (1):
                i1.color = new Color(
                    PlayerPrefs.GetFloat("triangle1R"),
                    PlayerPrefs.GetFloat("triangle1G"),
                    PlayerPrefs.GetFloat("triangle1B"));
                break;
            case (2):
                i2.color = new Color(
                    PlayerPrefs.GetFloat("triangle2R"),
                    PlayerPrefs.GetFloat("triangle2G"),
                    PlayerPrefs.GetFloat("triangle2B"));
                break;
            case (3):
                i3.color = new Color(
                     PlayerPrefs.GetFloat("triangle3R"),
                     PlayerPrefs.GetFloat("triangle3G"),
                     PlayerPrefs.GetFloat("triangle3B"));
                break;
            case (4):
                i4.color = new Color(
                    PlayerPrefs.GetFloat("triangle4R"),
                    PlayerPrefs.GetFloat("triangle4G"),
                    PlayerPrefs.GetFloat("triangle4B"));
                break;
            case 11:
                i1.color = new Color(
                       PlayerPrefs.GetFloat("triangle1R"),
                       PlayerPrefs.GetFloat("triangle1G"),
                       PlayerPrefs.GetFloat("triangle1B"));
                i2.color = new Color(
                       PlayerPrefs.GetFloat("triangle2R"),
                       PlayerPrefs.GetFloat("triangle2G"),
                       PlayerPrefs.GetFloat("triangle2B"));
                i3.color = new Color(
                         PlayerPrefs.GetFloat("triangle3R"),
                         PlayerPrefs.GetFloat("triangle3G"),
                         PlayerPrefs.GetFloat("triangle3B"));
                i4.color = new Color(
                    PlayerPrefs.GetFloat("triangle4R"),
                    PlayerPrefs.GetFloat("triangle4G"),
                    PlayerPrefs.GetFloat("triangle4B"));
                break;
        }
    }

    private void ChargePoints()
    {
        PlayerPrefs.SetInt("points", PlayerPrefs.GetInt("points") - chargeRate);
        pointsText.text = "Points: " + PlayerPrefs.GetInt("points");
    }
}
