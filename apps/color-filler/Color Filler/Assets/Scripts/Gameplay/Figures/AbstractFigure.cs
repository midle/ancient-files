﻿using UnityEngine;
using UnityEngine.UI;

public abstract class AbstractFigure : MonoBehaviour, IColorable
{

    public GameObject gameController;
    protected GameOverScript gameOverScript;

    protected Image figureImage;
    protected float fillageLevel;
    //public props
    public Image FigureImage
    {
        get
        {
            return figureImage;
        }
    }
    public float FillageLevel
    {
        get
        {
            return fillageLevel;
        }
    }

    protected float area;

    void Start()
    {
        figureImage = GetComponent<Image>();
        gameOverScript = gameController.GetComponent<GameOverScript>();

        SetColor();
    }


    public void MaxedOut()
    {
        gameOverScript.GameOver();
    }


    //depends on figure
    public abstract void OnFill(float fillage);
    public abstract void OnDisposal(float fillageToDestroy);
    public abstract float Area();
    protected abstract void SetColor();
}
