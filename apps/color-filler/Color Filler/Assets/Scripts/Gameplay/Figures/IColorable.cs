﻿//interface for future possible colorable items

interface IColorable
{
    void OnFill(float fillage);
    void OnDisposal(float fillageToDestroy);
    void MaxedOut();
    float Area();

    //for Med/Hard Modes ?
    //void Move();
}
