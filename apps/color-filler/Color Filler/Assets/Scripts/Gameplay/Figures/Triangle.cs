﻿using UnityEngine;

public class Triangle : AbstractFigure
{

    private float triangleHeight = 1;
    private float triangleBase = 2;

    public int triangleNo;

    public override float Area()
    {
        return triangleHeight * triangleBase / 2;
    }

    public override void OnDisposal(float fillageToDestroy)
    {
        figureImage.fillAmount -= fillageToDestroy;

        fillageLevel = (fillageLevel - fillageToDestroy) < 0 ? 0 : fillageLevel - fillageToDestroy;
    }

    public override void OnFill(float fillage)
    {
        fillageLevel += fillage / Area();

        figureImage.fillAmount = 2 * fillageLevel / triangleBase;

        if (fillageLevel >= 1)
        {
            MaxedOut();
        }
    }

    protected override void SetColor()
    {
        switch (triangleNo)
        {
            case (1):
                figureImage.color = new Color(
                    PlayerPrefs.GetFloat("triangle1R"),
                    PlayerPrefs.GetFloat("triangle1G"),
                    PlayerPrefs.GetFloat("triangle1B"));
                break;
            case (2):
                figureImage.color = new Color(
                    PlayerPrefs.GetFloat("triangle2R"),
                    PlayerPrefs.GetFloat("triangle2G"),
                    PlayerPrefs.GetFloat("triangle2B"));
                break;
            case (3):
                figureImage.color = new Color(
                     PlayerPrefs.GetFloat("triangle3R"),
                     PlayerPrefs.GetFloat("triangle3G"),
                     PlayerPrefs.GetFloat("triangle3B"));
                break;
            case (4):
                figureImage.color = new Color(
                    PlayerPrefs.GetFloat("triangle4R"),
                    PlayerPrefs.GetFloat("triangle4G"),
                    PlayerPrefs.GetFloat("triangle4B"));
                break;
        }

    }
}
