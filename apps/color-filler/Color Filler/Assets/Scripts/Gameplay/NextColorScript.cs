﻿using UnityEngine;

public class NextColorScript : MonoBehaviour
{

    private SpriteRenderer sprite;


    private void RollColor()
    {
        int randomInt = Random.Range(0, 5);

        switch (randomInt)
        {
            case 0:
                IfRollOk(new Color(
                    PlayerPrefs.GetFloat("triangle1R"),
                    PlayerPrefs.GetFloat("triangle1G"),
                    PlayerPrefs.GetFloat("triangle1B")));
                break;
            case 1:
                IfRollOk(new Color(
                    PlayerPrefs.GetFloat("triangle2R"),
                    PlayerPrefs.GetFloat("triangle2G"),
                    PlayerPrefs.GetFloat("triangle2B")));
                break;
            case 2:
                IfRollOk(new Color(
                     PlayerPrefs.GetFloat("triangle3R"),
                     PlayerPrefs.GetFloat("triangle3G"),
                     PlayerPrefs.GetFloat("triangle3B")));
                break;
            case 3:
                IfRollOk(new Color(
                    PlayerPrefs.GetFloat("triangle4R"),
                    PlayerPrefs.GetFloat("triangle4G"),
                    PlayerPrefs.GetFloat("triangle4B")));
                break;
            case 4:
                IfRollOk(Color.white);
                break;
            default:
                RollColor();
                break;
        }
    }

    private void IfRollOk(Color nextColor)
    {
        if (sprite.color != nextColor)
        {
            ChangeColor(nextColor);
        }
        else
        {
            RollColor();
        }
    }

    private void ChangeColor(Color color)
    {
        sprite.color = color;
    }

    public Color SwipeColors(Color playerColor)
    {
        Color tempColor = sprite.color;
        ChangeColor(playerColor);
        return tempColor;
    }

    public Color TakeColor()
    {
        SpriteCheck();

        Color tempColor = sprite.color;
        RollColor();
        return tempColor;
    }

    private void SpriteCheck()
    {
        if (sprite == null)
        {
            sprite = GetComponent<SpriteRenderer>();
            RollColor();
        }
    }
}
