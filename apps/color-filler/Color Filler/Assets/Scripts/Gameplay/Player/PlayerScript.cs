﻿using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public GameObject NextColorObj;
    private NextColorScript nextColorScript;

    private float scaleIncreaseRate = 1;

    private SpriteRenderer playerSprite;
    private Vector3 currentScale, maxScale;
    //public props
    public Vector3 CurrentScale
    {
        get
        {
            return currentScale;
        }

        set
        {
            currentScale = value;
        }
    }
    public Vector3 MaxScale
    {
        get
        {
            return maxScale;
        }
    }
    public SpriteRenderer PlayerSprite
    {
        get
        {
            return playerSprite;
        }

    }


    private void Start()
    {
        playerSprite = GetComponent<SpriteRenderer>();
        nextColorScript = NextColorObj.GetComponent<NextColorScript>();

        currentScale = transform.localScale;
        maxScale = new Vector3(0.5f, 0.5f, 1);

        ChangePlayerColor();
    }


    public void ChangePlayerColor()
    {
        playerSprite.color = nextColorScript.TakeColor();
    }

    public void SwipeColors()
    {
        playerSprite.color = nextColorScript.SwipeColors(playerSprite.color);
    }

    private void Update()
    {
        if (GameController.IsPlaying)
        {
            IncreaseField();
        }
    }

    private void IncreaseField()
    {
        scaleIncreaseRate += Time.deltaTime / 2;

        currentScale = currentScale.x < maxScale.x ?
            currentScale = new Vector3
            (currentScale.x + scaleIncreaseRate / 10000,
            currentScale.y + scaleIncreaseRate / 10000,
            transform.localScale.z)
            : currentScale = maxScale;

        transform.localScale = currentScale;
    }
}
