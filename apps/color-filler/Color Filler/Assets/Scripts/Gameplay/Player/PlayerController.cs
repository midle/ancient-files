﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{

    private PlayerScript playerScript;

    private Vector2 inputUp, inputDown;

    public GameObject[] triangles = new GameObject[4];
    private Triangle[] triangleImages = new Triangle[4];

    public GameObject gameController;

    //this field should stay private, but is serialized for in-game value customization
    [SerializeField]
    private float checkSwipeLength;


    private void Start()
    {
        playerScript = GetComponent<PlayerScript>();

        //get figures for comparing
        for (int i = 0; i < triangleImages.Length; i++)
        {
            triangleImages[i] = triangles[i].GetComponent<Triangle>();
        }
    }


    private void Update()
    {
        if (GameController.IsPlaying)
        {
            CheckSwipes();
            CheckIfMaxed();
        }

        //CheckButton();
    }

    private void CheckButton()
    {
        for (int i = 0; i < 20; i++)
        {
            if (Input.GetKeyDown("joystick 1 button " + i.ToString()))
            {
                Debug.Log(i.ToString());
                //up    4
                //left  3
                //right 1
                //down  0
            }
        }
    }

    private void CheckIfMaxed()
    {
        if (playerScript.CurrentScale.x >= playerScript.MaxScale.x)
        {
            ActionOnTriangle(Random.Range(0, 4));

            MakePlayerColor();
        }
    }

    private void MakePlayerColor()
    {
        playerScript.ChangePlayerColor();
        playerScript.CurrentScale = new Vector3(0.1f, 0.1f, 1);
    }


    private void CheckSwipes()
    {
        //compile only for the right platform
#if (UNITY_IOS || UNITY_ANDROID || UNITY_WP8)
        TouchController();
#else
        MouseController();
        DanceMatController();
#endif
    }

    private void MouseController()
    {
        if (Input.GetMouseButtonDown(0))
        {
            inputDown = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }

        if (Input.GetMouseButtonUp(0))
        {
            inputUp = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            GlobalControl();
        }
    }
    private void DanceMatController()
    {
        if (Input.GetKeyDown(KeyCode.Joystick1Button1))//right
        {
            ActionOnTriangle(2);
        }
        else if (Input.GetKeyDown(KeyCode.Joystick1Button3))//left
        {
            ActionOnTriangle(0);
        }
        else if (Input.GetKeyDown(KeyCode.Joystick1Button4))//up
        {
            ActionOnTriangle(1);
        }
        else if (Input.GetKeyDown(KeyCode.Joystick1Button0))//down
        {
            ActionOnTriangle(3);
        }
        else if (Input.GetKeyDown(KeyCode.Joystick1Button9))//down
        {
            playerScript.SwipeColors();
        }
    }

    private void TouchController()
    {
        if (Input.touchCount > 0)
        {
            Touch myTouch = Input.touches[0];

            if (myTouch.phase == TouchPhase.Began)
            {
                inputDown = new Vector2(myTouch.position.x, myTouch.position.y);
            }

            if (myTouch.phase == TouchPhase.Ended)
            {
                inputUp = new Vector2(myTouch.position.x, myTouch.position.y);

                GlobalControl();
            }
        }
    }

    private void GlobalControl()
    {
        float tempSwipeX = Mathf.Abs(inputDown.x - inputUp.x);
        float tempSwipeY = Mathf.Abs(inputDown.y - inputUp.y);

        Vector3 swipe = Camera.main.ScreenToWorldPoint(inputUp) - Camera.main.ScreenToWorldPoint(inputDown);
        swipe.z = 0;

        if (swipe.magnitude > checkSwipeLength)
        {
            if (inputDown.x < inputUp.x && tempSwipeX > tempSwipeY)//right
            {
                ActionOnTriangle(2);
            }
            else if (inputDown.x > inputUp.x && tempSwipeX > tempSwipeY)//left
            {
                ActionOnTriangle(0);
            }
            else if (inputDown.y < inputUp.y && tempSwipeX < tempSwipeY)//up
            {
                ActionOnTriangle(1);
            }
            else if (inputDown.y > inputUp.y && tempSwipeX < tempSwipeY)//down
            {
                ActionOnTriangle(3);
            }
        }
        else
        {
            playerScript.SwipeColors();
        }
    }

    private void ActionOnTriangle(int triangle)
    {
        if (playerScript.PlayerSprite.color == triangleImages[triangle].FigureImage.color
            || playerScript.PlayerSprite.color == Color.white)
        {
            triangleImages[triangle].OnFill((Mathf.Pow(playerScript.CurrentScale.x / 1.7f, 2)) * Mathf.PI);

            MakePlayerColor();

            gameController.GetComponent<FillRateChecker>().CheckIfDestroyable();

            gameController.GetComponent<DelayedButtonSound>().PlaySound(1);
        }
        else
        {
            gameController.GetComponent<GameOverScript>().GameOver();
        }
    }
}
