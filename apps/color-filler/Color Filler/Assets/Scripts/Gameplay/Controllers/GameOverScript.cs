﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameOverScript : MonoBehaviour
{

    private GameController gameControllerScript;

    public Text gameOverText;


    private void Start()
    {
        gameControllerScript = GetComponent<GameController>();
        gameOverText.text = "";
    }


    public void GameOver()
    {
        SetHighscore();
        SetMaxCombo();
        SetTimeScore();
        SetPoints();

        ResetLevel();
        GameController.IsPlaying = false;
        gameOverText.text = "GAME OVER \nYou got " + gameControllerScript.Score + " coins!";
    }

    private void SetHighscore()
    {
        if (gameControllerScript.Score > PlayerPrefs.GetInt("highscore"))
        {
            PlayerPrefs.SetInt("highscore", gameControllerScript.Score);
        }
    }

    private void SetMaxCombo()
    {
        if (gameControllerScript.MaxCombo > PlayerPrefs.GetInt("combo"))
        {
            PlayerPrefs.SetInt("combo", gameControllerScript.MaxCombo);
        }
    }

    private void SetTimeScore()
    {
        if (gameControllerScript.PlayTime > PlayerPrefs.GetFloat("time"))
        {
            PlayerPrefs.SetFloat("time", gameControllerScript.PlayTime);
        }
    }

    private void SetPoints()
    {
        PlayerPrefs.SetInt("points", PlayerPrefs.GetInt("points") + gameControllerScript.Score);
    }


    private void ResetLevel()
    {
        StartCoroutine(LevelResetter());
    }

    private IEnumerator LevelResetter()
    {
        yield return new WaitForSeconds(1.963f);
        SceneManager.LoadScene("Menu", LoadSceneMode.Single);
    }
}
