﻿using UnityEngine;

//takes all figures and check their current fill rate
public class FillRateChecker : MonoBehaviour
{

    public GameObject[] triangles = new GameObject[4];
    private Triangle[] triangleImages = new Triangle[4];


    private void Start()
    {
        for (int i = 0; i < triangleImages.Length; i++)
        {
            triangleImages[i] = triangles[i].GetComponent<Triangle>();
        }
    }


    public void CheckIfDestroyable()
    {

        if (WasDisposed(MinimumCommonFillage()))
        {
            GetComponent<DelayedButtonSound>().PlaySound(2);
            GetComponent<GameController>().SetCombo(true);
        }
        else
        {
            GetComponent<GameController>().SetCombo(false);
        }
    }

    private float MinimumCommonFillage()
    {
        float trianglesMinFillage = 1;

        for (int i = 0; i < triangleImages.Length; i++)
        {
            trianglesMinFillage = Mathf.Min(trianglesMinFillage, triangleImages[i].FillageLevel);
        }

        return trianglesMinFillage;
    }

    private bool WasDisposed(float fillage)
    {
        if (fillage > 0)
        {
            for (int i = 0; i < triangleImages.Length; i++)
            {
                triangleImages[i].OnDisposal(fillage);
            }

            return true;
        }
        //else
        return false;
    }
}
