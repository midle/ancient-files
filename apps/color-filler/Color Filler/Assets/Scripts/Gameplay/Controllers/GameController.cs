﻿using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public Text scoreText, timeText, comboText;

    private float playTime;
    private static bool isPlaying;
    private int score, comboCounter, maxCombo;
    //public props
    public int Score
    {
        get
        {
            return score;
        }
    }
    public int MaxCombo
    {
        get
        {
            return maxCombo;
        }
    }
    public static bool IsPlaying
    {
        get
        {
            return isPlaying;
        }

        set
        {
            isPlaying = value;
        }
    }
    public float PlayTime
    {
        get
        {
            return playTime;
        }
    }


    private void Start()
    {
        isPlaying = true;
        SetCombo(false);
        SetScore();
    }


    private void Update()
    {
        if (isPlaying)
        {
            UpdatePlayTime();
        }
    }

    private void UpdatePlayTime()
    {
        playTime += Time.deltaTime;
        timeText.text = playTime.ToString("n1");
    }

    //Text Setters
    public void SetCombo(bool combo)
    {
        if (combo)
        {
            comboCounter++;
            maxCombo = Mathf.Max(maxCombo, comboCounter);
            SetScore();
        }
        else
        {
            comboCounter = 0;
        }

        comboText.text = comboCounter.ToString();
    }

    public void SetScore()
    {
        score += comboCounter;
        scoreText.text = score.ToString();
    }

    //Pause
    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            isPlaying = false;
        }
        else
        {
            isPlaying = true;
        }
    }


}
