﻿using UnityEngine;
using UnityEngine.UI;

public class MenuTitleColorChanger : MonoBehaviour
{

    public Text titleText;

    private float[] colors = new float[3];
    private float[] deltas = new float[3];


    private void Start()
    {
        //set every delta to fully change in 2/4/6 sec (if 60fps)
        for (int i = 0; i < deltas.Length; i++)
        {
            deltas[i] = (1.0f / 60) / (2 * (i + 1));
        }
    }


    private void Update()
    {
        ChangeColor();

        titleText.color = new Color(colors[0], colors[1], colors[2]);
    }

    private void ChangeColor()
    {
        for (int i = 0; i < colors.Length; i++)
        {
            if (colors[i] > 1 || colors[i] < 0)
            {
                deltas[i] = -deltas[i];
            }

            colors[i] += deltas[i];
        }
    }
}
