﻿using UnityEngine;
using UnityEngine.UI;

public class MenuButtonController : DelayedButtonSound
{

    public Button fxButton;

    public Sprite fxOnSprite, fxOffSprite;

    private void Start()
    {
        ChangeFxSprite();
    }

    public void OpenRepo()
    {
        PlaySound(0);
        Application.OpenURL("https://bitbucket.org/midle/color-filler");
    }

    public void ChangeFX()
    {
        PlayerPrefs.SetInt("fx", (PlayerPrefs.GetInt("fx") == 0 ? 1 : 0));
        ChangeFxSprite();
        PlaySound(0);
    }

    private void ChangeFxSprite()
    {
        fxButton.image.sprite = (PlayerPrefs.GetInt("fx") == 1 ? fxOnSprite : fxOffSprite);
    }

    public void ExitApp()
    {
        PlaySound(0);
        Application.Quit();
    }
}
