﻿using UnityEngine;

public class AtLaunchChecker : MonoBehaviour
{

    private void Start()
    {
        FirstLaunch();
    }

    private void FirstLaunch()
    {
        if (PlayerPrefs.GetInt("launch") < 1)
        {
            PlayerPrefs.SetInt("launch", PlayerPrefs.GetInt("launch") + 1);
            PlayerPrefs.SetInt("highscore", 0);
            PlayerPrefs.SetInt("combo", 0);
            PlayerPrefs.SetFloat("time", 0);
            PlayerPrefs.SetInt("points", 0);
            PlayerPrefs.SetFloat("triangle1R", 1);
            PlayerPrefs.SetFloat("triangle1G", 0);
            PlayerPrefs.SetFloat("triangle1B", 0);
            PlayerPrefs.SetFloat("triangle2R", 0);
            PlayerPrefs.SetFloat("triangle2G", 1);
            PlayerPrefs.SetFloat("triangle2B", 0);
            PlayerPrefs.SetFloat("triangle3R", 0);
            PlayerPrefs.SetFloat("triangle3G", 0);
            PlayerPrefs.SetFloat("triangle3B", 1);
            PlayerPrefs.SetFloat("triangle4R", 1);
            PlayerPrefs.SetFloat("triangle4G", (175.0f / 255.0f));
            PlayerPrefs.SetFloat("triangle4B", 0);
        }

    }

}
