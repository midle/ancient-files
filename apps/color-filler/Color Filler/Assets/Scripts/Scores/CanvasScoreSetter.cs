﻿using UnityEngine;
using UnityEngine.UI;

public class CanvasScoreSetter : MonoBehaviour
{
    public Text scoreText, comboText, timeText;

    private void Start()
    {
        scoreText.text = "Highscore:\n" + (PlayerPrefs.GetInt("highscore")).ToString();
        comboText.text = "Highest combo:\n" + (PlayerPrefs.GetInt("combo")).ToString();
        timeText.text = "Highest playtime:\n" + (PlayerPrefs.GetFloat("time")).ToString("n3");
    }

}
