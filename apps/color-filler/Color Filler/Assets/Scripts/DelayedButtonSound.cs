﻿using UnityEngine.SceneManagement;
using UnityEngine;
using System.Collections;

public class DelayedButtonSound : AudioControllerScript
{

    public void LoadScene(string scene)
    {
        StartCoroutine(PlayDeleyedSound(scene));
    }

    private IEnumerator PlayDeleyedSound(string scene)
    {
        PlaySound(0);
        yield return new WaitForSeconds(audioSources[0].clip.length);
        SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }
}
