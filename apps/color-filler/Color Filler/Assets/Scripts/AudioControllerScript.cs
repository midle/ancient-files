﻿using UnityEngine;

public abstract class AudioControllerScript : MonoBehaviour
{

    public AudioSource[] audioSources;

    public void PlaySound(int audioNo)
    {
        if (PlayerPrefs.GetInt("fx") == 1)
            audioSources[audioNo].Play();
    }
}
