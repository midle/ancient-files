package com.mindklik.lightstrobe;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RelativeLayout bg = (RelativeLayout) findViewById(R.id.RelativeLayout);
        bg.setBackgroundColor(Color.RED);

        colorButtonListener();
    }

    private void colorButtonListener() {

        final Button colorChangeButton = (Button) findViewById(R.id.colorChangeButton);

        colorChangeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RelativeLayout bg = (RelativeLayout) findViewById(R.id.RelativeLayout);
                int color = ((ColorDrawable) bg.getBackground()).getColor();
                if (color == Color.RED) {
                    bg.setBackgroundColor(Color.BLUE);
                    colorChangeButton.setTextColor(Color.BLUE);
                } else {
                    bg.setBackgroundColor(Color.RED);
                    colorChangeButton.setTextColor(Color.RED);
                }
            }
        });
    }
}
