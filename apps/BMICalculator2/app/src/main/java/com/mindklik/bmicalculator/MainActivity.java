package com.mindklik.bmicalculator;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        calculateButton();
    }

    private void calculateButton() {

        Button button = (Button) findViewById(R.id.calculateButton);

        button.setBackgroundColor(Color.rgb(33, 33, 33));
        button.setTextColor(Color.CYAN);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText heightTxt = (EditText) findViewById(R.id.heightTxt);
                String heightStr = heightTxt.getText().toString();
                double height = Double.parseDouble(heightStr);

                EditText weightTxt = (EditText) findViewById(R.id.weightTxt);
                String weightStr = weightTxt.getText().toString();
                double weight = Double.parseDouble(weightStr);

                double bmi = weight / (height * height);
                DecimalFormat decimalFormat = new DecimalFormat("#.#");
                double bmiNumberTrimmed = Double.parseDouble(decimalFormat.format(bmi).replace(',', '.'));
                EditText bmiNumber = (EditText) findViewById(R.id.bmiNumber);
                bmiNumber.setText(Double.toString(bmiNumberTrimmed));

                TextView bmiTxt = (TextView) findViewById(R.id.category);
                bmiTxt.setText(checkCategory(bmiNumberTrimmed));
            }
        });
    }

    private String checkCategory(Double bmi) {

        if (bmi < 15) {
            return "GigaUnderweight";
        } else if (bmi < 16) {
            return " BigUnderweight";
        } else if (bmi < 18.5) {
            return " Underweight";
        } else if (bmi < 25) {
            return " OK";
        } else if (bmi < 30) {
            return " Obesity";
        } else if (bmi < 40) {
            return " BigObesity";
        } else {
            return "GigaObesity";
        }
    }
}
