package com.mindklik.courtcounter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private int scoreA, scoreB;
    public final int SCORE_3 = 3, SCORE_2 = 2, SCORE_1 = 1;

    private TextView teamAScore, teamBScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        istnantiateFromView();
    }

    private void istnantiateFromView() {
        teamAScore = (TextView) findViewById(R.id.a_score);
        teamBScore = (TextView) findViewById(R.id.b_score);
    }

    public void displayTeamA(int score) {
        teamAScore.setText(String.valueOf(score));
    }

    public void displayTeamB(int score) {
        teamBScore.setText(String.valueOf(score));
    }


    public void aScore3(View view) {
        scoreA += SCORE_3;
        displayTeamA(scoreA);
    }

    public void aScore2(View view) {
        scoreA += SCORE_2;
        displayTeamA(scoreA);
    }

    public void aScore1(View view) {
        scoreA += SCORE_1;
        displayTeamA(scoreA);
    }

    public void bScore3(View view) {
        scoreB += SCORE_3;
        displayTeamB(scoreB);
    }

    public void bScore2(View view) {
        scoreB += SCORE_2;
        displayTeamB(scoreB);
    }

    public void bScore1(View view) {
        scoreB += SCORE_1;
        displayTeamB(scoreB);
    }

    public void resetScores(View view) {
        scoreA = 0;
        scoreB = 0;
        displayTeamA(scoreA);
        displayTeamB(scoreB);
    }
}
