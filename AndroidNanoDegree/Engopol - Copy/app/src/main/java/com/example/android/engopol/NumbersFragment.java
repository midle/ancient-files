package com.example.android.engopol;


public class NumbersFragment extends BaseWordFragment {


    @Override
    protected void addWords() {
        wordsList.add(new Word("one", "jeden", R.drawable.number_one, R.raw.num1));
        wordsList.add(new Word("two", "dwa", R.drawable.number_two, R.raw.num2));
        wordsList.add(new Word("three", "trzy", R.drawable.number_three, R.raw.num3));
        wordsList.add(new Word("four", "cztery", R.drawable.number_four, R.raw.num4));
        wordsList.add(new Word("five", "piec", R.drawable.number_five, R.raw.num5));
        wordsList.add(new Word("six", "szesc", R.drawable.number_six, R.raw.num6));
        wordsList.add(new Word("seven", "siedem", R.drawable.number_seven, R.raw.num7));
        wordsList.add(new Word("eight", "osiem", R.drawable.number_eight, R.raw.num8));
        wordsList.add(new Word("nine", "dziewiec", R.drawable.number_nine, R.raw.num9));
        wordsList.add(new Word("ten", "dziesiec", R.drawable.number_ten, R.raw.num10));
    }

    @Override
    protected void setColorID() {
        colorID = R.color.category_numbers;
    }
}
