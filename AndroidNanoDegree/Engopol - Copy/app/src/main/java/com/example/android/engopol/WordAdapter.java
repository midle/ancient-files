package com.example.android.engopol;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;


public class WordAdapter extends ArrayAdapter<Word> {

    private int colorID;

    public WordAdapter(@NonNull Context context, @NonNull List<Word> objects, int colorID) {
//        resource number can be of any value, cuz it wont be used in creating object,
//                as they are used only while 1 element is active (one textview...)
        super(context, R.layout.simple_layout_1, objects);
        this.colorID = colorID;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.simple_layout_1, parent, false);
        }

        Word currentWord = getItem(position);

        TextView polishWord = (TextView) listItemView.findViewById(R.id.polishWord);
        polishWord.setText(currentWord.getPolishTranslation());

        TextView englishWord = (TextView) listItemView.findViewById(R.id.englishWord);
        englishWord.setText(currentWord.getEnglishTranslation());

        ImageView imageWord = (ImageView) listItemView.findViewById(R.id.imageWord);
        if (currentWord.hasImage()) {
            imageWord.setImageResource(currentWord.getImageResourceID());
            imageWord.setVisibility(View.VISIBLE);
        } else {
            imageWord.setVisibility(View.GONE);
        }

        LinearLayout linearLayout = (LinearLayout) listItemView.findViewById(R.id.textLayout);
        linearLayout.setBackgroundColor(listItemView.getResources().getColor(colorID));

        return listItemView;
    }
}
