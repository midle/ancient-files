package com.example.android.engopol;

/**
 * Created by Mental on 2018-02-03.
 */

public class ColorsFragment extends BaseWordFragment {

    @Override
    protected void addWords() {
        wordsList.add(new Word("red", "czerwony", R.drawable.color_red, R.raw.czerwony));
        wordsList.add(new Word("green", "zielony", R.drawable.color_green, R.raw.zielony));
        wordsList.add(new Word("black", "czarny", R.drawable.color_black, R.raw.czarny));
    }

    @Override
    protected void setColorID() {
        colorID = R.color.category_colors;
    }


}
