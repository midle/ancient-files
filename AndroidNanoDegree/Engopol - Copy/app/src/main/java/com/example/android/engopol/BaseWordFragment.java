package com.example.android.engopol;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;


public abstract class BaseWordFragment extends Fragment {

    protected WordAdapter wordsAdapter;

    private MediaPlayer fxPlayer;
    private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            releaseFxPlayer();
        }
    };

    private AudioManager audioManager;

    AudioManager.OnAudioFocusChangeListener afChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        @Override
        public void onAudioFocusChange(int focusChange) {
            if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT ||
                    focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
                fxPlayer.pause();
                fxPlayer.seekTo(0);
            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {

                releaseFxPlayer();
            } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                fxPlayer.start();
            }
        }
    };


    protected ArrayList<Word> wordsList;
    protected int colorID;

    View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.activity_words, container, false);

        wordsList = new ArrayList<>();

        audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);

        addWords();
        setColorID();
        setAdapter();
        makeViews();
        return rootView;
    }


    private void setAdapter() {
        wordsAdapter = new WordAdapter(getActivity(), wordsList, colorID);
    }

    private void makeViews() {
        ListView wordsView = (ListView) rootView.findViewById(R.id.wordsView);
        wordsView.setAdapter(wordsAdapter);

        wordsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Word currentWord = wordsList.get(position);

                releaseFxPlayer();

                // Request audio focus for playback
                int result = audioManager.requestAudioFocus(afChangeListener,
                        // Use the music stream.
                        AudioManager.STREAM_MUSIC,
                        // Request permanent focus.
                        AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);

                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    fxPlayer = MediaPlayer.create(getActivity(), currentWord.getSoundResourceID());
                    fxPlayer.start();
                    fxPlayer.setOnCompletionListener(mCompletionListener);
                }
            }
        });
    }

    protected abstract void addWords();

    protected abstract void setColorID();

    private void releaseFxPlayer() {
        if (fxPlayer != null) {
            fxPlayer.release();
            fxPlayer = null;
            audioManager.abandonAudioFocus(afChangeListener);
        }
    }

    @Override
    public String toString() {
        return "BaseWordFragment{" +
                "wordsAdapter=" + wordsAdapter +
                ", fxPlayer=" + fxPlayer +
                ", mCompletionListener=" + mCompletionListener +
                ", audioManager=" + audioManager +
                ", afChangeListener=" + afChangeListener +
                ", wordsList=" + wordsList +
                ", colorID=" + colorID +
                ", rootView=" + rootView +
                '}';
    }

    @Override
    public void onStop() {
        super.onStop();
        releaseFxPlayer();
    }
}
