package com.example.android.engopol;


public class PhrasesFragment extends BaseWordFragment {

    @Override
    protected void addWords() {
        wordsList.add(new Word("My name is...", "Mam na imię...", R.raw.mni));
        wordsList.add(new Word("How are you?", "Jak się masz?", R.raw.jsm));
        wordsList.add(new Word("Do you know, that...", "Czy wiesz, że...", R.raw.cwz));
    }

    @Override
    protected void setColorID() {
        colorID = R.color.category_phrases;
    }
}
