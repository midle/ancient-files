package com.example.android.engopol;

/**
 * Created by Mental on 2018-02-03.
 */

public class FamilyFragment extends BaseWordFragment{

    @Override
    protected void addWords() {
        wordsList.add(new Word("mother", "mama", R.drawable.family_mother, R.raw.mama));
        wordsList.add(new Word("father", "tata", R.drawable.family_father, R.raw.tata));
        wordsList.add(new Word("sister", "siostra", R.drawable.family_younger_sister, R.raw.siostra));
        wordsList.add(new Word("brother", "brat", R.drawable.family_older_brother, R.raw.brat));
    }

    @Override
    protected void setColorID() {
        colorID = R.color.category_family;
    }
}
