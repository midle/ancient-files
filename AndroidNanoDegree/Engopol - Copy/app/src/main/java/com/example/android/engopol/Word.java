package com.example.android.engopol;


public class Word {

    private String englishTranslation;
    private String polishTranslation;

    private int imageResourceID = NO_RESOURCE;
    private int soundResourceID = NO_RESOURCE;
    private static final int NO_RESOURCE = -1;

    public Word(String englishTranslation, String polishTranslation, int soundResourceID) {
        this.englishTranslation = englishTranslation;
        this.polishTranslation = polishTranslation;
        this.soundResourceID = soundResourceID;
    }

    public Word(String englishTranslation, String polishTranslation, int imageResourceID, int soundResourceID) {
        this.englishTranslation = englishTranslation;
        this.polishTranslation = polishTranslation;
        this.imageResourceID = imageResourceID;
        this.soundResourceID = soundResourceID;
    }


    public String getEnglishTranslation() {
        return englishTranslation;
    }

    public String getPolishTranslation() {
        return polishTranslation;
    }

    public int getSoundResourceID() {
        return soundResourceID;
    }

    public int getImageResourceID() {
        return imageResourceID;
    }

    public boolean hasImage() {
        return imageResourceID != NO_RESOURCE;
    }
}
