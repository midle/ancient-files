package com.mindklik.justjava;

import android.content.Intent;
import android.net.Uri;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Guideline;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    public static final int COFFEE_PRICE = 5;

    private TextView quantityText, thanksText, priceText;
    private Guideline guideline;
    private EditText nameText;

    private int coffeesNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeVars();
    }

    private void initializeVars() {
        quantityText = (TextView) findViewById(R.id.quantityChange);
        priceText = (TextView) findViewById(R.id.priceChange);
        guideline = (Guideline) findViewById(R.id.guideline);
        nameText = (EditText) findViewById(R.id.name);
    }


    public void inc(View view) {
        coffeesNum++;
        changePriceTexts();
    }

    public void dec(View view) {
        coffeesNum = coffeesNum > 0 ? coffeesNum - 1 : 0;
        changePriceTexts();
    }

    private void changePriceTexts() {
        displayChangedText(coffeesNum);
        displayPrice(calculatePrice());
    }

    private void displayChangedText(int number) {
        quantityText.setText("" + number);
    }

    private void displayPrice(int number) {
        priceText.setText(NumberFormat.getCurrencyInstance().format(number));
    }

    private int calculatePrice() {
        int cream = 0;
        int chocolate = 0;

        if (ifHasChocolate()) {
            cream = 1;
        }
        if (ifHasCream()) {
            chocolate = 2;
        }
        return coffeesNum * (COFFEE_PRICE + cream + chocolate);
    }


    public void submitOrder(View view) {
        String orderSummary = createOrderSummary(ifHasCream(), ifHasChocolate(), getName());

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("*/*");
        emailIntent.putExtra(Intent.EXTRA_TEXT, orderSummary);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "JUSTJAVA COFFEE ORDER");

        if (emailIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(emailIntent);
        }

        showToast(orderSummary);
    }

    private String getName() {
        return nameText.getText().toString();
    }

    private boolean ifHasCream() {
        CheckBox creamBox = (CheckBox) findViewById(R.id.creamBox);
        return creamBox.isChecked();
    }

    private boolean ifHasChocolate() {
        CheckBox chocolateBox = (CheckBox) findViewById(R.id.chocolateBox);
        return chocolateBox.isChecked();
    }


    private String createOrderSummary(boolean cream, boolean chocolate, String name) {
        String orderSummary = getResources().getString(R.string.order_name, name);
        orderSummary += "\n" + getResources().getString(R.string.chocolate) + ": " + chocolate;
        orderSummary += "\n" + getResources().getString(R.string.cream) + ": " + cream;
        orderSummary += "\n" + getResources().getString(R.string.price) + " " + calculatePrice();
        orderSummary += "\n" + getResources().getString(R.string.thanks);
        return orderSummary;
    }

    private void showToast(String toastText) {
        Toast toast = Toast.makeText(getApplicationContext(), toastText, Toast.LENGTH_SHORT);
        toast.show();
    }


    public void alterGuideLine(View view) {
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) guideline.getLayoutParams();
        layoutParams.guidePercent = (float) (0.5f + Math.random() * 0.35f);
        guideline.setLayoutParams(layoutParams);
    }

    public void printToLogs(View view) {
        // Find first menu item TextView and print the text to the logs
        TextView textView1 = (TextView) findViewById(R.id.menu_item_1);
        Log.i("tv1", textView1.getText() + "");
        // Find second menu item TextView and print the text to the logs
        TextView textView2 = (TextView) findViewById(R.id.menu_item_2);
        Log.i("tv2", textView2.getText() + "");
        // Find third menu item TextView and print the text to the logs
        TextView textView3 = (TextView) findViewById(R.id.menu_item_3);
        Log.i("tv3", textView3.getText() + "");
    }
}
