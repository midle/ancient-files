package com.example.android.quakereport;


public class EarthquakeEntry {

    private float magnitude;
    private String placeOfOccurance;
    private String url;
    private long dateOfOccurance;

    public EarthquakeEntry(float magnitude, String placeOfOccurance, long dateOfOccurance, String url) {
        this.magnitude = magnitude;
        this.placeOfOccurance = placeOfOccurance;
        this.dateOfOccurance = dateOfOccurance;
        this.url=url;
    }

    public float getMagnitude() {
        return magnitude;
    }

    public String getPlaceOfOccurance() {
        return placeOfOccurance;
    }

    public long getDateOfOccurance() {
        return dateOfOccurance;
    }

    public String getUrl() {
        return url;
    }
}
