package com.example.android.quakereport;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class EarthquakesAdapter extends ArrayAdapter<EarthquakeEntry> {

    public EarthquakesAdapter(@NonNull Context context, @NonNull List<EarthquakeEntry> objects) {
        super(context, 0, objects);
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View listItemView = convertView;

        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.simple_eartquake_layout, parent, false);
        }

        EarthquakeEntry currentEntry = getItem(position);


        //setting magnitude layout
        TextView magnitudeText = (TextView) listItemView.findViewById(R.id.magnitude);
        String magnitudeIn = formatMagnitude(currentEntry.getMagnitude());
        magnitudeText.setText(magnitudeIn);

        GradientDrawable magnitudeCircle = (GradientDrawable) magnitudeText.getBackground();
        magnitudeCircle.setColor(setCircleColor((int) currentEntry.getMagnitude()));


        //setting location layout
        String fullLocalization = currentEntry.getPlaceOfOccurance();

        TextView localizationText = (TextView) listItemView.findViewById(R.id.localization);
        localizationText.setText(formatPlace(fullLocalization));

        TextView offset = (TextView) listItemView.findViewById(R.id.offset);
        offset.setText(formatOffset(fullLocalization));


        //setting time layout
        Date dateObject = new Date(currentEntry.getDateOfOccurance());

        TextView dateText = (TextView) listItemView.findViewById(R.id.date);
        String formattedDate = formatDate(dateObject);
        dateText.setText(formattedDate);

        TextView timeText = (TextView) listItemView.findViewById(R.id.time);
        String formattedTime = formatTime(dateObject);
        timeText.setText(formattedTime);

        return listItemView;
    }

    private String formatDate(Date dateInMilis) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormatter.format(dateInMilis);
        return date;
    }

    private String formatTime(Date dateInMilis) {
        SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
        String time = timeFormatter.format(dateInMilis);
        return time;
    }

    private String formatOffset(String input) {
        StringBuilder offset = new StringBuilder();

        if (input.contains("of")) {

            int commaIndex = input.indexOf("of");

            offset.append(input, 0, commaIndex + 2);
            offset.append(":");
        }

        return offset.toString();
    }

    private String formatPlace(String input) {
        StringBuilder place = new StringBuilder();

        if (input.contains("of")) {
            int commaIndex = input.indexOf("of");
            place.append(input, commaIndex + 3, input.length());

        } else {
            place.append(input, 0, input.length());
        }

        return place.toString();
    }

    private String formatMagnitude(double magnitudeIn) {
        DecimalFormat magnitudeFormatter = new DecimalFormat("0.0", new DecimalFormatSymbols(Locale.US));

        return magnitudeFormatter.format(magnitudeIn);
    }

    private int setCircleColor(int magnitude) {
        int magnitudeColor;

        switch ((int) Math.floor(magnitude)) {
            case 0:

            case 1:
                magnitudeColor = ContextCompat.getColor(getContext(), R.color.magnitude1);
                break;
            case 2:
                magnitudeColor = ContextCompat.getColor(getContext(), R.color.magnitude2);
                break;
            case 3:
                magnitudeColor = ContextCompat.getColor(getContext(), R.color.magnitude3);
                break;
            case 4:
                magnitudeColor = ContextCompat.getColor(getContext(), R.color.magnitude4);
                break;
            case 5:
                magnitudeColor = ContextCompat.getColor(getContext(), R.color.magnitude5);
                break;
            case 6:
                magnitudeColor = ContextCompat.getColor(getContext(), R.color.magnitude6);
                break;
            case 7:
                magnitudeColor = ContextCompat.getColor(getContext(), R.color.magnitude7);
                break;
            case 8:
                magnitudeColor = ContextCompat.getColor(getContext(), R.color.magnitude8);
                break;
            case 9:
                magnitudeColor = ContextCompat.getColor(getContext(), R.color.magnitude9);
                break;
            default:
                magnitudeColor = ContextCompat.getColor(getContext(), R.color.magnitude10plus);
        }

        return magnitudeColor;
    }
}
