package com.example.android.quakereport;

import android.content.AsyncTaskLoader;
import android.content.Context;

import java.util.List;


public class EarthquakeLoader extends AsyncTaskLoader<List<EarthquakeEntry>> {

    private String url;
    public static final String LOG_TAG = EarthquakeLoader.class.getName();

    public EarthquakeLoader(Context context, String url) {
        super(context);
        this.url = url;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    //fetch lists of quakes from USGS
    @Override
    public List<EarthquakeEntry> loadInBackground() {
        if (url == null) {
            return null;
        }

        return QueryUtils.fetchQuakes(url);
    }
}