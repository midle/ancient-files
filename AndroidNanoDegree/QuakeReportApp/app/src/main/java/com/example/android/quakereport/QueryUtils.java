package com.example.android.quakereport;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.EventListener;


public class QueryUtils {

    private final static String LOG_TAG = QueryUtils.class.toString();

    //private constructor, so only static method works
    private QueryUtils() {

    }

    public static ArrayList<EarthquakeEntry> fetchQuakes(String url) {

        //creating url object
        URL url1 = createUrl(url);

//        creating jsonRepsonse for fetching json data
        String jsonReposnse = null;

        try {
            jsonReposnse = makeHttpRequest(url1);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        Return the {@link  ArrayList<EarthquakeEntry>} objects list

        //testing loading indicators / blank spaces etc
//        try {
//            Thread.sleep(2500);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        return extractQuakes(jsonReposnse);
    }

    private static URL createUrl(String url) {

        URL url1 = null;

        try {
            url1 = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return url1;
    }

    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";

        // If the URL is null, then return early.
        if (url == null) {
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(8000 /* milliseconds */);
            urlConnection.setConnectTimeout(12000 /* milliseconds */);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // If the request was successful (response code 200),
            // then read the input stream and parse the response.
            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error response code: " + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem retrieving the earthquake JSON results.", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private static String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }


    public static ArrayList<EarthquakeEntry> extractQuakes(String JSON_RESPONSE) {

        ArrayList<EarthquakeEntry> earthquakesList = new ArrayList<>();

        try {

            JSONObject entryJSON = new JSONObject(JSON_RESPONSE);

            JSONArray earthquakes = entryJSON.getJSONArray("features");


            for (int i = 0; i < earthquakes.length(); i++) {
                JSONObject currentObject = earthquakes.getJSONObject(i);
                JSONObject currentProps = currentObject.getJSONObject("properties");

                //setting url to currentEarthquake
                String url = currentProps.getString("url");

                double magnitude = currentProps.getDouble("mag");
                String place = currentProps.getString("place");
                long dateInMilis = currentProps.getLong("time");

                earthquakesList.add(new EarthquakeEntry((float) magnitude, place, dateInMilis, url));
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Log.i(LOG_TAG, "stacktrace");
        }

        return earthquakesList;
    }
}
