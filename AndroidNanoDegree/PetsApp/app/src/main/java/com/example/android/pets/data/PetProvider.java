package com.example.android.pets.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

/**
 * {@link ContentProvider} for Pets app.
 */
public class PetProvider extends ContentProvider {

    /**
     * Tag for the log messages
     */
    public static final String LOG_TAG = PetProvider.class.getSimpleName();

    private static final int PETS = 100;
    private static final int PET_ID = 101;

    private static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        URI_MATCHER.addURI(PetContract.CONTENT_AUTHORITY, PetContract.PATH_PETS, PETS);
        URI_MATCHER.addURI(PetContract.CONTENT_AUTHORITY, PetContract.PATH_PETS + "/#", PET_ID);
    }

    private PetDBHelper petDB;

    /**
     * Initialize the provider and the database helper object.
     */
    @Override
    public boolean onCreate() {

        petDB = new PetDBHelper(getContext());

        return true;
    }

    /**
     * Perform the query for the given URI. Use the given projection, selection, selection arguments, and sort order.
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {

        SQLiteDatabase db = petDB.getReadableDatabase();

        Cursor cursor = null;

        int match = URI_MATCHER.match(uri);

        switch (match) {
            case PETS:
                cursor = db.query(PetContract.PetEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case PET_ID:
                selection = PetContract.PetEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                cursor = db.query(PetContract.PetEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Can not make query for: " + uri);
        }

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    /**
     * Insert new data into the provider with the given ContentValues.
     */
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        final int match = URI_MATCHER.match(uri);
        switch (match) {
            case PETS:
                return insertPet(uri, contentValues);
            default:
                throw new IllegalArgumentException("Insertion is not supported for " + uri);
        }
    }

    private Uri insertPet(Uri uri, ContentValues values) {

        String tempName = values.getAsString(PetContract.PetEntry.PET_NAME);
        if (tempName == null) {
            throw new IllegalArgumentException("no name!");
        }

        Integer tempGender = values.getAsInteger(PetContract.PetEntry.PET_GENDER);
        if (tempGender == null || !PetContract.PetEntry.isValidGender(tempGender)) {
            throw new IllegalArgumentException("wrong gender?");
        }

        Integer tempWeight = values.getAsInteger(PetContract.PetEntry.PET_WEIGHT);
        if (tempWeight == null && tempWeight < 0) {
            throw new IllegalArgumentException("wrong weight?");
        }

        SQLiteDatabase db = petDB.getWritableDatabase();

        long id = db.insert(PetContract.PetEntry.TABLE_NAME, null, values);

        // Once we know the ID of the new row in the table,
        // return the new URI with the ID appended to the end of it

        if (id == -1) {
            Log.i(LOG_TAG, "id=-1");
            return null;
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return ContentUris.withAppendedId(uri, id);
    }

    /**
     * Updates the data at the given selection and selection arguments, with the new ContentValues.
     */
    @Override
    public int update(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {

        final int petMatch = URI_MATCHER.match(uri);

        switch (petMatch) {
            case PETS:
                return updatePet(uri, contentValues, selection, selectionArgs);
            case PET_ID:
                selection = PetContract.PetEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                return updatePet(uri, contentValues, selection, selectionArgs);
            default:
                throw new IllegalArgumentException("update wont work for: " + uri);
        }

    }

    private int updatePet(Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        if (values.size() == 0) {
            return 0;
        }

        if (values.containsKey(PetContract.PetEntry.PET_NAME)) {
            String name = values.getAsString(PetContract.PetEntry.PET_NAME);
            if (name == null) {
                throw new IllegalArgumentException("wrong name");
            }
        }

        if (values.containsKey(PetContract.PetEntry.PET_WEIGHT)) {
            Integer weight = values.getAsInteger(PetContract.PetEntry.PET_WEIGHT);
            if (weight != null && weight < 0) {
                throw new IllegalArgumentException("wrong weight");
            }
        }

        if (values.containsKey(PetContract.PetEntry.PET_GENDER)) {
            Integer gender = values.getAsInteger(PetContract.PetEntry.PET_GENDER);
            if (gender == null || !PetContract.PetEntry.isValidGender(gender)) {
                throw new IllegalArgumentException("wrong gender " + gender);
            }
        }

        SQLiteDatabase database = petDB.getWritableDatabase();

        int changed = database.update(PetContract.PetEntry.TABLE_NAME, values, selection, selectionArgs);

        if (changed != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        // Returns the number of database rows affected by the update statement
        return changed;
    }

    /**
     * Delete the data at the given selection and selection arguments.
     */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {

        SQLiteDatabase database = petDB.getWritableDatabase();

        final int petMatch = URI_MATCHER.match(uri);

        int deleted;

        switch (petMatch) {
            case PETS:
                deleted = database.delete(PetContract.PetEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case PET_ID:
                selection = PetContract.PetEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                deleted = database.delete(PetContract.PetEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("delete wont work for: " + uri);
        }

        if (deleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return deleted;
    }

    /**
     * Returns the MIME type of data for the content URI.
     */
    @Override
    public String getType(Uri uri) {

        final int match = URI_MATCHER.match(uri);

        switch (match) {
            case PETS:
                return PetContract.PetEntry.CONTENT_LIST_TYPE;
            case PET_ID:
                return PetContract.PetEntry.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalStateException("wrong uri" + uri);
        }

    }
}