package com.example.android.pets.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.android.pets.data.PetContract.PetEntry;


public class PetDBHelper extends SQLiteOpenHelper {

    private static final String CREATE_TABLE = "CREATE TABLE " + PetEntry.TABLE_NAME + " ( "
            + PetEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + PetEntry.PET_NAME + " TEXT NOT NULL, "
            + PetEntry.PET_BREED + " TEXT, "
            + PetEntry.PET_GENDER + " INTEGER NOT NULL, "
            + PetEntry.PET_WEIGHT + " INTEGER NOT NULL DEFAULT 0);";

    private static final int TABLE_VERSION = 1;
    private static final String DB_NAME = "shelter";


    public PetDBHelper(Context context) {
        super(context, DB_NAME, null, TABLE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
