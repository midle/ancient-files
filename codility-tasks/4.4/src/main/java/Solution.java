// task name: MaxCounters

import java.util.Arrays;

public class Solution {

    public int[] solution(int N, int[] A) {

//        already 0
        int[] counterTab = new int[N];

        int tempMax = 0;
//        initialization, number <0! for futher loops/conditions
        int tempMaxFillLength = -1;
        int addPart = 0;

        for (int i = 0; i < A.length; i++) {
            if (A[i] > counterTab.length) {
                tempMaxFillLength = i;
            }
        }

        for (int i = 0; i < A.length; i++) {

            if (i == tempMaxFillLength) {
                Arrays.fill(counterTab, tempMax);

            } else if (A[i] <= counterTab.length) {
                if (counterTab[A[i] - 1] < addPart) {
                    counterTab[A[i] - 1] = addPart;
                }

                counterTab[A[i] - 1]++;

                if (counterTab[A[i] - 1] > tempMax) {
                    tempMax = counterTab[A[i] - 1];
                }

            } else {
                addPart = tempMax;
            }
        }

        return counterTab;
    }

}
