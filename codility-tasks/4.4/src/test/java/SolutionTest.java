import org.junit.Test;

import java.util.Arrays;
import java.util.Random;

import static org.junit.Assert.*;

public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int N = 5;
        int[] A = new int[]{3, 4, 4, 6, 1, 4, 4};

        int[] result = solution.solution(N, A);

        int[] equals = new int[]{3, 2, 2, 4, 2};

        assertArrayEquals(equals, result);

    }

    @Test
    public void changing() throws Exception {

        int N = 5;
        int[] A = new int[]{9, 9, 3, 4, 4, 6, 9, 9, 1, 9, 4, 4, 9, 9, 3, 4};

        int[] result = solution.solution(N, A);

        int[] equals = new int[]{5, 5, 6, 6, 5};

        assertArrayEquals(equals, result);

    }

    @Test
    public void small() throws Exception {

        int N = 1;
        int[] A = new int[]{1};

        int[] result = solution.solution(N, A);

        int[] equals = new int[]{1};

        assertArrayEquals(equals, result);

    }


    //    randomized ints for time checking !
    @Test
    public void max() throws Exception {

        int N = 100000;
        int[] A = new int[100000];

        Arrays.fill(A, 100001);

        Random random = new Random(2);

        for (int i = 0; i < A.length; i++) {
            int randomInt = random.nextInt();
            if (randomInt == 0) {
                A[i] = random.nextInt(N);
            } else {
                A[i] = N + 2;
            }
        }

        int[] result = solution.solution(N, A);

//        just for time and no errors/bad answers
        assertEquals(result, result);
    }
}