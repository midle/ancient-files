import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        String S = "CAGCCTA";
        int[] P = new int[]{2, 5, 0};
        int[] Q = new int[]{4, 5, 6};

        int[] result = solution.solution(S, P, Q);
        int[] assertResult = new int[]{2, 4, 1};

        assertArrayEquals(assertResult, result);
    }

    @Test
    public void min1() throws Exception {

        String S = "C";
        int[] P = new int[]{0};
        int[] Q = new int[]{0};

        int[] result = solution.solution(S, P, Q);
        int[] assertResult = new int[]{2};

        assertArrayEquals(assertResult, result);
    }

    @Test
    public void min2() throws Exception {

        String S = "CA";
        int[] P = new int[]{1};
        int[] Q = new int[]{1};

        int[] result = solution.solution(S, P, Q);
        int[] assertResult = new int[]{1};

        assertArrayEquals(assertResult, result);
    }

    @Test
    public void almin2() throws Exception {

        String S = "CA";
        int[] P = new int[]{1, 1};
        int[] Q = new int[]{1, 1};

        int[] result = solution.solution(S, P, Q);
        int[] assertResult = new int[]{1, 1};

        assertArrayEquals(assertResult, result);
    }

    @Test
    public void t2() throws Exception {

        String S = "TC";
        int[] P = new int[]{0, 0, 1};
        int[] Q = new int[]{0, 1, 1};

        int[] result = solution.solution(S, P, Q);
        int[] assertResult = new int[]{4, 2, 2};

        assertArrayEquals(assertResult, result);
    }

    @Test
    public void t3() throws Exception {

        String S = "AC";
        int[] P = new int[]{0, 0, 1};
        int[] Q = new int[]{0, 1, 1};

        int[] result = solution.solution(S, P, Q);
        int[] assertResult = new int[]{1, 1, 2};

        assertArrayEquals(assertResult, result);
    }

}