// task name: MinAvgTwoSlice

import java.util.ArrayList;

public class Solution {

    public int[] solution(String S, int[] P, int[] Q) {

//        initiate
        int[] solution = new int[P.length];

        ArrayList<Integer> listA = new ArrayList<>();
        ArrayList<Integer> listC = new ArrayList<>();
        ArrayList<Integer> listG = new ArrayList<>();
        ArrayList<Integer> listT = new ArrayList<>();

        if (S.charAt(0) == 'A') {
            listA.add(1);
            listC.add(0);
            listG.add(0);
            listT.add(0);
        } else if (S.charAt(0) == 'C') {
            listA.add(0);
            listC.add(1);
            listG.add(0);
            listT.add(0);
        } else if (S.charAt(0) == 'G') {
            listA.add(0);
            listC.add(0);
            listG.add(1);
            listT.add(0);
        } else {
            listA.add(0);
            listC.add(0);
            listG.add(0);
            listT.add(1);
        }

        //rewrite loop
        for (int i = 0; i < S.length() - 1; i++) {

            char tempChar = S.charAt(i + 1);

            if (tempChar == 'A') {
                listA.add(listA.get(i) + 1);
                listC.add(listC.get(i));
                listG.add(listG.get(i));
                listT.add(listT.get(i));
            } else if (tempChar == 'C') {
                listA.add(listA.get(i));
                listC.add(listC.get(i) + 1);
                listG.add(listG.get(i));
                listT.add(listT.get(i));
            } else if (tempChar == 'G') {
                listA.add(listA.get(i));
                listC.add(listC.get(i));
                listG.add(listG.get(i) + 1);
                listT.add(listT.get(i));
            } else {
                listA.add(listA.get(i));
                listC.add(listC.get(i));
                listG.add(listG.get(i));
                listT.add(listT.get(i) + 1);
            }

        }

        //write loop
        for (int i = 0; i < P.length; i++) {

            if (listA.get(Q[i]) > listA.get(P[i]) || S.charAt(P[i]) == 'A') {
                solution[i] = 1;
            } else if (listC.get(Q[i]) > listC.get(P[i]) || S.charAt(P[i]) == 'C') {
                solution[i] = 2;
            } else if (listG.get(Q[i]) > listG.get(P[i]) || S.charAt(P[i]) == 'G') {
                solution[i] = 3;
            } else {
                solution[i] = 4;
            }
        }

        return solution;
    }
}
