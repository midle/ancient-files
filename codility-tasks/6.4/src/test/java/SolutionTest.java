import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void solution() throws Exception {

        int[] A = new int[]{1, 5, 2, 1, 4, 0};

        int result = solution.solution(A);

        assertEquals(11, result);
    }

    @Test
    public void solution2() throws Exception {

        int[] A = new int[]{1, Integer.MAX_VALUE, 0};

        int result = solution.solution(A);

        assertEquals(2, result);
    }

}