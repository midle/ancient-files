// task name: NumberOfDiscIntersections

import java.util.Arrays;
import java.util.Comparator;

public class Solution {

    //    easy but not-optimal
//    public int solution(int[] A) {
//
//        int solution = 0;
//        //            double for overflow handle
//        for (int i = 0; i < A.length; i++) {
//
//            double left = i - (double)A[i];
//            double right = i + (double)A[i];
//            for (int j = i + 1; j < A.length; j++) {
//                double    tempLeft = j - (double)A[j];
//                double   tempRight = j + (double)A[j];
//                if (tempRight >= left && tempLeft <= right) {
//                    solution++;
//                }
//
//                if (solution > 10000000) {
//                    return -1;
//                }
//            }
//        }
//
//        return solution;
//    }


    //    a bit better solution in efficiency, but still not too efficient (but by small small margin)! yet it's very clear
//    public int solution(int[] A) {
//
//        int result = 0;
//
//        float[][] sections = new float[A.length][2];
//
//        for (int i = 0; i < A.length; i++) {
//            sections[i][0] = i - (float) A[i];
//            sections[i][1] = i + (float) A[i];
//        }
//
//        Arrays.sort(sections, Comparator.comparingDouble(o -> o[0]));
//
//        for (int i = 0; i < A.length; i++) {
//
//
//            if (result > 10000000) {
//                return -1;
//            }
//
//            for (int j = i + 1; j < A.length; j++) {
//                if (sections[i][1] < sections[j][0]) {
//                    break;
//                } else {
//                    result++;
//
//                    if (result > 10000000) {
//                        return -1;
//                    }
//                }
//            }
//        }
//        return result;
//    }


//    100/100

    public int solution(int[] A) {

        int result = 0;

        float[] left = new float[A.length];
        float[] right = new float[A.length];

        for (int i = 0; i < A.length; i++) {
            left[i] = i - (float) A[i];
            right[i] = i + (float) A[i];
        }

        Arrays.sort(left);

        for (int i = 0; i < A.length; i++) {

//            using binary search to make efficiency logN
//            finding index >= our number (>our number cuz of 0.1)
//            adding 0.1 to find always a bit greater number (we receive integers)
//            binary will never find number with 0.1 decimal, so it returns: "(-(insertedValue)- 1)". We shall than add 1 and *(-1)

//            Right array starts from 0!!! which allows us to KNOW that if any further left is lower than [i] right, than it INTERSECTS!

            int foundIndex = (Arrays.binarySearch(left, (right[i] + 0.1f)));

//            -1 itself
//            -i -previous
            result += ((foundIndex + 1) * (-1)) - i - 1;

            System.out.println(foundIndex);

            if (result > 10000000) {
                return -1;
            }
        }


        return result;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] A = new int[]{1, 5, 2, 1, 4, 0};
        solution.solution(A);

    }
}
