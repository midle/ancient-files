import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by MD on 2017-10-30.
 */
public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int[] A = new int[]{1, 2, 3, 4, 3, 4, 1, 2, 3, 4, 6, 2};

        assertEquals(3, solution.solution(A));
    }

    @Test
    public void single() throws Exception {

        int[] A = new int[]{2};

        assertEquals(0, solution.solution(A));
    }

    @Test
    public void onePeak() throws Exception {

        int[] A = new int[]{1, 2, 1};

        assertEquals(1, solution.solution(A));
    }

    @Test
    public void my3() throws Exception {

        int[] A = new int[]{1, 2, 1, 2, 1, 2, 1};

        assertEquals(1, solution.solution(A));
    }

    @Test
    public void my2() throws Exception {

        int[] A = new int[]{1, 2, 1, 1, 1, 1, 1, 2, 1, 1};

        assertEquals(2, solution.solution(A));
    }

    @Test
    public void my1Prime() throws Exception {

        int[] A = new int[]{13, 13, 13, 13, 13, 13, 13, 17, 13, 13, 13, 13, 13};

        assertEquals(1, solution.solution(A));
    }
}