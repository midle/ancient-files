// task name: Peaks

import java.util.ArrayList;

/**
 * Created by MD on 2017-10-30.
 */
public class Solution {

    public int solution(int[] A) {

        return solvingAlgo(A);
    }

    private boolean[] setPeak(int[] A) {
        boolean[] peak = new boolean[A.length];
        for (int i = 1; i < A.length - 1; i++) {
            if (A[i] > A[i - 1] && A[i] > A[i + 1]) {
                peak[i] = true;
            }
        }
        return peak;
    }

    private int[] nextPeak(int[] A) {

        boolean[] peaks = setPeak(A);
        int[] nextFlag = new int[A.length];
        nextFlag[A.length - 1] = -1;
        for (int i = A.length - 2; i >= 0; i--) {
            if (peaks[i]) {
                nextFlag[i] = i;
            } else {
                nextFlag[i] = nextFlag[i + 1];
            }
        }
        return nextFlag;
    }

    private ArrayList<Integer> dividers(int len) {

        ArrayList<Integer> dividersArray = new ArrayList<>();

        for (int i = 1; i <= len; i++) {
            if (len % i == 0) {
                dividersArray.add(i);
            }
        }
        return dividersArray;
    }

    private int solvingAlgo(int[] A) {

        int result = 0;

        ArrayList<Integer> dividers = dividers(A.length);

        int[] peaks = nextPeak(A);

        for (int i = 0; i < dividers.size(); i++) {

            int checkNum = dividers.get(i);

            int j = 0;
            int multiplier = 1;
            int tempRes = 0;

            while (j < A.length) {

                if (peaks[j] >= checkNum * multiplier || peaks[j] == -1) {
                    tempRes = 0;
                    break;
                }

                tempRes++;
                j += checkNum;
                multiplier++;
            }

            result = Math.max(result, tempRes);
        }

        return result;
    }
}
