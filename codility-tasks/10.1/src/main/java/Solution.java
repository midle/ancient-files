// task name: MinPerimeterRectangle


public class Solution {

    public int solution(int N) {

        int solution = 0;

//        a*b=N, so b=N/a
//        L(b)=2a+2b=2b+2N/b
//        *L(b)'= 2-(2N/(b^2))*
//        min from derivative..., second '' is always >0, so we will look always for minimum P value

        int a = 0;
        int b = 0;
        b = (int) Math.sqrt((2 * N) / 2); //rounded to floor

        for (int i = b; i > 0; i--) {

            if (N % i == 0) {
                a = N / i;
                System.out.println(a);
                System.out.println(i);
                return 2 * i + 2 * a;
            }
        }

        return solution;
    }
}
