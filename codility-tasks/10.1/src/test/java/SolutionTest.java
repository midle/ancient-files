import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by MD on 2017-10-19.
 */
public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int N = 30;

        assertEquals(22, solution.solution(N));
    }

    @Test
    public void test100() throws Exception {

        int N = 100;

        assertEquals(40, solution.solution(N));
    }

    @Test
    public void test1() throws Exception {

        int N = 1;

        assertEquals(4, solution.solution(N));
    }

    @Test
    public void test17() throws Exception {

        int N = 17;

        assertEquals(36, solution.solution(N));
    }

}