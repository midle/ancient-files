// task name: Dominator

import java.util.Stack;

public class Solution {

    public int solution(int[] A) {

        int tempIndex = -1;

        Stack<Integer> tempLeaderStack = new Stack();

        for (int i = 0; i < A.length; i++) {

            if (tempLeaderStack.empty()) {
                tempLeaderStack.push(A[i]);
                tempIndex = i;
            } else {
                if (tempLeaderStack.peek() == A[i]) {
                    tempLeaderStack.push(A[i]);
                    tempIndex = i;
                } else {
                    tempLeaderStack.pop();
                }
            }
        }

        int counter = 0;

        for (int i = 0; i < A.length; i++) {
            if (!tempLeaderStack.empty() && tempLeaderStack.peek() == A[i]) {
                counter++;
            }
        }

        if (counter > A.length / 2) {
            return tempIndex;
        } else {
            return -1;
        }
    }

}
