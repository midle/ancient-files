import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by MD on 2017-10-07.
 */
public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int[] A = new int[]{4, 3, 4, 4, 4, 2};

        int result = solution.solution(A);

        assertEquals(4, result);
    }

    @Test
    public void their1() throws Exception {

        int[] A = new int[]{2, 1, 1, 3, 4};

        int result = solution.solution(A);

        assertEquals(-1, result);
    }

}