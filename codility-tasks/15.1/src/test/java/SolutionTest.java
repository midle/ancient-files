import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by MD on 2017-11-09.
 */
public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int[] A = new int[]{-5, -3, -1, 0, 3, 6};

        assertEquals(5, solution.solution(A));
    }

    @Test
    public void extreme1() throws Exception {

        int maxLen = 100000;
        int[] A = new int[maxLen];

        for (int i = 0; i < A.length; i++) {
            A[i] = Integer.MAX_VALUE;
        }

        assertEquals(1, solution.solution(A));
    }

}