// task name: AbsDistinct

import java.util.HashSet;

/**
 * Created by MD on 2017-11-09.
 */
public class Solution {

    public int solution(int[] A) {

        HashSet<Integer> checkSet = new HashSet<>();

        for (int i = 0; i < A.length; i++) {
            checkSet.add(Math.abs(A[i]));
        }

        return checkSet.size();
    }
}
