// task name: CountNonDivisible

import java.util.ArrayList;

/**
 * Created by MD on 2017-11-02.
 */
public class Solution {

    public int[] solution(int[] A) {

        int[] existingDivisors = existingDivisors(A);

        int[] result = solve(A, existingDivisors);

        return result;
    }

    private int[] existingDivisors(int[] A) {

        int[] existingDivisors = new int[A.length * 2 + 1];

        for (int i = 0; i < A.length; i++) {
            existingDivisors[A[i]]++;
        }
        return existingDivisors;
    }

    private ArrayList<Integer> getDivisors(int n) {

        ArrayList<Integer> divisors = new ArrayList<>();

        int i = 1;
        while (i * i <= n) {
            if (n % i == 0) {
                divisors.add(i);
                if (i != n / i) {
                    divisors.add(n / i);
                }
            }
            i++;
        }

        return divisors;
    }

    private int[] solve(int[] A, int[] existingDivisors) {

        int[] result = new int[A.length];

        for (int i = 0; i < A.length; i++) {
            int tempResult = A.length;

            ArrayList<Integer> divisors = getDivisors(A[i]);

            for (int entry :
                    divisors) {
                tempResult -= existingDivisors[entry];
            }

            result[i] = tempResult;
        }

        return result;
    }
}
