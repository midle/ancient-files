import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by MD on 2017-11-02.
 */
public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int[] A = new int[]{3, 1, 2, 3, 6};

        int[] result = new int[]{2, 4, 3, 2, 0};

        assertArrayEquals(result, solution.solution(A));
    }

    @Test
    public void my1() throws Exception {

        int[] A = new int[]{3, 1, 2, 3, 10};

        int[] result = new int[]{2, 4, 3, 2, 2};

        assertArrayEquals(result, solution.solution(A));
    }

    @Test
    public void single() throws Exception {

        int[] A = new int[]{1};

        int[] result = new int[]{0};

        assertArrayEquals(result, solution.solution(A));
    }

    @Test
    public void doublet() throws Exception {

        int[] A = new int[]{2, 4};

        int[] result = new int[]{1, 0};

        assertArrayEquals(result, solution.solution(A));
    }

    @Test
    public void bigArray() throws Exception {

        int[] A = new int[50000];

        for (int i = 0; i < A.length; i++) {
            A[i] = i + 1;
        }

        assertArrayEquals(solution.solution(A), solution.solution(A));
    }

}