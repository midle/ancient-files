// task name: MinAbsSumOfTwo

import java.util.Arrays;

public class Solution {

    public int solution(int[] A) {

        int result = Integer.MAX_VALUE;

        Arrays.sort(A);

        int start = 0;
        int end = A.length - 1;

        while (start <= end) {

            result = Math.min(result, Math.abs(A[start] + A[end]));

            if (result == 0) {
                return result;
            }

            if (A[start] + A[end] < 0) {
                start++;
            } else {
                end--;
            }
        }

        return result;
    }
}
