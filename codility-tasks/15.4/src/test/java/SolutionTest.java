import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by MD on 2017-11-12.
 */
public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void solution() throws Exception {

        int[] A = new int[]{1, 4, -3};

        assertEquals(1, solution.solution(A));
    }

    @Test
    public void x() throws Exception {

        int[] A = new int[]{0, 4, -3};

        assertEquals(0, solution.solution(A));
    }

    @Test
    public void y() throws Exception {

        int[] A = new int[]{-5, -1, 1, 1, 5};

        assertEquals(0, solution.solution(A));
    }


    @Test
    public void max() throws Exception {

        int max = 100000;

        int[] A = new int[max];

        for (int i = 0; i < A.length; i++) {
            A[i] = 1000000000;
        }

        assertEquals(2000000000, solution.solution(A));
    }

}