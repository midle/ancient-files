// task name: MinMaxDivision


// used idea from:
//         https://github.com/ZRonchy/Codility/blob/master/Lesson12/MinMaxDivision.java

public class Solution {

    int solution(int K, int M, int A[]
//            , int N    - they wanted N but finally do not want ?
    ) {

        int max = 0;
        int min = 0;

        for (int i = 0; i < A.length; i++) {
            max += A[i];
            min = Math.max(min, A[i]);
        }

        int result = max;

        while (min <= max) {
            int tempRes = (max + min) / 2;

            if (canDivide(tempRes, K, A)) {
                max = tempRes - 1;
                result = tempRes;
            } else {
                min = tempRes + 1;
            }
        }

        return result;
    }

    private boolean canDivide(int tempRes, int K, int[] A) {

        int tempSum = 0;

        for (int i = 0; i < A.length; i++) {
            tempSum += A[i];
            if (tempSum > tempRes) {
                K--;
                tempSum = A[i];
            }
            if (K == 0) {
                return false;
            }
        }

        return true;
    }
}

