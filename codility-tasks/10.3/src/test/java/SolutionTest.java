import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by MD on 2017-10-29.
 */
public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int[] A = new int[]{1, 5, 3, 4, 3, 4, 1, 2, 3, 4, 6, 2};

        assertEquals(3, solution.solution(A));
    }


    @Test
    public void single() throws Exception {

        int[] A = new int[]{2};

        assertEquals(0, solution.solution(A));
    }

    @Test
    public void doublet() throws Exception {

        int[] A = new int[]{1, 2};

        assertEquals(0, solution.solution(A));
    }

    @Test
    public void singlepeak() throws Exception {

        int[] A = new int[]{1, 2, 1};

        assertEquals(1, solution.solution(A));
    }

    @Test
    public void notPeakDoublex4() throws Exception {

        int[] A = new int[]{1, 2, 2, 1};

        assertEquals(0, solution.solution(A));
    }

    @Test
    public void doubleFlag() throws Exception {

        int[] A = new int[]{1, 2, 1, 1, 2, 1};

        assertEquals(2, solution.solution(A));
    }

    @Test
    public void peakUp() throws Exception {

        int[] A = new int[]{1, 1, 2, 1, 1, 1, 2, 1, 1, 1, 1, 2, 1};

        assertEquals(3, solution.solution(A));
    }

    @Test
    public void peakExceed() throws Exception {

        int[] A = new int[]{1, 2, 1, 2, 1, 1, 1, 1, 2, 1, 2, 1};

        assertEquals(2, solution.solution(A));
    }

    @Test
    public void when3IsMoreThan4() throws Exception {

        int[] A = new int[]{1, 2, 1, 1, 2, 1, 3, 1, 2, 1};

        assertEquals(3, solution.solution(A));
    }

}