// task name: Flags


/**
 * Created by MD on 2017-10-29.
 */
public class Solution {
    //        WITH 'GOLD' SOLUTION

    //        counting possible flags and their places
    private boolean[] setFlag(int[] A) {
        boolean[] flags = new boolean[A.length];
        for (int i = 1; i < A.length - 1; i++) {
            if (A[i] > A[i - 1] && A[i] > A[i + 1]) {
                flags[i] = true;
            }
        }
        return flags;
    }

    //        describe on which i position is the next flag
    private int[] setNextFlag(int[] A) {
        boolean[] flags = setFlag(A);
        int[] nextFlag = new int[A.length];
        nextFlag[A.length - 1] = -1;
        for (int i = A.length - 2; i >= 0; i--) {
            if (flags[i]) {
                nextFlag[i] = i;
            } else {
                nextFlag[i] = nextFlag[i + 1];
            }
        }
        return nextFlag;
    }

    //    100/100 solving algorithm
    private int solve(int[] A) {

        int result = 0;

        int tempMaxFlagSize = (int) Math.sqrt(A.length - 4) + 2;
        int tempPos = 1;
        int[] nextFlag = setNextFlag(A);
        while (tempPos <= tempMaxFlagSize) {
            int tempCheck = 0;
            int tempResult = 0;
            while (tempCheck < A.length && tempResult < tempPos) {
                tempCheck = nextFlag[tempCheck];
                if (tempCheck == -1) {
                    break;
                }
                tempCheck += tempPos;
                tempResult++;
            }
            result = Math.max(result, tempResult);
            tempPos++;
        }

        return result;
    }

    public int solution(int[] A) {

        return solve(A);
    }
}


//        100/76 solution


//        int result = 0;
//
////        counting possible flags and their places
//        ArrayList<Integer> maxFlags = new ArrayList<>();
//        for (int i = 1; i < A.length - 1; i++) {
//            if (A[i] > A[i - 1] && A[i] > A[i + 1]) {
//                maxFlags.add(i);
//            }
//        }
//
//        if (maxFlags.size() >= 2) {
//            result++;
//        } else if (maxFlags.size() == 1) {
//            return 1;
//        } else {
//            return 0;
//        }


//        maximum number of possibilities will never exceed sqrt(A.length)
//        flags can be placed in a second place from front and in a second place from rear which means -4 and then SQRT
//        int tempMaxFlagSize = Math.min((int) Math.sqrt(A.length - 4) + 2, maxFlags.size());
//        for (int i = tempMaxFlagSize; i > 1; i--) {
//            int tempPos = maxFlags.size() - 1;
//            int tempCheck = maxFlags.size() - 2;
//            int tempResult = 1;
//            while (tempCheck >= 0 && tempResult < i) {
//                if (maxFlags.get(tempPos) - maxFlags.get(tempCheck) >= i) {
//                    tempResult++;
//                    tempPos = tempCheck;
//                    tempCheck--;
//                } else {
//                    tempCheck--;
//                }
//                if (tempResult == i) {
//                    return tempResult;
//                }
//            }
//            result = Math.max(result, tempResult);
//            if (result >= i) {
//                return result;
//            }
//        }