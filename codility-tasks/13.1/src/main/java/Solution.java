// task name: Ladder


public class Solution {

    public int[] solution(int[] A, int[] B) {

        int[] results = new int[A.length];
        int[] fiboList = fiboCheck(A.length);

        for (int i = 0; i < A.length; i++) {
            results[i] = fiboList[A[i]] % (int) Math.pow(2, B[i]);
        }

        return results;
    }

    private int[] fiboCheck(int n) {

        int divider = (int) Math.pow(2, 30);
        int[] fiboList = new int[n + 1];

        fiboList[1] = 1;

        if (n > 1) {
            fiboList[2] = 2;
            int temp1 = 1;
            int temp2 = 2;

            for (int i = 3; i < fiboList.length; i++) {
                fiboList[i] = (temp1 + temp2) % divider;
                temp1 = temp2;
                temp2 = fiboList[i];
            }
        }
        return fiboList;
    }
}
