import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by MD on 2017-11-03.
 */
public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int[] A = new int[]{4, 4, 5, 5, 1};
        int[] B = new int[]{3, 2, 4, 3, 1};

        int[] results = new int[]{5, 1, 8, 0, 1};

        assertArrayEquals(results, solution.solution(A, B));
    }

    @Test
    public void myCheck2() throws Exception {

        int[] A = new int[]{2, 2};
        int[] B = new int[]{1, 2};

        int[] results = new int[]{0, 2};

        assertArrayEquals(results, solution.solution(A, B));
    }


    @Test
    public void myCheck1() throws Exception {

        int[] A = new int[]{1};
        int[] B = new int[]{1};

        int[] results = new int[]{1};

        assertArrayEquals(results, solution.solution(A, B));
    }


    @Test
    public void max() throws Exception {

        int maxNum = 50000;

        int[] A = new int[maxNum];
        int[] B = new int[maxNum];

        for (int i = 0; i < A.length; i++) {
            A[i] = i;
            B[i] = 30;
        }

        assertArrayEquals(solution.solution(A, B), solution.solution(A, B));
    }

}