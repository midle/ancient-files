import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int[] A = new int[]{2, 1, 1, 2, 3, 1};

        int result = solution.solution(A);

        assertEquals(3, result);

    }

    @Test
    public void min0() throws Exception {

        int[] A = new int[]{};

        int result = solution.solution(A);

        assertEquals(0, result);

    }

    @Test
    public void min1() throws Exception {

        int[] A = new int[]{1};

        int result = solution.solution(A);

        assertEquals(1, result);

    }

    @Test
    public void min21() throws Exception {

        int[] A = new int[]{1, 1};

        int result = solution.solution(A);

        assertEquals(1, result);

    }

    @Test
    public void min22() throws Exception {

        int[] A = new int[]{1, 2};

        int result = solution.solution(A);

        assertEquals(2, result);

    }

    @Test
    public void max() throws Exception {

        int[] A = new int[100000];

        for (int i = 0; i < A.length; i++) {
            A[i] = i;
        }

        int result = solution.solution(A);

        assertEquals(100000, result);

    }

}