// task name: Distinct

import java.util.stream.IntStream;

public class Solution {

    public int solution(int[] A) {

        return (int) IntStream.of(A).distinct().count();
    }
}
