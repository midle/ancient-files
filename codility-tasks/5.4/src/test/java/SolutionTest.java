import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int[] A = new int[]{4, 2, 2, 5, 1, 5, 8};

        int result = solution.solution(A);

        assertEquals(1, result);
    }

    @Test
    public void min1() throws Exception {

        int[] A = new int[]{1,2};

        int result = solution.solution(A);

        assertEquals(0, result);
    }

    @Test
    public void min2() throws Exception {

        int[] A = new int[]{10,-50,10};

        int result = solution.solution(A);

        assertEquals(0, result);
    }

    @Test
    public void decrease1() throws Exception {

        int[] A = new int[]{-10,-50,-100,-200,-500};

        int result = solution.solution(A);

        assertEquals(3, result);
    }

}