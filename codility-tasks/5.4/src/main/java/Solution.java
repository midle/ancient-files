// task name: GenomicRangeQuery

public class Solution {

    public int solution(int[] A) {

        int result = 0;

        int maxSliceLength = 3;

        float res2;
        float res3;
        float tempRes = Integer.MAX_VALUE;

        if (A.length > 2) {

            for (int i = 0; i <= A.length - maxSliceLength; i++) {

                res2 = A[i] + A[i + 1];
                res3 = A[i] + A[i + 1] + A[i + 2];

                if (res2 / 2 < tempRes) {
                    tempRes = res2 / 2;
                    result = i;
                }

                if (res3 / 3 < tempRes) {
                    tempRes = res3 / 3;
                    result = i;
                }

//                to check the last 2
                if (i == A.length - maxSliceLength) {
                    res2 = A[i + 1] + A[i + 2];
                    if (res2 / 2 < tempRes) {
                        tempRes = res2 / 2;
                        result = i+1;
                    }
                }
            }
        } else {
            result = 0;
        }

        return result;
    }
}
