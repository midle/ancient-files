// task name: MaxProductOfThree

import java.util.Arrays;

public class Solution {

    public int solution(int[] A) {

        int result;

        Arrays.sort(A);

        result = A[A.length - 3] * A[A.length - 2] * A[A.length - 1];

        if (A[0] * A[1] * A[A.length - 1] > result) {
            result = A[0] * A[1] * A[A.length - 1];
        }

        return result;
    }
}
