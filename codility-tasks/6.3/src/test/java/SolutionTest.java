import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int[] A = new int[]{-3, 1, 2, -2, 5, 6};

        int result = solution.solution(A);

        assertEquals(60, result);
    }

    @Test
    public void min() throws Exception {

        int[] A = new int[]{2, 5, 6};

        int result = solution.solution(A);

        assertEquals(60, result);
    }

    @Test
    public void test0() throws Exception {

        int[] A = new int[]{0, 5, 6};

        int result = solution.solution(A);

        assertEquals(0, result);
    }

    @Test
    public void test1() throws Exception {

        int[] A = new int[]{1, 2, 3, 3, 2, 1};

        int result = solution.solution(A);

        assertEquals(18, result);
    }

    @Test
    public void twoMinus() throws Exception {

        int[] A = new int[]{1, 2, 3, 3, 2, 1, -6, -6};

        int result = solution.solution(A);

        assertEquals(108, result);
    }

    @Test
    public void threeMinus() throws Exception {

        int[] A = new int[]{1, 2, 3, 3, 2, 1, -6, -6, -6};

        int result = solution.solution(A);

        assertEquals(108, result);
    }

}