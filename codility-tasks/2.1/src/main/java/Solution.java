// task name: OddOccurencesInArray


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Solution {

    public int solution(int[] A) {

        HashMap<Integer, Integer> hashMap = new HashMap<>();

        int result = 0;

        for (int i = 0; i < A.length; i++) {

            if (!hashMap.containsKey(A[i])) {
                hashMap.put(A[i], 0);
            }

            hashMap.put(A[i], hashMap.get(A[i]) + 1);

        }

        Iterator iterator = hashMap.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry pair = (Map.Entry) iterator.next();
            if ((int) pair.getValue() % 2 == 1) {
                result = (int) pair.getKey();
            }
        }

        return result;
    }
}
