// task name: CountTriangles

import java.util.Arrays;

public class Solution {

    public int solution(int[] A) {

        if (A.length < 3) {
            return 0;
        }

        int result = 0;

        Arrays.sort(A);

        for (int i = 0; i < A.length; i++) {

            int end1 = i + 1;
            int end2 = i + 2;

            while (end1 < A.length - 1) {

                if (end2 < A.length && A[i] + A[end1] > A[end2]) {
                    end2++;
                } else {

//                    with sorted array we can iterate numbers to find the biggest (sroted) one
                    result += end2 - end1 - 1;
                    end1++;
                }
            }
        }
        return result;
    }
}
