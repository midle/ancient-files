import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by MD on 2017-11-12.
 */
public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int[] A = new int[]{10, 2, 5, 1, 8, 12};

        assertEquals(4, solution.solution(A));
    }

    @Test
    public void x() throws Exception {

        int[] A = new int[]{1, 2, 6, 8, 11, 12, 13};

        assertEquals(12, solution.solution(A));
    }

    @Test
    public void max() throws Exception {

        int[] A = new int[1000];

        for (int i = 0; i < A.length; i++) {
            A[i] = Integer.MAX_VALUE - i;
        }

        assertEquals(solution.solution(A), solution.solution(A));
    }

}