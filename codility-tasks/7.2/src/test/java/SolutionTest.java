import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their1() throws Exception {

        String S = "{[()()]}";

        int result = solution.solution(S);

        assertEquals(1, result);
    }

    @Test
    public void their0() throws Exception {

        String S = "([)()]";

        int result = solution.solution(S);

        assertEquals(0, result);
    }

    @Test
    public void empty() throws Exception {

        String S = "";

        int result = solution.solution(S);

        assertEquals(1, result);
    }

    @Test
    public void wrongFirst() throws Exception {

        String S = "}}{@$}^gfgd";

        int result = solution.solution(S);

        assertEquals(0, result);
    }

    @Test
    public void wrongNext1() throws Exception {

        String S = "{[()()]}()(){}{}{}(";

        int result = solution.solution(S);

        assertEquals(0, result);
    }

    @Test
    public void crrectNext1() throws Exception {

        String S = "{[()()]}()(){}{}{}()";

        int result = solution.solution(S);

        assertEquals(1, result);
    }

    @Test
    public void wrongNext2EmptyStack() throws Exception {

        String S = "{[()()]}}()(){}{}{}()";

        int result = solution.solution(S);

        assertEquals(0, result);
    }

}