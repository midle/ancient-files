import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by MD on 2017-11-10.
 */
public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int A[] = new int[]{3, 4, 5, 5, 2};
        int M = 5;

        assertEquals(9, solution.solution(M, A));
    }

    @Test
    public void x2() throws Exception {

        int A[] = new int[]{3, 4, 5, 4, 2};
        int M = 5;

        assertEquals(11, solution.solution(M, A));
    }

    @Test
    public void dbl() throws Exception {

        int A[] = new int[]{3, 4};
        int M = 4;

        assertEquals(3, solution.solution(M, A));
    }

    @Test
    public void x() throws Exception {

        int A[] = new int[]{0, 4, 5, 0};
        int M = 5;

        assertEquals(9, solution.solution(M, A));
    }

    @Test
    public void y() throws Exception {

        int A[] = new int[]{0, 1, 2, 3};
        int M = 5;

        assertEquals(10, solution.solution(M, A));
    }

    @Test
    public void max() throws Exception {

        int max = 100000;

        int A[] = new int[max];
        int M = max;

        for (int i = 0; i < A.length; i++) {
            A[i] = M;
        }

        assertEquals(A.length, solution.solution(M, A));
    }

    @Test
    public void maxDif() throws Exception {

        int max = 100000;

        int A[] = new int[max];
        int M = max;

        for (int i = 0; i < A.length; i++) {
            A[i] = i;
        }

        assertEquals(1000000000, solution.solution(M, A));
    }

    @Test
    public void min() throws Exception {

        int max = 1;

        int A[] = new int[max];
        int M = max;

        for (int i = 0; i < A.length; i++) {
            A[i] = M;
        }

        assertEquals(A.length, solution.solution(M, A));
    }

}