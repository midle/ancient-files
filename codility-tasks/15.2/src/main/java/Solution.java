// task name: CountDistinctSlices

public class Solution {

    public int solution(int M, int[] A) {

        int result = A.length;

        int[] numbers = new int[M + 1];

        int end = 0;

        for (int start = 0; start < A.length; start++) {
            numbers[A[start]] = 1;

            if (end < start) {
                end = start;
            }

            while (end < A.length - 1) {

                if (numbers[A[end + 1]] != 1) {
                    numbers[A[end + 1]] = 1;

//                    these are series numbers, so using this formula simplify counting greatly
//                    numbers are added like +1,+2,+3,+4 if series are kept
                    result += end - start + 1;

                    end++;
                    if (result > 1000000000) {
                        return 1000000000;
                    }
                } else {
                    break;
                }
            }

            numbers[A[start]] = 0;
        }


        return result;
    }


//    slow but EZ
//
//    public int solution(int M, int[] A) {
//
//        int result = A.length;
//
//        int[] numbers = new int[M + 1];
//
//        int end = 0;
//
//        for (int start = 0; start < A.length; start++) {
//
//
//            numbers[A[start]] = 1;
//            end = start;
//
//            while (end < A.length - 1) {
//
//                if (numbers[A[end + 1]] != 1) {
//                    numbers[A[end + 1]] = 1;
//                    end++;
//                    result ++;
//                    if (result > 1000000000) {
//                        return 1000000000;
//                    }
//                } else {
//                    break;
//                }
//            }
//
//            for (int i = start; i <= end; i++) {
//                numbers[A[i]] = 0;
//            }
//
//        }
//
//        return result;
//    }

}
