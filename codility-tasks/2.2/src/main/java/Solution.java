// task name: CyclicRotation

public class Solution {

    public int[] solution(int[] A, int K) {

        int[] newA = new int[A.length];


        for (int i = 0; i < A.length; i++) {

            newA[(i + K) % (A.length)] = A[i];
        }

        return newA;
    }
}
