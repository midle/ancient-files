import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int a = 5;
        int b = 11;
        int k = 2;

        int result = solution.solution(a, b, k);

        assertEquals(3, result);
    }

    @Test
    public void min() throws Exception {

        int a = 0;
        int b = 0;
        int k = 1;

        int result = solution.solution(a, b, k);

        assertEquals(1, result);
    }

    @Test
    public void their1() throws Exception {

        int a = 5;
        int b = 11;
        int k = 1;

        int result = solution.solution(a, b, k);

        assertEquals(7, result);
    }

    @Test
    public void single() throws Exception {

        int a = 0;
        int b = 1;
        int k = 1;

        int result = solution.solution(a, b, k);

        assertEquals(2, result);
    }

    @Test
    public void bigger() throws Exception {

        int a = 0;
        int b = 10;
        int k = 5;

        int result = solution.solution(a, b, k);

        assertEquals(3, result);
    }

    @Test
    public void bigger2() throws Exception {

        int a = 0;
        int b = 11;
        int k = 5;

        int result = solution.solution(a, b, k);

        assertEquals(3, result);
    }

    @Test
    public void bigger3() throws Exception {

        int a = 0;
        int b = 12;
        int k = 3;

        int result = solution.solution(a, b, k);

        assertEquals(5, result);
    }

    @Test
    public void testx() throws Exception {

        int a = 5;
        int b = 8;
        int k = 2;

        int result = solution.solution(a, b, k);

        assertEquals(2, result);
    }

    @Test
    public void testy() throws Exception {

        int a = 11;
        int b = 345;
        int k = 17;

        int result = solution.solution(a, b, k);

        assertEquals(20, result);
    }

    @Test
    public void test00() throws Exception {

        int a = 1;
        int b = 2;
        int k = 100;

        int result = solution.solution(a, b, k);

        assertEquals(0, result);
    }

}