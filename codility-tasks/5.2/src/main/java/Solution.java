// task name: PassingCars

public class Solution {

//    public int solution(int A, int B, int K) {
//
//        int result = 0;
//
//        if (A != 0 && A % K == 0) {
//            result++;
//        }
//
//        result += (B - A) / K;
//
//        return result;
//    }

    // for them if A=0, B=0 and K=1, result should be 1... so the tests and this solution is prepared for THAT occasion
    public int solution(int A, int B, int K) {

        int result = 0;

        result += B / K;
        result -= A / K;

        if (A == 0 || A % K == 0) {
            result++;
        }

        return result;
    }
}
