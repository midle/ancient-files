import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by MD on 2017-11-13.
 */
public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int[] A = new int[]{1, -2, 0, 9, -1, -2};

        assertEquals(8, solution.solution(A));
    }

    @Test
    public void x() throws Exception {

        int[] A = new int[]{1, 2, 3, -2, -1, 5};

        assertEquals(11, solution.solution(A));
    }

}