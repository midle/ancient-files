// task name: NumberSolitaire


public class Solution {

    public int solution(int[] A) {

        int[] result = new int[A.length];

        result[0] = A[0];

        for (int i = 1; i < A.length; i++) {

            int tempRes = Integer.MIN_VALUE;
            int counter = 1;

            while (counter <= 6 && i - counter >= 0) {
                tempRes = Math.max(tempRes, result[i - counter]);
                counter++;
            }

            result[i] = tempRes + A[i];
        }

        return result[result.length - 1];
    }
}
