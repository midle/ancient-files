import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int[] H = new int[]{8, 8, 5, 7, 9, 8, 7, 4, 8};

        int result = solution.solution(H);

        assertEquals(7, result);

    }

    @Test
    public void max() throws Exception {

        int[] H = new int[100000];

        for (int i = H.length-1; i >= 0; i--) {
            H[i] = i;
        }

        int result = solution.solution(H);

        assertEquals(100000, result);

    }

}