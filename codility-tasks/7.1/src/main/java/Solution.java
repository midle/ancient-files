// task name: StoneWall

import java.util.Stack;

public class Solution {

    public int solution(int[] H) {

        int result = 0;

        Stack<Integer> heightStack = new Stack<>();

        for (int i = 0; i < H.length; i++) {

            if (heightStack.empty() || H[i] > heightStack.peek()) {

                result++;
                heightStack.push(H[i]);

            } else if (H[i] < heightStack.peek()) {

                while (!heightStack.empty() && H[i] < heightStack.peek()) {
                    heightStack.pop();
                }

                if (!heightStack.empty() && H[i] != heightStack.peek() || heightStack.empty()) {
                    heightStack.push(H[i]);
                    result++;
                }
            }
        }

        return result;
    }
}
