// task name: Nesting

import java.util.Stack;

public class Solution {

    public int solution(String S) {

        Stack<Character> charStack = new Stack<>();

        if (S.length() == 0) {
            return 1;
        } else if (S.charAt(0) == ')') {
            return 0;
        } else {
            charStack.push(S.charAt(0));
        }

        for (int i = 1; i < S.length(); i++) {

            if (S.charAt(i) == '(') {
                charStack.push(S.charAt(i));
            } else if (!charStack.empty() && charStack.peek() == '(') {
                charStack.pop();
            } else {
                return 0;
            }
        }

        if (charStack.empty()) {
            return 1;
        } else {
            return 0;
        }
    }
}
