import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by MD on 2017-10-18.
 */
public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int[] A = new int[]{3, 2, -6, 4, 0};

        assertEquals(5, solution.solution(A));
    }

    @Test
    public void minSize() throws Exception {

        int[] A = new int[]{-3};

        assertEquals(-3, solution.solution(A));
    }

    @Test
    public void minus1() throws Exception {

        int[] A = new int[]{-3, -2, -3};

        assertEquals(-2, solution.solution(A));
    }

    @Test
    public void down() throws Exception {

        int[] A = new int[]{3, 2, 1};

        assertEquals(6, solution.solution(A));
    }

    @Test
    public void up() throws Exception {

        int[] A = new int[]{1, 2, 3};

        assertEquals(6, solution.solution(A));
    }

    @Test
    public void only1() throws Exception {

        int[] A = new int[]{-1, -2, 4, -5};

        assertEquals(4, solution.solution(A));
    }

}