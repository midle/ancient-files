// task name: MaxDoubleSliceSum


public class Solution {

    public int solution(int[] A) {

        int tempSolution = 0;

        int solution = Integer.MIN_VALUE;

        for (int i = 0; i < A.length; i++) {

            tempSolution = Math.max(0, tempSolution + A[i]);
            if (solution < 0) {
                if (A[i] > solution) {
                    solution = A[i];
                }
            } else {
                solution = Math.max(solution, tempSolution);
            }
        }
        return solution;
    }
}
