// task name: BinaryGap

public class Solution {

    public int solution(int N) {

        int x = 0;

        int tempX = 0;

        String binaryString = Integer.toBinaryString(N);

        StringBuilder stringBuilder = new StringBuilder(binaryString);


        for (int i = 0; i < stringBuilder.length(); i++) {

            if (Character.getNumericValue(stringBuilder.charAt(i)) == 0) {

                tempX++;

            } else {

                if (tempX > x) {
                    x = tempX;
                }

                tempX = 0;
            }
        }

        return x;
    }
}
