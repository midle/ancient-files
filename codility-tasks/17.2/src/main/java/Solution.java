// task name: MinAbsSum


// with shortcut usage of : https://codility.com/media/train/solution-min-abs-sum.pdf

public class Solution {

    public int solution(int[] A) {

        if (A.length == 0) {
            return 0;
        }

        int result = Integer.MAX_VALUE;

//        just for initialization, lower number than the lowest possible input number
        int maxEle = -101;
        int sum = 0;

//        we can choose between -1 or 1 multiplier, so we can make already abs values as title say AND start finding differences
        for (int i = 0; i < A.length; i++) {
            A[i] = Math.abs(A[i]);
            maxEle = Math.max(maxEle, A[i]);
            sum += A[i];
        }

        int[] counter = new int[maxEle + 1];
        for (int aA : A) {
            counter[aA]++;
        }

        int[] canGetSum = new int[sum + 1];
        for (int i = 1; i < canGetSum.length; i++) {
            canGetSum[i] = -1;
        }

        for (int i = 1; i < counter.length; i++) {
            if (counter[i] > 0) {
                for (int j = 0; j < sum; j++) {
                    if (canGetSum[j] >= 0) {
                        canGetSum[j] = counter[i];
                    } else if (j >= i && canGetSum[j - i] > 0) {
                        canGetSum[j] = canGetSum[j - i] - 1;
                    }
                }
            }
        }

        for (int i = 0; i < sum / 2 + 1; i++) {
            if (canGetSum[i] >= 0) {
                result = Math.min(result, sum - 2 * i);
            }
        }

        return result;
    }
}
