import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int[] A = new int[]{1, 5, 2, -2};

        assertEquals(0, solution.solution(A));
    }

    @Test
    public void single() throws Exception {

        int[] A = new int[]{5};

        assertEquals(5, solution.solution(A));
    }

    @Test
    public void empty() throws Exception {

        int[] A = new int[]{};

        assertEquals(0, solution.solution(A));
    }

    @Test
    public void max() throws Exception {

        int[] A = new int[20000];
        for (int i = 0; i < A.length; i++) {
            A[i] = 100;
        }

        assertEquals(0, solution.solution(A));
    }

}