// task name: TieRopes


public class Solution {

    public int solution(int K, int[] A) {

        int result = 0;

        int tempLength = 0;

        for (int i = 0; i < A.length; i++) {

            tempLength += A[i];

            if (tempLength >= K) {
                tempLength = 0;
                result++;
            }
        }
        return result;
    }
}
