import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by MD on 2017-11-13.
 */
public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int[] A = new int[]{1, 2, 3, 4, 1, 1, 3};
        int K = 4;

        assertEquals(3, solution.solution(K, A));
    }

    @Test
    public void x() throws Exception {

        int[] A = new int[]{4,3,2,3,2,2};
        int K = 4;

        assertEquals(3, solution.solution(K, A));
    }

    @Test
    public void min1() throws Exception {

        int[] A = new int[]{1};
        int K = 1;

        assertEquals(1, solution.solution(K, A));
    }

    @Test
    public void min0() throws Exception {

        int[] A = new int[]{1};
        int K = 2;

        assertEquals(0, solution.solution(K, A));
    }

    @Test
    public void max() throws Exception {

        int[] A = new int[100000];
        int K = 1;

        for (int i = 0; i < A.length; i++) {
            A[i] = 1;
        }

        assertEquals(A.length, solution.solution(K, A));
    }

}