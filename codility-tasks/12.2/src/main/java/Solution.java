// task name: CommonPrimeDivisor



/**
 * Created by MD on 2017-11-03.
 */
public class Solution {


//    100% correctness, but only for low numbers, as arrays for N~intMAX are too big
//    public int solution(int[] A, int[] B) {
//
//        int result = 0;
//
//        ArrayList<Integer> tempA;
//        ArrayList<Integer> tempB;
//
//        for (int i = 0; i < A.length; i++) {
//
//            tempA = findAllPrimeDivisors(A[i]);
//            tempB = findAllPrimeDivisors(B[i]);
//
//            System.out.println(tempA);
//            System.out.println(tempB);
//
//            if (tempA.size() == tempB.size() && tempA.equals(tempB)) {
//                result++;
//            }
//        }
//        return result;
//    }
//
//    public int[] findLowestPrimeDivisors(int x) {
//
//        int[] LPD = new int[x + 1];
//
//        int i = 2;
//        while (i * i <= x) {
//            if (LPD[i] == 0) {
//                int k = i * i;
//                while (k <= x) {
//                    if (LPD[k] == 0) {
//                        LPD[k] = i;
//                    }
//                    k += i;
//                }
//            }
//            i++;
//        }
//
//        return LPD;
//    }
//
//    public ArrayList<Integer> findAllPrimeDivisors(int n) {
//
//        int[] A = findLowestPrimeDivisors(n);
//
//        ArrayList<Integer> APD = new ArrayList<>();
//
//        while (A[n] > 0) {
//            if (!APD.contains(A[n])) {
//                APD.add(A[n]);
//            }
//            n /= A[n];
//        }
//        if (!APD.contains(n)) {
//            APD.add(n);
//        }
//
//        return APD;
//    }


    public int solution(int[] A, int[] B) {

        int result = 0;

        for (int i = 0; i < A.length; i++) {
            if (ifContainAnother(A[i], B[i])) {
                result++;
            }
        }

        return result;
    }

    //    greatest common divisor for two numbers, share their every prime divisor
//    and because of that if number share another common divisor with GCD it is another prime divisor not contained by other number
    private int GCD(int n, int m) {

        if (n % m == 0) {
            return m;
        } else {
            return GCD(m, n % m);
        }
    }

    private int anotherPrimeDivisor(int n, int m) {

        while (n != 1) {
            int GCDofNumberAndGCD = GCD(n, m);
            if (GCDofNumberAndGCD == 1) {
                break;
            }
            n /= GCDofNumberAndGCD;
        }
        return n;
    }

    private boolean ifContainAnother(int a, int b) {

        if (anotherPrimeDivisor(a, GCD(a, b)) != 1) {
            return false;
        } else if (anotherPrimeDivisor(b, GCD(a, b)) != 1) {
            return false;
        }

        return true;
    }
}
