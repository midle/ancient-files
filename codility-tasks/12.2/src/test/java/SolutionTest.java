import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by MD on 2017-11-03.
 */
public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int[] A = new int[]{15, 10, 3};
        int[] B = new int[]{75, 30, 5};

        assertEquals(1, solution.solution(A, B));
    }

    @Test
    public void their2() throws Exception {

        int[] A = new int[]{6059, 551};
        int[] B = new int[]{442307, 303601};

        assertEquals(2, solution.solution(A, B));
    }

    @Test
    public void single() throws Exception {

        int[] A = new int[]{15};
        int[] B = new int[]{15};

        assertEquals(1, solution.solution(A, B));
    }

    @Test
    public void singleMAX() throws Exception {

        int[] A = new int[]{Integer.MAX_VALUE};
        int[] B = new int[]{Integer.MAX_VALUE};

        assertEquals(1, solution.solution(A, B));
    }

    @Test
    public void maxLen() throws Exception {

        int maxLen = 6000;

        int[] A = new int[maxLen];
        int[] B = new int[maxLen];

        for (int i = 0; i < A.length; i++) {
            A[i] = Integer.MAX_VALUE;
            B[i] = Integer.MAX_VALUE;
        }

        assertEquals(maxLen, solution.solution(A, B));
    }

}