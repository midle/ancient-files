// task name: CountFactors


public class Solution {

    public int solution(int N) {

        if (N == 1) {
            return 1;
        }

        int tempDiv = (int) Math.sqrt(N);
        int counter = 0;

        for (int i = 1; i <= tempDiv; i++) {
            if (N % i == 0) {
                counter++;
            }
        }
        counter *= 2;

        if (tempDiv * tempDiv == N) {
            counter--;
        }

        return counter;
    }
}
