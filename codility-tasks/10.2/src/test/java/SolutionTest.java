import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by MD on 2017-10-20.
 */
public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int N = 24;

        assertEquals(8,solution.solution(N));
    }

    @Test
    public void min1() throws Exception {

        int N = 1;

        assertEquals(1,solution.solution(N));
    }

    @Test
    public void min2() throws Exception {

        int N = 2;

        assertEquals(2,solution.solution(N));
    }

    @Test
    public void prime13() throws Exception {

        int N = 13;

        assertEquals(2,solution.solution(N));
    }

    @Test
    public void norm100() throws Exception {

        int N = 100;

        assertEquals(9,solution.solution(N));
    }

    @Test
    public void norm10() throws Exception {

        int N = 10;

        assertEquals(4,solution.solution(N));
    }

}