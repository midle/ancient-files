import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by MD on 2017-11-12.
 */
public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int[] A = new int[]{1, 3, 7, 9, 9};
        int[] B = new int[]{5, 6, 8, 9, 10};

        assertEquals(3, solution.solution(A, B));
    }

    @Test
    public void max() throws Exception {

        int[] A = new int[30000];
        int[] B = new int[30000];
        for (int i = 0; i < A.length; i++) {
            A[i] = i;
            B[i] = i;
        }

        assertEquals(30000, solution.solution(A, B));
    }

}