// task name: MaxNonoverlappingSegments


public class Solution {

    public int solution(int[] A, int[] B) {

        if (A.length == 0) {
            return 0;
        }

        int result = 1;

        int lineStart;
        int lineEnd = B[0];

        for (int i = 1; i < A.length; i++) {

            lineStart = A[i];

            if (lineStart > lineEnd) {
                lineEnd = B[i];
                result++;
            }
        }

        return result;
    }
}
