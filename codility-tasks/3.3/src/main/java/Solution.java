// task name: FrogJmp

import java.util.ArrayList;
import java.util.Collections;

public class Solution {

    public int solution(int[] A) {

        int result = 1;

        ArrayList<Integer> arrayList = new ArrayList<>();

        if (A.length != 0) {


            for (int i = 0; i < A.length; i++) {
                arrayList.add(A[i]);
            }

            Collections.sort(arrayList);

            if (arrayList.get(0) != 1) {
                result = 1;
            } else if (arrayList.get(arrayList.size() - 1) != arrayList.size() + 1) {
                result = arrayList.size() + 1;
            } else if (arrayList.size() > 1) {


                for (int i = 0; i < arrayList.size() - 1; i++) {

                    if (arrayList.get(i + 1) - arrayList.get(i) != 1) {
                        result = arrayList.get(i) + 1;
                        break;
                    }
                }
            } else if (arrayList.size() == 1) {
                result = 2;
            } else {
                result = 1;
            }
        }

        return result;
    }
}
