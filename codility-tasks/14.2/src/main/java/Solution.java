// task name: NailingPlanks


// used Yavor Yanev help

public class Solution {

    public int solution(int[] A, int[] B, int[] C) {

        int result = -1;

        int min = 1;
        int max = A.length;

        while (min <= max) {

            int midCheck = (min + max) / 2;

            if (ifEnoughNails(midCheck, A, B, C)) {
                max = midCheck - 1;
                result = midCheck;
            } else {
                min = midCheck + 1;
            }
        }

        return result;
    }

    private boolean ifEnoughNails(int midCheck, int[] A, int[] B, int[] C) {

        int[] nails;
        if (C.length >= A.length) {
            nails = new int[C.length * 2 + 1];
        } else {
            nails = new int[A.length * 2 + 1];
        }

        for (int i = 0; i < midCheck; i++) {
            nails[C[i]] = 1;
        }

//        from 1 and at index 0 it is already sth
        for (int i = 1; i < nails.length; i++) {
            nails[i] += nails[i - 1];
        }

        for (int i = 0; i < B.length; i++) {
//            if 0 nails were added it means it was not enough
            if (nails[B[i]] - nails[A[i] - 1] == 0) {
                return false;
            }
        }

        return true;
    }
}