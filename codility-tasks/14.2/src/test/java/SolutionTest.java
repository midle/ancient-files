import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by MD on 2017-11-08.
 */
public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int[] A = new int[]{1, 4, 5, 8};
        int[] B = new int[]{4, 5, 9, 10};
        int[] C = new int[]{4, 6, 7, 10, 2};

        assertEquals(4, solution.solution(A, B, C));
    }

    @Test
    public void single1() throws Exception {

        int[] A = new int[]{1};
        int[] B = new int[]{2};
        int[] C = new int[]{2};

        assertEquals(1, solution.solution(A, B, C));
    }

    @Test
    public void single0() throws Exception {

        int[] A = new int[]{1};
        int[] B = new int[]{1};
        int[] C = new int[]{2};

        assertEquals(-1, solution.solution(A, B, C));
    }

    @Test
    public void extremeNum() throws Exception {

        int maxNum = 30000;

        int[] A = new int[maxNum];
        int[] B = new int[maxNum];
        int[] C = new int[1000201];

        for (int i = 0; i < maxNum; i++) {
            A[i] = 1;
            B[i] = maxNum * 2 - 1;
            C[i] = maxNum * 2;
        }

        for (int i = 0; i < C.length; i++) {
            C[i] = maxNum * 2;
        }

        assertEquals(-1, solution.solution(A, B, C));
    }

}