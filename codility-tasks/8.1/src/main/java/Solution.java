// task name: EquiLeader

import java.util.Stack;

public class Solution {

    public int solution(int[] A) {

        int solution = 0;

        Stack<Integer> tempLeaderStack = new Stack();

        for (int i = 0; i < A.length - 1; i++) {

            if (tempLeaderStack.empty()) {
                tempLeaderStack.push(A[i]);
            } else {
                if (tempLeaderStack.peek() == A[i]) {
                    tempLeaderStack.push(A[i]);
                } else {
                    tempLeaderStack.pop();
                }
            }
        }

        int counter = 0;

        for (int i = 0; i < A.length; i++) {
            if (!tempLeaderStack.empty() && tempLeaderStack.peek() == A[i]) {
                counter++;
            }
        }

        int leader;

        if (counter > A.length / 2) {
            leader = tempLeaderStack.peek();
        } else {
            return 0;
        }

        int solutionsCounter = 0;

        for (int i = 0; i < A.length; i++) {

            if (A[i] == leader) {
                solutionsCounter++;
            }
            if (solutionsCounter > (i + 1) / 2 && counter - solutionsCounter > (A.length - i - 1) / 2) {
                solution++;
            }
        }
        return solution;
    }
}