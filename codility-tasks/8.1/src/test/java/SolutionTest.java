import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by MD on 2017-10-07.
 */
public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their2() throws Exception {

        int[] A = new int[]{4, 3, 4, 4, 4, 2};

        int result = solution.solution(A);

        assertEquals(2, result);
    }

    @Test
    public void mine2() throws Exception {

        int[] A = new int[]{5, 6, 6, 5, 5, 5};

        int result = solution.solution(A);

        assertEquals(2, result);
    }

    @Test
    public void empty() throws Exception {

        int[] A = new int[]{};

        int result = solution.solution(A);

        assertEquals(0, result);
    }

    @Test
    public void single() throws Exception {

        int[] A = new int[]{1};

        int result = solution.solution(A);

        assertEquals(0, result);
    }

    @Test
    public void double0() throws Exception {

        int[] A = new int[]{1, 2};

        int result = solution.solution(A);

        assertEquals(0, result);
    }

    @Test
    public void double1() throws Exception {

        int[] A = new int[]{1, 1};

        int result = solution.solution(A);

        assertEquals(1, result);
    }

    @Test
    public void next() throws Exception {

        int[] A = new int[]{0, 1, 2, 3, 4, 5, 6, 7};

        int result = solution.solution(A);

        assertEquals(0, result);
    }

    @Test
    public void next2() throws Exception {

        int[] A = new int[]{1, 2, 3, 4, 5};

        int result = solution.solution(A);

        assertEquals(0, result);
    }

    @Test
    public void highValues() throws Exception {

        int[] A = new int[]{Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE};

        int result = solution.solution(A);

        assertEquals(1, result);
    }

    @Test
    public void backward() throws Exception {

        int[] A = new int[]{1, 2, 3, 3, 2, 1};

        int result = solution.solution(A);

        assertEquals(0, result);
    }

    @Test
    public void their3() throws Exception {

        int[] A = new int[]{4, 4, 2, 5, 3, 4, 4, 4};

        int result = solution.solution(A);

        assertEquals(3, result);
    }

}