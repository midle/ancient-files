import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their1() throws Exception {

        int[] tab = {1, 3, 6, 4, 1, 2};

        int result = solution.solution(tab);

        assertEquals(5, result);
    }

    @Test
    public void their2() throws Exception {

        int[] tab = {1, 2, 3};

        int result = solution.solution(tab);

        assertEquals(4, result);
    }

    @Test
    public void their3() throws Exception {

        int[] tab = {-1, -3};

        int result = solution.solution(tab);

        assertEquals(1, result);
    }

    @Test
    public void min1() throws Exception {

        int[] tab = {1, 3};

        int result = solution.solution(tab);

        assertEquals(2, result);
    }

    @Test
    public void min2() throws Exception {

        int[] tab = {1};

        int result = solution.solution(tab);

        assertEquals(2, result);
    }

    @Test
    public void max1() throws Exception {

        int[] tab = new int[100000];

        for (int i = 0; i < tab.length; i++) {
            tab[i] = 100000000;
        }

        int result = solution.solution(tab);

        assertEquals(1, result);
    }

    @Test
    public void max2() throws Exception {

        int[] tab = new int[100000];

        for (int i = 0; i < tab.length; i++) {
            tab[i] = i + 1;
        }

        int result = solution.solution(tab);

        assertEquals(100001, result);
    }

}