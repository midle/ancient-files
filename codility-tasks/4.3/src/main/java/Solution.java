// task name: MissingInteger

import java.util.*;

public class Solution {

/*
*THIS IS 100% CORRECT SOLUTION, QUITE EASY using TreeMap, but efficiency is too low (about 15% only)
 */
//    TreeMap<Integer, Integer> treeMap = new TreeMap<>();
//
//    public int solution(int[] A) {
//
//        for (int temp : A
//                ) {
//            treeMap.put(temp, temp);
//        }
//
//        int tempMin = 0;
//
//        if (treeMap.lastKey() < 1) {
//            return 1;
//        }
//
//        for (Integer temp : treeMap.keySet()
//                ) {
//
//            if (temp >= 1) {
//                if (tempMin == temp - 1) {
//                    tempMin = temp;
//                } else {
//                    return tempMin + 1;
//                }
//            }
//        }
//        return tempMin + 1;
//    }


    /*
    * THIS IS COMPLETE 100% solution
     */

    private ArrayList<Integer> arrayList = new ArrayList<>();

    public int solution(int[] A) {

        Arrays.sort(A);

        for (int temp : A
                ) {
            arrayList.add(temp);
        }

        int tempMin = 0;

        if (arrayList.get(A.length - 1) < 1) {
            return 1;
        }

        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i) >= 1) {
                if (tempMin >= arrayList.get(i) - 1) {
                    tempMin = arrayList.get(i);
                } else {
                    return tempMin + 1;
                }
            }
        }
        return tempMin + 1;
    }
}
