import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by MD on 2017-10-16.
 */
public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int[] A = new int[]{3, 2, 6, -1, 4, 5, -1, 2};

        assertEquals(17, solution.solution(A));
    }

    @Test
    public void t0() throws Exception {

        int[] A = new int[]{1, 2, 3};

        assertEquals(0, solution.solution(A));
    }

    @Test
    public void t20() throws Exception {

        int[] A = new int[]{};

        assertEquals(0, solution.solution(A));
    }

    @Test
    public void m1() throws Exception {

        int[] A = new int[]{-1, -2, -3, -4, -5, 1};

        assertEquals(0, solution.solution(A));
    }

    @Test
    public void m2() throws Exception {

        int[] A = new int[]{1, -2, 3, -4, 5};

        assertEquals(3, solution.solution(A));
    }

}