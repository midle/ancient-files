// task name: MaxProfit

public class Solution {

//    KADANE

    public int solution(int[] A) {

        int solution = 0;

        if (A.length < 3) {
            return 0;
        }

        int[] maxStartHere = new int[A.length];
        int[] maxEndHere = new int[A.length];

        int goRight = 0;
        for (int i = 1; i < A.length - 2; i++) {

            goRight = Math.max(0, goRight + A[i]);
            maxEndHere[i] = goRight;
        }

        int goLeft = 0;
        for (int i = A.length - 2; i > 1; i--) {

            goLeft = Math.max(0, goLeft + A[i]);
            maxStartHere[i] = goLeft;
        }

        for (int i = 0; i < A.length - 2; i++) {

            solution = Math.max(solution, maxEndHere[i] + maxStartHere[i + 2]);
        }

        return solution;
    }
}
