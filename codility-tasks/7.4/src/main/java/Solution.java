// task name: Fish

import java.util.Stack;

public class Solution {

    Stack<Fish> fishStack = new Stack<>();

    private class Fish {

        int size;
        int direction;

        public Fish(int size, int direction) {
            this.size = size;
            this.direction = direction;
        }

        public int getSize() {
            return size;
        }

        public int getDirection() {
            return direction;
        }
    }

    private void addFishToStack(int size, int dir) {
        fishStack.push(new Fish(size, dir));
    }

    int solution(int A[], int B[]) {


        if (A.length != B.length) {
            return 0;

        } else {
            for (int i = 0; i < A.length; i++) {
                if (!fishStack.isEmpty() && fishStack.peek().getDirection() == 1) {
                    if (B[i] == 1) {
                        addFishToStack(A[i], B[i]);
                    } else {
                        while (!fishStack.isEmpty() && fishStack.peek().getDirection() == 1 && fishStack.peek().getSize() < A[i]) {
                            fishStack.pop();
                        }
                        if (!fishStack.isEmpty() && fishStack.peek().getDirection() == 0) {
                            addFishToStack(A[i], B[i]);
                        } else if (fishStack.isEmpty()) {
                            addFishToStack(A[i], B[i]);
                        }
                    }
                } else {
                    addFishToStack(A[i], B[i]);
                }
            }
        }
        return fishStack.size();
    }
}
