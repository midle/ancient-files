import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their2() throws Exception {

        int[] A = new int[]{4, 3, 2, 1, 5};
        int[] B = new int[]{0, 1, 0, 0, 0};

        int result = solution.solution(A, B);

        assertEquals(2, result);
    }

    @Test
    public void my1() throws Exception {

        int[] A = new int[]{2, 1, 3};
        int[] B = new int[]{1, 0, 0};

        int result = solution.solution(A, B);

        assertEquals(1, result);
    }

    @Test
    public void empty() throws Exception {

        int[] A = new int[]{};
        int[] B = new int[]{};

        int result = solution.solution(A, B);

        assertEquals(0, result);
    }

    @Test
    public void AIsNotB() throws Exception {

        int[] A = new int[]{4, 3, 2, 1, 5};
        int[] B = new int[]{0, 1, 0};

        int result = solution.solution(A, B);

        assertEquals(0, result);
    }
}