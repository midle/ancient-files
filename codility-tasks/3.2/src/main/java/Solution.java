// task name: TapeEquilibrium

public class Solution {


    public int solution(int[] A) {

        int solution;


        if (A.length < 2) {
            return 0;

        } else if (A.length == 2) {
            return Math.abs(A[0] - A[1]);

        } else {
            int temp1 = A[0];
            int temp2 = 0;
            int temp22 = 0;
            int tempSolution;


            for (int i = 1; i < A.length; i++) {
                temp22 += A[i];
                temp2 += A[i];
            }

            solution = Math.abs(temp1 - temp22);

            for (int i = 1; i < A.length - 1; i++) {
                temp1 += A[i];
                temp2 -= A[i];
                tempSolution = Math.abs(temp1 - temp2);
                if (tempSolution < solution) {
                    solution = tempSolution;
                }
            }
        }

        return solution;
    }
}
