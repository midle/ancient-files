import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

public class SolutionTest {

    Solution solution = new Solution();


    @Test
    public void solution0() throws Exception {

        int[] tab = new int[1];

        for (int i = 0; i < tab.length; i++) {
            tab[i] = 1;
        }

        int result = solution.solution(tab);

        assertEquals(0, result);
    }

    @Test
    public void solution10() throws Exception {

        int[] tab = new int[10];

        for (int i = 0; i < tab.length; i++) {
            tab[i] = 1;
        }

        int result = solution.solution(tab);

        assertEquals(0, result);
    }

    @Test
    public void solutionTheir() throws Exception {

        int[] tab = {3, 1, 2, 4, 3};

        int result = solution.solution(tab);

        assertEquals(1, result);
    }

    @Test
    public void solution3() throws Exception {

        int[] tab = {-10, -20, -30, -40, 100};

        int result = solution.solution(tab);

        assertEquals(20, result);
    }

    @Test
    public void solutionMy() throws Exception {

        int[] tab = {5, 4, 6, 4, 8};

        int result = solution.solution(tab);

        assertEquals(3, result);
    }

    @Test
    public void solutionMy2() throws Exception {

        int[] tab = {0, 122};

        int result = solution.solution(tab);

        assertEquals(122, result);
    }

    @Test
    public void solutionminVal() throws Exception {

        int[] tab = new int[100000];

        for (int i = 0; i < tab.length; i++) {
            tab[i] = -1000;
        }

        int result = solution.solution(tab);

        assertEquals(0, result);
    }

    @Test
    public void solutionmaxVal() throws Exception {

        int[] tab = new int[100000];

        for (int i = 0; i < tab.length; i++) {
            tab[i] = 1000;
        }

        int result = solution.solution(tab);

        assertEquals(0, result);
    }

    //just for random testing - processing time checking -/ so solution = solution :)
    @Test
    public void solutionRandomVal() throws Exception {

        Random random = new Random();

        int[] tab = new int[100000];

        for (int i = 0; i < tab.length; i++) {
            tab[i] = random.nextInt(2000) - 1000;
        }

        int result = solution.solution(tab);

        assertEquals(result, result);
    }

}