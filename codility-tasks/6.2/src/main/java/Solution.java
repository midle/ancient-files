// task name: Triangle

import java.util.Arrays;

public class Solution {

    public int solution(int[] A) {

        int result = 0;

        Arrays.sort(A);

        if (A.length >= 3) {

            float a;
            float b;
            float c;

            for (int i = 0; i < A.length - 2; i++) {
                a = A[i];
                b = A[i + 1];
                c = A[i + 2];
                if (c < a + b) {
                    result = 1;
                    break;
                }
            }
        }

        return result;
    }
}
