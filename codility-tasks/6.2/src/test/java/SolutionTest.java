import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their1() throws Exception {

        int[] A = new int[]{10, 2, 5, 1, 8, 20};

        int result = solution.solution(A);

        assertEquals(1, result);
    }

    @Test
    public void their0() throws Exception {

        int[] A = new int[]{10, 50, 5, 1};

        int result = solution.solution(A);

        assertEquals(0, result);
    }

    @Test
    public void test0() throws Exception {

        int[] A = new int[]{};

        int result = solution.solution(A);

        assertEquals(0, result);
    }

    @Test
    public void test2() throws Exception {

        int[] A = new int[]{1, 2};

        int result = solution.solution(A);

        assertEquals(0, result);
    }

    @Test
    public void maxes() throws Exception {

        int[] A = new int[]{Integer.MAX_VALUE - 2, Integer.MAX_VALUE - 1, Integer.MAX_VALUE};

        int result = solution.solution(A);

        assertEquals(1, result);
    }

}