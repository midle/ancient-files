import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by MD on 2017-11-02.
 */
public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int N = 10;
        int M = 4;

        assertEquals(5, solution.solution(N, M));
    }

    @Test
    public void overflow() throws Exception {

        int N = 1000000000;
        int M = 3;

        assertEquals(1000000000, solution.solution(N, M));
    }

}