// task name: ChocolatesByNumber


public class Solution {

    public int solution(int N, int M) {

        int result = 0;

        result = (int) (leastCommonMultipleLCM(N, M) / M);

        return result;
    }

    private double greatestCommonDivisorGCD(double n, double m) {

        if (n % m == 0) {
            return m;
        } else {
            return greatestCommonDivisorGCD(m, n % m);
        }
    }

    private double leastCommonMultipleLCM(double n, double m) {

        double lcm = n * m / greatestCommonDivisorGCD(n, m);

        return lcm;
    }
}
