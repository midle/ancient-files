// task name: CountSemiPrimes

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by MD on 2017-10-31.
 */
public class Solution {

    public int[] solution(int N, int[] P, int[] Q) {

        int[] result = new int[P.length];

//        checkIfAnyPrime Exists
        if (N < 4) {
            for (int i = 0; i < result.length; i++) {
                result[i] = 0;
                return result;
            }
        }

        ArrayList<Integer> semiprimesList = getSemiprimes(N);
        Collections.sort(semiprimesList);

        int[] semiprimesCounter = new int[N + 1];
        semiprimesCounter[0] = 0;

        int semiprimeNum = 0;
        for (int i = 1; i < semiprimesCounter.length; i++) {
            if (semiprimeNum == semiprimesList.size()) {
                semiprimesCounter[i] = semiprimeNum;
            } else {
                if (i == semiprimesList.get(semiprimeNum)) {
                    semiprimeNum++;
                }
                semiprimesCounter[i] = semiprimeNum;
            }
        }

        for (int i = 0; i < P.length; i++) {
            result[i] = semiprimesCounter[Q[i]] - semiprimesCounter[P[i] - 1];
        }

        return result;
    }

    private ArrayList<Integer> getSemiprimes(int N) {

        ArrayList<Integer> semiprimes = new ArrayList<>();

        boolean[] sieve = new boolean[N + 1];
        sieve[0] = true;
        sieve[1] = true;

        int i = 2;
        while (i * i <= N) {
            if (!sieve[i]) {
                int k = i * i;
                while (k <= N) {
                    sieve[k] = true;
                    k += i;
                }
            }
            i++;
        }

        int ii = 2;
        while (ii * ii <= N) {
            if (!sieve[ii]) {
                int k = ii * ii;
                while (k <= N) {
                    if (k % ii == 0 && !sieve[k / ii]) {
                        semiprimes.add(k);
                    }
                    k += ii;
                }
            }
            ii++;
        }

        return semiprimes;
    }
}
