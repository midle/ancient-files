import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by MD on 2017-10-31.
 */
public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int N = 1231231;
        int[] P = new int[]{1, 4, 16};
        int[] Q = new int[]{26, 10, 20};

        int[] result = new int[]{10, 4, 0};

        assertArrayEquals(result, solution.solution(N, P, Q));
    }

    @Test
    public void noSemiprime() throws Exception {

        int N = 3;
        int[] P = new int[]{1};
        int[] Q = new int[]{4};

        int[] result = new int[]{0};

        assertArrayEquals(result, solution.solution(N, P, Q));
    }

    @Test
    public void samenumberNotSemiprime() throws Exception {

        int N = 15;
        int[] P = new int[]{11};
        int[] Q = new int[]{11};

        int[] result = new int[]{0};

        assertArrayEquals(result, solution.solution(N, P, Q));
    }

    @Test
    public void samenumberSemiprime() throws Exception {

        int N = 15;
        int[] P = new int[]{10};
        int[] Q = new int[]{10};

        int[] result = new int[]{1};

        assertArrayEquals(result, solution.solution(N, P, Q));
    }

}