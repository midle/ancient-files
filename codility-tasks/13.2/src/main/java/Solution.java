// task name: FibFrog

import java.util.ArrayList;

/**
 * Created by MD on 2017-11-04.
 */
public class Solution {

    public int solution(int[] A) {

//        it is really easier wit this x<
        int[] newA = new int[A.length + 1];
        newA[A.length] = 1;
        for (int i = 0; i < A.length; i++) {
            newA[i] = A[i];
        }

        ArrayList<Integer> fibList = getFibNumList(newA.length);
        int[] jumpList = getInitializedJumpArray(newA, fibList);

//        for fast return
        if (jumpList[A.length] == 1 || A.length <= 2) {
            return 1;
        }


//        solving algo
        for (int i = 2; i < newA.length; i++) {

            if (newA[i] == 0 || jumpList[i] == 1) {
                continue;
            }

            int minJump = Integer.MAX_VALUE;

            for (int entry :
                    fibList) {

                if (i - entry < 0) {
                    break;
                }

                if (jumpList[i - entry] == 0 || minJump < jumpList[i - entry]) {
                    continue;
                }

                minJump = jumpList[i - entry];
            }

            if (minJump != Integer.MAX_VALUE) {
                jumpList[i] = minJump + 1;
            }
        }

        return jumpList[newA.length - 1];
    }

    private ArrayList<Integer> getFibNumList(int n) {

        ArrayList<Integer> fibList = new ArrayList<>();

//        we omit first 0 and 1' for purpose of not multi-same-value
        fibList.add(1);
        fibList.add(2);

        int i = 1;
        while (fibList.get(i) <= n) {
            fibList.add(fibList.get(i) + fibList.get(i - 1));
            i++;
        }
        fibList.remove(i);

        return fibList;
    }

    private int[] getInitializedJumpArray(int[] newA, ArrayList<Integer> fibList) {

        int[] jumpList = new int[newA.length];
        jumpList[newA.length - 1] = -1;
        for (int i = 0; i < fibList.size(); i++) {

            if (newA[fibList.get(i) - 1] == 1) {
                jumpList[fibList.get(i) - 1] = 1;
            }
        }

        return jumpList;
    }
}
