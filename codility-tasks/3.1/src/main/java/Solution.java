// task name: PermMissingElement

public class Solution {

    private int x = 0;

    private int preSolution(int X, int Y, int D) {

        x = (Y - X) / D;
        return x;
    }

    public int solution(int x, int y, int d) {

        preSolution(x, y, d);


        return (y - x) % d == 0 ? this.x : this.x + 1;
    }
}
