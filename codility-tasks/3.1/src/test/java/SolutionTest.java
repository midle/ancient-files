import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {

    @Test
    public void test1() {
        Solution solution1 = new Solution();

        int test = solution1.solution(10, 85, 30);

        assertEquals(3, test);
    }

}