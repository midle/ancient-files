import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int[] A = new int[]{0, 1, 0, 1, 1};

        int result = solution.solution(A);

        assertEquals(5, result);
    }

    @Test
    public void diff() throws Exception {

        int[] A = new int[]{1, 0, 1, 0, 0};

        int result = solution.solution(A);

        assertEquals(1, result);
    }

    @Test
    public void single1() throws Exception {

        int[] A = new int[]{1};

        int result = solution.solution(A);

        assertEquals(0, result);
    }

    @Test
    public void single0() throws Exception {

        int[] A = new int[]{0};

        int result = solution.solution(A);

        assertEquals(0, result);
    }

    @Test
    public void min() throws Exception {

        int[] A = new int[]{0, 1};

        int result = solution.solution(A);

        assertEquals(1, result);
    }

    @Test
    public void min0() throws Exception {

        int[] A = new int[]{1, 0};

        int result = solution.solution(A);

        assertEquals(0, result);
    }

    @Test
    public void max() throws Exception {

        int[] A = new int[1000001];

        for (int i = 0; i < A.length; i++) {
            if (i < 100000) {
                A[i] = 0;
            } else {
                A[i] = 1;
            }
        }

        int result = solution.solution(A);

        assertEquals(-1, result);
    }

}