// task name: CountDiv

public class Solution {

    public int solution(int[] A) {

//        initialize req vars (if it could start not only with 0...)
//        int carA = A[0];
//        int carB = (carA == 0) ? 1 : 0;
//        int carB = 1;

        int carA = 0;
        int counterA = 0;
        int result = 0;

        for (int i = 0; i < A.length; i++) {
            if (A[i] == carA) {
                counterA++;
            } else {
                result += counterA;
            }

            if (result > 1000000000) {
                return -1;
            }
        }

        return result;
    }
}
