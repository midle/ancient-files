// task name: PermCheck

import java.util.HashMap;

public class Solution {

    public int solution(int[] A) {

        /* USING HASHMAP CUZ:
        ** PUTTING VALUE INTO IT HAS LOW COMPLEXITY O(1)
        ** CHECKING IF THE KEY IS ALREADY HERE IS ALSO 0(1) .. SO NOT SO COMPLEX
         */
        HashMap<Integer, Integer> hashMap = new HashMap();

        int tempMax = 0;
        int tempMin = 100;//could be also as low as 2.. just should be one, as the whole process SHOULD start from 1!
        int sizeA = A.length;

        for (int i = 0; i < sizeA; i++) {

            hashMap.put(A[i], 1);

            //used for return 0 as soon as it find value larger then sizeA, could be 1st from 100 000 options
            if (A[i] > tempMax) {
                tempMax = A[i];
                if (tempMax > sizeA) {
                    return 0;
                }
            }

            if (tempMin > A[i]) {
                tempMin = A[i];
            }
        }

        //AFTER MAIN LOOP CHECKINGS

        if (tempMin != 1) { //tempMin is there to check if there is actually value of 1 somewhere
            return 0;
        }

        //if hashmaps size does not equal Array length it means that some keys could be repeated..
        if (hashMap.size() != A.length) {
            return 0;
        }

        return 1;
    }
}
