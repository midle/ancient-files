import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void solution1() throws Exception {

        int[] A = {4, 1, 3, 2};

        int result = solution.solution(A);

        assertEquals(1, result);
    }

    @Test
    public void solution0() throws Exception {

        int[] A = {2, 1, 4};

        int result = solution.solution(A);

        assertEquals(0, result);
    }

    @Test
    public void solutionMinimumElements() throws Exception {

        int[] A = {1};

        int result = solution.solution(A);

        assertEquals(1, result);
    }

    @Test
    public void solutionMinimumElements2() throws Exception {

        int[] A = {1, 2};

        int result = solution.solution(A);

        assertEquals(1, result);
    }

    @Test
    public void solutionMinimumElements0() throws Exception {

        int[] A = {1, 3};

        int result = solution.solution(A);

        assertEquals(0, result);
    }

    @Test
    public void solutionNotFrom1() throws Exception {

        int[] A = {2, 3};

        int result = solution.solution(A);

        assertEquals(0, result);
    }

    @Test
    public void solutionSame1() throws Exception {

        int[] A = {1, 1};

        int result = solution.solution(A);

        assertEquals(0, result);
    }

    @Test
    public void solutionSame2() throws Exception {

        int[] A = {2, 2};

        int result = solution.solution(A);

        assertEquals(0, result);
    }

    @Test
    public void solutionMax0() throws Exception {

        int[] A = new int[100000];
        A[0] = 2;
        for (int i = 1; i < A.length; i++) {
            A[i] = A[i - 1] + 1;
        }

        int result = solution.solution(A);

        assertEquals(0, result);
    }

    @Test
    public void solutionMax1() throws Exception {

        int[] A = new int[100000];
        for (int i = 0; i < A.length; i++) {
            A[i] = i + 1;
        }


        int result = solution.solution(A);

        assertEquals(1, result);
    }
}