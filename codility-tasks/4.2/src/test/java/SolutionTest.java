import static org.junit.Assert.*;

import org.junit.Test;

import java.util.Random;

public class SolutionTest {

    Solution test = new Solution();

    @Test
    public void solution1() throws Exception {

        int[] A = {1, 2, 3, 4, 5, 9};

        int x = 5;

        int solution = test.solution(x, A);

        assertEquals(4, solution);

    }

    @Test
    public void solution2() throws Exception {

        int[] A = {1, 2, 3, 4, 6, 7, 8};

        int x = 5;

        int solution = test.solution(x, A);

        assertEquals(-1, solution);

    }

    @Test
    public void solution3() throws Exception {

        int[] A = {1, 3, 1, 4, 2, 3, 5, 4};

        int x = 5;

        int solution = test.solution(x, A);

        assertEquals(6, solution);

    }

    @Test
    public void solution4() throws Exception {

        int[] A = new int[100];


        for (int i = 0; i < A.length; i++) {
            A[i] = i + 1;
        }

        int x = 100;

        int solution = test.solution(x, A);

        assertEquals(x - 1, solution);

    }
}