// task name: FrogRiverOne

public class Solution {

    public int solution(int X, int[] A) {

        int solution = -1;

        int res = 0;

        int currentIter = 0;

        boolean check[] = new boolean[X];

        for (int i = 0; i < A.length; i++) {

            if (A[i] <= check.length && !check[A[i] - 1]) {
                check[A[i] - 1] = true;
                res++;
            }

            if (res == check.length) {
                solution = currentIter;
                break;
            }
            currentIter++;
        }


        return solution;
    }
}
