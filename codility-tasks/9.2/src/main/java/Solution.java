// task name: MaxSliceSum


public class Solution {

    public int solution(int[] A) {

        int solution = 0;

        if (A.length < 2) {
            return solution;
        }

        int tempSolution = A[0];
        int tempMin = A[0];
        int tempMax = 0;

        for (int i = 1; i < A.length; i++) {

            if (A[i] >= tempMin) {
                tempMax = Math.max(tempMax, A[i]);
            } else {
                tempMin = A[i];
                tempMax = 0;
            }

            tempSolution = tempMax - tempMin;

            if (tempSolution > solution)
                solution = tempMax - tempMin;
        }

        return solution;
    }
}
