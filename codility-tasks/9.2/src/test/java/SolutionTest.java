import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by MD on 2017-10-17.
 */
public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void their() throws Exception {

        int[] A = new int[]{23171, 21011, 21123, 21366, 21013, 21367};

        assertEquals(356, solution.solution(A));
    }

    @Test
    public void asc() throws Exception {

        int[] A = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};

        assertEquals(8, solution.solution(A));
    }

    @Test
    public void desc() throws Exception {

        int[] A = new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1};

        assertEquals(0, solution.solution(A));
    }

    @Test
    public void empty() throws Exception {

        int[] A = new int[]{};

        assertEquals(0, solution.solution(A));
    }

    @Test
    public void tooSmall() throws Exception {

        int[] A = new int[]{5};

        assertEquals(0, solution.solution(A));
    }

    @Test
    public void minimalUp() throws Exception {

        int[] A = new int[]{5, 7};

        assertEquals(2, solution.solution(A));
    }

    @Test
    public void minimalDown() throws Exception {

        int[] A = new int[]{7, 5};

        assertEquals(0, solution.solution(A));
    }

}