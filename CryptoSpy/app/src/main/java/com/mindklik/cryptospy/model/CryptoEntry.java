package com.mindklik.cryptospy.model;


public class CryptoEntry {

    private String shortName;
    private double someVar;
    private double change;
    private double currentRate;
    private String url;

    public CryptoEntry(String shortName, double someVar, double change, double currentRate, String url) {
        this.shortName = shortName;
        this.change = change;
        this.currentRate = currentRate;
        this.url = url;
        this.someVar = someVar;
    }


    //    getners
    public String getShortName() {
        return shortName;
    }

    public double getSomeVar() {
        return someVar;
    }

    public double getChange() {
        return change;
    }

    public double getCurrentRate() {
        return currentRate;
    }

    public String getUrl() {
        return url;
    }
}
