package com.mindklik.cryptospy.controllers.fragmentControllers;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.mindklik.cryptospy.CryptoSpyActivity;
import com.mindklik.cryptospy.controllers.adapters.CryptoEntryAdapter;
import com.mindklik.cryptospy.model.CryptoEntry;
import com.mindklik.cryptospy.R;
import com.mindklik.cryptospy.views.fragments.PortfolioFragment;

import java.util.ArrayList;

public abstract class FragmentWithAdapterBase extends FragmentBase {

    protected CryptoEntryAdapter cryptoEntryAdapter;

    protected ListView cryptoList;
    protected TextView emptyList;

    protected int entriesCount;
    private boolean isCreated;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (this instanceof PortfolioFragment) {
            rootView = inflater.inflate(R.layout.fragment_portfolio, container, false);
        } else {
            rootView = inflater.inflate(R.layout.fragment_current, container, false);
        }

        initializeVariables();
        checkIfEmpty();

        refreshData();

        return rootView;
    }

    @Override
    protected void initializeVariables() {
        super.initializeVariables();

        // Find a references to the {@link ListView} and {@link ProgressBar} in fragment_current
        cryptoList = rootView.findViewById(R.id.crypto_list);
        cryptoEntryAdapter = new CryptoEntryAdapter(rootView.getContext(), new ArrayList<CryptoEntry>(), this instanceof PortfolioFragment);

        cryptoList.setAdapter(cryptoEntryAdapter);

        isCreated = true;
    }


    protected void checkIfEmpty() {
        emptyList = (TextView) rootView.findViewById(R.id.blankView);
        cryptoList.setEmptyView(emptyList);

        if (cryptoEntryAdapter.getCount() == 0) {
            emptyList.setText(setFragmentEmptyRes());
        }
    }

    protected abstract int setFragmentEmptyRes();


    protected void refreshDataInner() {
        //clear the data before refresh
        cryptoEntryAdapter.clear();

        if (((CryptoSpyActivity) getContext()) != null) {
            ((CryptoSpyActivity) getContext()).progressBar.setVisibility(View.VISIBLE);
        }
    }

    protected void refreshData() {
        ((CryptoSpyActivity) getContext()).refreshImage();

        if (entriesCount == 0 && ((CryptoSpyActivity) getContext()).isNetAvailable()) {
            refreshDataInner();
        }
    }


    @Override
    public void onPause() {
        //set iscreated to false, because userhint is here before oncreate and old val remains as true:o
        super.onPause();
        isCreated = false;
    }


    //    if we come back to our fragment, it should refresh to apply new options
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isCreated && isVisibleToUser) {
            refreshData();
        }
    }
}
