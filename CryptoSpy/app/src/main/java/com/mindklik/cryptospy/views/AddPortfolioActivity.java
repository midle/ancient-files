package com.mindklik.cryptospy.views;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.mindklik.cryptospy.CryptoSpyActivity;
import com.mindklik.cryptospy.R;
import com.mindklik.cryptospy.controllers.data.CryptoDBHelper;
import com.mindklik.cryptospy.controllers.data.CryptoContract.*;


public class AddPortfolioActivity extends AppCompatActivity {

    private CryptoDBHelper cryptoDBHelper;

    AutoCompleteTextView coinName;
    EditText coinAmount;
    ImageButton addButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        cryptoDBHelper = CryptoDBHelper.getCryptoDBHelper(getBaseContext());
        setContentView(R.layout.activity_add_portfolio);

        initializeVariables();
        addButtonListener();
    }

    protected void initializeVariables() {
        coinName = findViewById(R.id.coinName);
        coinAmount = findViewById(R.id.coinAmount);
        addButton = findViewById(R.id.addButton);

        ArrayAdapter<String> adapter = new ArrayAdapter(getBaseContext(), android.R.layout.simple_spinner_item, CryptoSpyActivity.getAvailableCoins());
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the input
        coinName.setAdapter(adapter);

        Intent intent = getIntent();
        coinName.setText(intent.getStringExtra("name"));

        double coinAmountString = intent.getDoubleExtra("amount", 0);
        if (coinAmountString > 0) {
            coinAmount.setText(String.valueOf(coinAmountString));
        }
    }

    private void addButtonListener() {
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempCoinName = coinName.getText().toString().toUpperCase();
                String tempCoinAmount = coinAmount.getText().toString();

                if (CryptoSpyActivity.getAvailableCoins().contains(tempCoinName)) {

                    ContentValues contentValues = new ContentValues();
                    contentValues.put(CryptoDBEntry.COLUMN_PORTFOLIO_COIN, tempCoinName);
                    contentValues.put(CryptoDBEntry.COLUMN_PORTFOLIO_AMOUNT, tempCoinAmount);

                    SQLiteDatabase db = cryptoDBHelper.getReadableDatabase();

                    Cursor cursor = db.rawQuery("SELECT * FROM " + CryptoDBEntry.TABLE_PORTFOLIO_NAME +
                            " WHERE " + CryptoDBEntry.COLUMN_PORTFOLIO_COIN +
                            " = \"" + tempCoinName + "\"", null);

                    if (cursor.getCount() <= 0) {
                        long insert = db.insert(CryptoDBEntry.TABLE_PORTFOLIO_NAME, null, contentValues);
                        if (insert != -1) {
                            Toast.makeText(getBaseContext(), getResources().getString(R.string.added_coins) + "\n" + tempCoinAmount + " " + tempCoinName, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getBaseContext(), getResources().getString(R.string.error_adding_coins), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        db.update(CryptoDBEntry.TABLE_PORTFOLIO_NAME, contentValues, CryptoDBEntry.COLUMN_PORTFOLIO_COIN + " =?", new String[]{tempCoinName});
                        Toast.makeText(getBaseContext(), getResources().getString(R.string.changed) + " " + tempCoinName + " " + getResources().getString(R.string.amount_to) + " " + tempCoinAmount, Toast.LENGTH_SHORT).show();
                    }

                    cursor.close();
                    db.close();
                } else {
                    Toast.makeText(getBaseContext(), getResources().getString(R.string.coin_unavailable), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return false;
    }
}
