package com.mindklik.cryptospy.controllers.adapters;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mindklik.cryptospy.CryptoSpyActivity;
import com.mindklik.cryptospy.controllers.data.CryptoContract;
import com.mindklik.cryptospy.controllers.data.CryptoContract.*;
import com.mindklik.cryptospy.R;
import com.mindklik.cryptospy.controllers.data.CryptoDBHelper;
import com.mindklik.cryptospy.model.CryptoEntry;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;


public class CryptoEntryAdapter extends ArrayAdapter<CryptoEntry> {

    private static final int RES_ID = 0;

    private CryptoDBHelper cryptoDBHelper;
    private SharedPreferences sharedPreferences;

    private boolean isPortfolio;


    public CryptoEntryAdapter(@NonNull Context context, @NonNull List<CryptoEntry> objects, boolean isPortfolio) {
        super(context, RES_ID, objects);

        this.isPortfolio = isPortfolio;

        cryptoDBHelper = CryptoDBHelper.getCryptoDBHelper(getContext());
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View listViewItem = convertView;

        if (listViewItem == null) {
            listViewItem = LayoutInflater.from(getContext()).inflate(R.layout.list_view_item, parent, false);
        }

        CryptoEntry currentEntry = getItem(position);

//        setting views
        TextView shortName = (TextView) listViewItem.findViewById(R.id.short_name);
        shortName.setText(formatShortName(currentEntry.getShortName()));
        shortName.setText(formatShortName(currentEntry.getShortName()));

        TextView var1 = (TextView) listViewItem.findViewById(R.id.someVar);
        var1.setText(formatVar(currentEntry.getSomeVar(), currentEntry.getCurrentRate(), currentEntry.getShortName()));

        TextView currentRate = (TextView) listViewItem.findViewById(R.id.current_rate);
        currentRate.setText(formatCurrentRate(currentEntry.getCurrentRate(), currentEntry.getShortName()));

        TextView change = (TextView) listViewItem.findViewById(R.id.change);
        change.setText(formatChange(currentEntry.getChange()));

        ImageView favImage = (ImageView) listViewItem.findViewById(R.id.favImage);
        checkFavImage(favImage, currentEntry);

        ImageView infoImage = (ImageView) listViewItem.findViewById(R.id.infoImage);
        if (isPortfolio) {
            infoImage.setImageResource(android.R.drawable.ic_delete);
            infoImage.setColorFilter(Color.RED);
            setDeleteListener(infoImage, currentEntry.getShortName());
        } else {
            setInfoListener(infoImage, currentEntry.getUrl());
        }

//        listeners
        setFavListener(favImage, currentEntry);

//        set appropriate bg color basing on change rate
        listViewItem.setBackgroundColor(setBgColor(currentEntry.getChange()));

        return listViewItem;
    }


    private void setFavListener(final ImageView favImage, final CryptoEntry currentEntry) {
        favImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFav(currentEntry.getShortName());
                checkFavImage(favImage, currentEntry);
            }
        });
    }

    private void checkFavImage(ImageView favImage, CryptoEntry currentEntry) {

        SQLiteDatabase readableDB = cryptoDBHelper.getReadableDatabase();

        Cursor cursor = readableDB.rawQuery("SELECT * FROM " + CryptoDBEntry.TABLE_LIKE_NAME + " WHERE " + CryptoDBEntry.COLUMN_LIKE_COIN + " = \"" + currentEntry.getShortName() + "\"", null);

        if (cursor.getCount() <= 0) {
            favImage.setImageResource(android.R.drawable.star_big_off);
        } else {
            favImage.setImageResource(android.R.drawable.star_big_on);
        }

        cursor.close();
        readableDB.close();
    }

    //    check if favourite
    private void setFav(String currentEntry) {

        SQLiteDatabase readableDB = cryptoDBHelper.getReadableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(CryptoDBEntry.COLUMN_LIKE_COIN, currentEntry);

        Cursor cursor = readableDB.rawQuery("SELECT * FROM " + CryptoDBEntry.TABLE_LIKE_NAME + " WHERE " + CryptoDBEntry.COLUMN_LIKE_COIN + " = \"" + currentEntry + "\"", null);

        if (cursor.getCount() <= 0) {
            readableDB.insert(CryptoDBEntry.TABLE_LIKE_NAME, null, contentValues);
        } else {
            readableDB.delete(CryptoDBEntry.TABLE_LIKE_NAME, CryptoDBEntry.COLUMN_LIKE_COIN + "=?", new String[]{currentEntry});
        }

        cursor.close();
        readableDB.close();
    }


    private void setInfoListener(ImageView infoImage, final String url) {
        infoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri cryptoUri = Uri.parse(url);

                Intent webIntent = new Intent(Intent.ACTION_VIEW);
                webIntent.setData(cryptoUri);

                if (webIntent.resolveActivity(CryptoEntryAdapter.super.getContext().getPackageManager()) != null) {
                    CryptoEntryAdapter.super.getContext().startActivity(webIntent);
                }
            }
        });
    }

    private void setDeleteListener(ImageView infoImage, final String name) {
        infoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SQLiteDatabase db = cryptoDBHelper.getReadableDatabase();
                int del = db.delete(CryptoContract.CryptoDBEntry.TABLE_PORTFOLIO_NAME,
                        CryptoContract.CryptoDBEntry.COLUMN_PORTFOLIO_COIN + "=\"" + name + "\"",
                        null);
                db.close();

                if (del > 0) {
                    LocalBroadcastManager.getInstance(getContext()).sendBroadcast(new Intent("deleted"));
                    Toast.makeText(getContext(), name + " " + getContext().getResources().getString(R.string.deleted_from_portfolio), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //    format inputs
    @NonNull
    private String formatShortName(String shortName) {
        return shortName.trim();
    }

    @NonNull
    private String formatVar(double cap, double price, String name) {

        if (isPortfolio) {

            SQLiteDatabase readableDB = cryptoDBHelper.getReadableDatabase();

            Cursor cursor = readableDB.rawQuery("SELECT * FROM " + CryptoDBEntry.TABLE_PORTFOLIO_NAME + " WHERE " + CryptoDBEntry.COLUMN_PORTFOLIO_COIN + " = \"" + name + "\"", null);

            double quantity = 0;
            if (cursor.moveToFirst()) {
                quantity = cursor.getDouble(cursor.getColumnIndex(CryptoDBEntry.COLUMN_PORTFOLIO_AMOUNT));
            }

            cursor.close();
            readableDB.close();

            return "Coins: " + quantity;
        } else {
            if (cap != 0) {

                DecimalFormat capFormatter = new DecimalFormat("0.000", new DecimalFormatSymbols(Locale.US));

                return "Cap(mld$): " + capFormatter.format(cap / 1_000_000_000);
            } else {
                DecimalFormat rateFormatter = new DecimalFormat("0.00######", new DecimalFormatSymbols(Locale.US));
                return rateFormatter.format(price / CryptoSpyActivity.getCompareCoinPrice()) + " " + sharedPreferences.getString("cryptoCompare", "BTC");
            }
        }
    }

    @NonNull
    private String formatCurrentRate(double currentRate, String name) {

        if (isPortfolio) {
            DecimalFormat rateFormatter = new DecimalFormat("0", new DecimalFormatSymbols(Locale.US));

            SQLiteDatabase readableDB = cryptoDBHelper.getReadableDatabase();

            Cursor cursor = readableDB.rawQuery("SELECT * FROM " + CryptoDBEntry.TABLE_PORTFOLIO_NAME + " WHERE " + CryptoDBEntry.COLUMN_PORTFOLIO_COIN + " = \"" + name + "\"", null);

            double quantity = 0;
            if (cursor.moveToFirst()) {
                quantity = cursor.getDouble(cursor.getColumnIndex(CryptoDBEntry.COLUMN_PORTFOLIO_AMOUNT));
            }

            cursor.close();
            readableDB.close();

            return "Total: " + rateFormatter.format(quantity * currentRate) + "$";
        } else {
            DecimalFormat rateFormatter = new DecimalFormat("0.0##", new DecimalFormatSymbols(Locale.US));
            return rateFormatter.format(currentRate) + "$";
        }
    }

    @NonNull
    private String formatChange(double change) {

        DecimalFormat rateFormatter = new DecimalFormat("0.00", new DecimalFormatSymbols(Locale.US));

        StringBuilder changeSB = new StringBuilder(rateFormatter.format(change));

        if (change > 0) {
            changeSB.insert(0, "+");
        }

        changeSB.append("%");

        return changeSB.toString();
    }


    //    bg color change method
    private int setBgColor(double change) {

        if (change <= -50) {
            return ContextCompat.getColor(getContext(), R.color.changeMin50);
        } else if (change <= -25) {
            return ContextCompat.getColor(getContext(), R.color.changeMin25);
        } else if (change <= -10) {
            return ContextCompat.getColor(getContext(), R.color.changeMin10);
        } else if (change <= -5) {
            return ContextCompat.getColor(getContext(), R.color.changeMin5);
        } else if (change <= -2.5) {
            return ContextCompat.getColor(getContext(), R.color.changeMin2dot5);
        } else if (change <= -0.1) {
            return ContextCompat.getColor(getContext(), R.color.changeMin0dot01);
        } else if (change >= 100) {
            return ContextCompat.getColor(getContext(), R.color.change100plus);
        } else if (change >= 50) {
            return ContextCompat.getColor(getContext(), R.color.change50);
        } else if (change >= 25) {
            return ContextCompat.getColor(getContext(), R.color.change25);
        } else if (change >= 10) {
            return ContextCompat.getColor(getContext(), R.color.change10);
        } else if (change >= 5) {
            return ContextCompat.getColor(getContext(), R.color.change5);
        } else if (change >= 2.5) {
            return ContextCompat.getColor(getContext(), R.color.change2dot5);
        } else if (change >= 0.1) {
            return ContextCompat.getColor(getContext(), R.color.change0dot01);
        } else {
            return ContextCompat.getColor(getContext(), R.color.change0);
        }
    }
}