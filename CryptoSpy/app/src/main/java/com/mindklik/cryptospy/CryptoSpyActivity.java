package com.mindklik.cryptospy;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.mindklik.cryptospy.controllers.netQueries.CryptoLoader;
import com.mindklik.cryptospy.controllers.netQueries.CryptoQuery;
import com.mindklik.cryptospy.views.SettingsActivity;
import com.mindklik.cryptospy.controllers.fragmentControllers.TabPagerAdapter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;


public class CryptoSpyActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<String>> {

    private SharedPreferences sharedPreferences;

    private TabPagerAdapter tabsAdapter;

    private static ArrayList<String> availableCoins;
    private static double compareCoinPrice = 1;

    private ViewPager viewPager;
    private TabLayout tabLayout;
    public ProgressBar progressBar;
    private ImageView offlineImage;

    private LoaderManager loaderManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crypto_spy);

        initializeVars();

        if (availableCoins.isEmpty())
            initLoader(CryptoQuery.CRYPTO_LOADER_TOP_ID);

        new CompareCoinDownloader(this).execute(CryptoQuery.URL_CRYPTO_BASE + CryptoQuery.METHOD_LIKED_COINS + sharedPreferences.getString("cryptoCompare", "BTC"));
    }

    private void initializeVars() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        offlineImage = (ImageView) findViewById(R.id.offlineImage);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        availableCoins = new ArrayList<>();

        //For Nice Fragment UI
        tabsAdapter = new TabPagerAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(tabsAdapter);
//        viewPager.setOffscreenPageLimit(1); //change later maybe
        tabLayout.setupWithViewPager(viewPager);

        loaderManager = getLoaderManager();
    }

    public void refreshImage() {

        if (!isNetAvailable()) {
            offlineImage.setImageResource(R.drawable.offline_image);
        } else {
            offlineImage.setImageResource(0);
        }
    }


    //    MENU CODE
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_resync) {
            refreshCurrentFragment();
        }

        return super.onOptionsItemSelected(item);
    }


    private void refreshCurrentFragment() {
        if (isNetAvailable()) {

            offlineImage.setImageResource(0);

            if (availableCoins.isEmpty()) {
                initLoader(CryptoQuery.CRYPTO_LOADER_TOP_ID);
            }

            tabsAdapter.refreshFragments(viewPager.getCurrentItem());
        } else {
            progressBar.setVisibility(View.GONE);
            offlineImage.setImageResource(R.drawable.offline_image);
        }
    }


    public boolean isNetAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getBaseContext().getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;

        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }


    private void initLoader(int id) {
        if (isNetAvailable()) {
            loaderManager.initLoader(id, null, this);
        }
    }

    //    interface implementation
    @Override
    public Loader<ArrayList<String>> onCreateLoader(int id, Bundle args) {
        if (id == CryptoQuery.CRYPTO_LOADER_TOP_ID) {
            Uri baseUri = Uri.parse(CryptoQuery.URL_CRYPTO_BASE);
            Uri.Builder uriBuilder = baseUri.buildUpon();
            uriBuilder.appendPath(CryptoQuery.COINS_NAME_LINK);
            return new CryptoLoader(this, uriBuilder.toString(), id);
        } else {
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<String>> loader, ArrayList<String> data) {

        if (data.size() > 0 && availableCoins.size() != data.size()) {
            //       firstly clear the data
            availableCoins.clear();

            availableCoins.addAll(data);
        }
    }


    @Override
    public void onLoaderReset(Loader<ArrayList<String>> loader) {
    }


    //    Getters
    public static ArrayList<String> getAvailableCoins() {
        return availableCoins;
    }

    public static double getCompareCoinPrice() {
        return compareCoinPrice;
    }


    @Override
    protected void onResume() {
        super.onResume();

        new CompareCoinDownloader(this).execute(CryptoQuery.URL_CRYPTO_BASE + CryptoQuery.METHOD_LIKED_COINS + sharedPreferences.getString("cryptoCompare", "BTC"));
    }


    //Async Tasks
    private static class CompareCoinDownloader extends AsyncTask<String, Void, Double> {

        private final WeakReference<CryptoSpyActivity> mainWeakReference;

        CompareCoinDownloader(CryptoSpyActivity cryptoSpyActivity) {
            mainWeakReference = new WeakReference<>(cryptoSpyActivity);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Double doInBackground(String... url) {
            if (url != null) {
                return CryptoQuery.fetchComparator(url[0]);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Double value) {
            compareCoinPrice = value;

            mainWeakReference.get().refreshCurrentFragment();
        }
    }
}

