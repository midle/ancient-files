package com.mindklik.cryptospy.controllers.fragmentControllers;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mindklik.cryptospy.R;
import com.mindklik.cryptospy.views.fragments.FavouriteFragment;
import com.mindklik.cryptospy.views.fragments.FocusFragment;
import com.mindklik.cryptospy.views.fragments.PortfolioFragment;
import com.mindklik.cryptospy.views.fragments.TopFragment;


public class TabPagerAdapter extends FragmentPagerAdapter {

    private static final int PORTFOLIO_NO = 0;
    private static final int FAV_NO = 1;
    private static final int FOCUS_NO = 2;
    private static final int TOP_NO = 3;

    //change if coin added!
    private static final int FRAGS_COUNT = 4;

    private Context context;

    //save fragments in the array to enable refreshing the list
    private FragmentBase[] fragmentBases;

    public TabPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;

        fragmentBases = new FragmentBase[FRAGS_COUNT];
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case PORTFOLIO_NO:
                fragmentBases[PORTFOLIO_NO] = new PortfolioFragment();
                break;
            case FAV_NO:
                fragmentBases[FAV_NO] = new FavouriteFragment();
                break;
            case FOCUS_NO:
                fragmentBases[FOCUS_NO] = new FocusFragment();
                break;
            case TOP_NO:
                fragmentBases[TOP_NO] = new TopFragment();
                break;
        }

        return fragmentBases[position];
    }

    @Override
    public int getCount() {
        return FRAGS_COUNT;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {

        if (position == FAV_NO) {
            return context.getString(R.string.favs_coins);
        } else if (position == TOP_NO) {
            return context.getString(R.string.top_coins);
        } else if (position == FOCUS_NO) {
            return context.getString(R.string.check_coin);
        } else if (position == PORTFOLIO_NO) {
            return context.getString(R.string.portfolio);
        } else {
            return null;
        }
    }

    //    refresh fragments state
    public void refreshFragments(int itemNO) {
        if (fragmentBases[itemNO] != null)
            fragmentBases[itemNO].refreshData();
    }
}
