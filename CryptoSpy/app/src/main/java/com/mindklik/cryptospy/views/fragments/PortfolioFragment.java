package com.mindklik.cryptospy.views.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mindklik.cryptospy.controllers.data.CryptoContract;
import com.mindklik.cryptospy.controllers.fragmentControllers.FragmentWithAdapterBase;
import com.mindklik.cryptospy.controllers.netQueries.CryptoQuery;
import com.mindklik.cryptospy.views.AddPortfolioActivity;
import com.mindklik.cryptospy.model.CryptoEntry;
import com.mindklik.cryptospy.R;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Locale;


public class PortfolioFragment extends FragmentWithAdapterBase {


    private ImageView imageAdd;
    private TextView textBalance, textBalanceChange;

    private double currentBalance, currentChange;
    private HashMap<String, Double> currentAmounts;

    private BroadcastReceiver broadcastReceiver;

    @Override
    protected void initializeVariables() {
        super.initializeVariables();

        imageAdd = (ImageView) rootView.findViewById(R.id.imageAdd);
        textBalance = (TextView) rootView.findViewById(R.id.textBalance);
        textBalanceChange = (TextView) rootView.findViewById(R.id.textBalanceChange);
        currentAmounts = new HashMap<>();

        setImageAddListener();
        setEntryListener();
    }

    private void setImageAddListener() {
        imageAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityIntent = new Intent(getActivity(), AddPortfolioActivity.class);
                startActivity(activityIntent);
            }
        });
    }

    private void setEntryListener() {
        cryptoList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view, final int position, long id) {

                SQLiteDatabase db = cryptoDBHelper.getReadableDatabase();

                String currentName = ((CryptoEntry) parent.getItemAtPosition(position)).getShortName();

                Cursor cursor = db.rawQuery("SELECT * FROM " + CryptoContract.CryptoDBEntry.TABLE_PORTFOLIO_NAME +
                                " WHERE " + CryptoContract.CryptoDBEntry.COLUMN_PORTFOLIO_COIN +
                                " LIKE \"" + currentName + "\";",
                        null);

                cursor.moveToFirst();
                Double currentAmount = cursor.getDouble(cursor.getColumnIndex(CryptoContract.CryptoDBEntry.COLUMN_PORTFOLIO_AMOUNT));

                cursor.close();
                db.close();

                Animation animation1 = new AlphaAnimation(0.3f, 1.0f);
                animation1.setDuration(100);
                view.startAnimation(animation1);

                Intent activityIntent = new Intent(getActivity(), AddPortfolioActivity.class);
                activityIntent.putExtra("name", currentName);
                activityIntent.putExtra("amount", currentAmount);
                startActivity(activityIntent);
            }
        });
    }


    @Override
    public void refreshDataInner() {

        super.refreshDataInner();

        currentBalance = 0;
        currentChange = 0;

        SQLiteDatabase db = cryptoDBHelper.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + CryptoContract.CryptoDBEntry.TABLE_PORTFOLIO_NAME, null);

        entriesCount = cursor.getCount();

        if (cursor.moveToFirst()) {
            do {
                String entry = cursor.getString(cursor.getColumnIndex(CryptoContract.CryptoDBEntry.COLUMN_PORTFOLIO_COIN));
                currentAmounts.put(entry, cursor.getDouble(cursor.getColumnIndex(CryptoContract.CryptoDBEntry.COLUMN_PORTFOLIO_AMOUNT)));
                new PortfolioDownloader(this).execute(CryptoQuery.URL_CRYPTO_BASE + CryptoQuery.METHOD_LIKED_COINS + entry);

            } while (cursor.moveToNext());
        } else {
            hideProgressBar();
            textBalance.setText(String.format("%s$", String.valueOf(0)));
            textBalanceChange.setText(String.format("%s%%", String.valueOf(0)));
            textBalanceChange.setTextColor(Color.BLACK);
        }

        cursor.close();

        db.close();
    }


    private void setTexts() {

        for (int i = 0; i < cryptoEntryAdapter.getCount(); i++) {
            //sometimes hashmap get init error and with this if statement, app wont crash
            if (currentAmounts.get(cryptoEntryAdapter.getItem(i).getShortName()) != null) {

                double currentCoinBalance = cryptoEntryAdapter.getItem(i).getCurrentRate() * currentAmounts.get(cryptoEntryAdapter.getItem(i).getShortName());
                currentBalance += currentCoinBalance;

                currentChange += cryptoEntryAdapter.getItem(i).getChange() * currentCoinBalance;
            }
        }

        textBalance.setText(formatCurrentRate(currentBalance));
        textBalanceChange.setText(formatChange(currentChange / currentBalance));
    }

    private String formatChange(double change) {

        DecimalFormat rateFormatter = new DecimalFormat("0.00", new DecimalFormatSymbols(Locale.US));

        StringBuilder changeSB = new StringBuilder(rateFormatter.format(change));

        if (change > 0) {
            changeSB.insert(0, "+");
            textBalanceChange.setTextColor(Color.GREEN);
        } else {
            textBalanceChange.setTextColor(Color.RED);
        }

        changeSB.append("%");

        return changeSB.toString();
    }


    @NonNull
    private String formatCurrentRate(double currentRate) {

        DecimalFormat rateFormatter = new DecimalFormat("0.00", new DecimalFormatSymbols(Locale.US));

        return rateFormatter.format(currentRate) + "$";
    }

    @Override
    protected int setFragmentEmptyRes() {
        return R.string.no_portfolio_currencies;
    }


    //broadcastreceivers
    @Override
    public void onStart() {
        super.onStart();

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                refreshData();
            }
        };

        LocalBroadcastManager.getInstance(rootView.getContext()).registerReceiver(broadcastReceiver, new IntentFilter("deleted"));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(rootView.getContext()).unregisterReceiver(broadcastReceiverBack);
    }


    //downloader class
    private static class PortfolioDownloader extends AsyncTask<String, Void, CryptoEntry> {

        private final WeakReference<PortfolioFragment> portfolioFragmentWeakReference;
        private String coinName;

        PortfolioDownloader(PortfolioFragment portfolioFragment) {
            portfolioFragmentWeakReference = new WeakReference<>(portfolioFragment);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected CryptoEntry doInBackground(String... url) {
            if (url != null) {
                coinName = url[0].replace(CryptoQuery.URL_CRYPTO_BASE, "").replace(CryptoQuery.METHOD_LIKED_COINS, "");
                return CryptoQuery.fetchPortfolio(url[0]);
            }
            return null;
        }

        @Override
        protected void onPostExecute(CryptoEntry cryptoEntry) {

            if (portfolioFragmentWeakReference.get() != null) {
                // set names
                if (cryptoEntry != null) {
                    portfolioFragmentWeakReference.get().cryptoEntryAdapter.add(cryptoEntry);
                } else {
                    portfolioFragmentWeakReference.get().cryptoEntryAdapter.add
                            (new CryptoEntry(coinName, 0, 0, 0, ""));
                    Toast.makeText(portfolioFragmentWeakReference.get().rootView.getContext(),
                            coinName + " " + portfolioFragmentWeakReference.get().rootView.getContext().getResources().getString(R.string.coin_unavailable)
                            , Toast.LENGTH_SHORT).show();
                }

                portfolioFragmentWeakReference.get().entriesCount -= 1;

                if (portfolioFragmentWeakReference.get().entriesCount == 0) {
                    portfolioFragmentWeakReference.get().hideProgressBar();
                    portfolioFragmentWeakReference.get().setTexts();
                }
            }
        }
    }
}
