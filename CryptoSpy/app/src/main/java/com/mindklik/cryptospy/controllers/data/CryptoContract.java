package com.mindklik.cryptospy.controllers.data;

import android.provider.BaseColumns;


public class CryptoContract {


    private CryptoContract() {
    }


    public static final class CryptoDBEntry implements BaseColumns {

        //MIMES

        
        //LIKE TABLE
        public static final String TABLE_LIKE_NAME = "likes";

        public static final String COLUMN_LIKE_ID = BaseColumns._ID;
        public static final String COLUMN_LIKE_COIN = "coin_like";

        //PORTFOLIO TABLE
        public static final String TABLE_PORTFOLIO_NAME = "portfolio";

        public static final String COLUMN_PORTFOLIO_ID = BaseColumns._ID;
        public static final String COLUMN_PORTFOLIO_COIN = "coin";
        public static final String COLUMN_PORTFOLIO_AMOUNT = "amount";

//        //PORTFOLIO COINS
//        public static final String TABLE_COINS_NAME = "coins";
//
//        public static final String COLUMN_COINS_ID = BaseColumns._ID;
//        public static final String COLUMN_COINS_COIN = "coin";
    }

}
