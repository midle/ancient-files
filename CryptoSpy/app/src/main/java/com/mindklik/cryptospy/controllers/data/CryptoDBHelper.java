package com.mindklik.cryptospy.controllers.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.mindklik.cryptospy.controllers.data.CryptoContract.*;


public class CryptoDBHelper extends SQLiteOpenHelper {


    private static final int DB_VERSION = 5;
    private static final String DB_NAME = "crypto_spy_database.db";

    //SINGLETON here
    private static CryptoDBHelper cryptoDBHelper;

    private CryptoDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public static synchronized CryptoDBHelper getCryptoDBHelper(Context context) {

        if (cryptoDBHelper == null) {
            cryptoDBHelper = new CryptoDBHelper(context.getApplicationContext());
        }

        return cryptoDBHelper;
    }


    private static final String CREATE_TABLE_LIKE = "CREATE TABLE " + CryptoDBEntry.TABLE_LIKE_NAME + " ( "
            + CryptoDBEntry.COLUMN_LIKE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + CryptoDBEntry.COLUMN_LIKE_COIN + " TEXT NOT NULL);";

    private static final String CREATE_TABLE_PORTFOLIO = "CREATE TABLE " + CryptoDBEntry.TABLE_PORTFOLIO_NAME + " ( "
            + CryptoDBEntry.COLUMN_PORTFOLIO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + CryptoDBEntry.COLUMN_PORTFOLIO_COIN + " TEXT NOT NULL, "
            + CryptoDBEntry.COLUMN_PORTFOLIO_AMOUNT + " REAL DEFAULT 0);";

//    private static final String CREATE_TABLE_COINS = "CREATE TABLE " + CryptoDBEntry.TABLE_COINS_NAME + " ( "
//            + CryptoDBEntry.COLUMN_COINS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
//            + CryptoDBEntry.COLUMN_COINS_COIN + " TEXT NOT NULL);";


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TABLE_LIKE);
        db.execSQL(CREATE_TABLE_PORTFOLIO);
//        db.execSQL(CREATE_TABLE_COINS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
