package com.mindklik.cryptospy.views.fragments;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindklik.cryptospy.controllers.data.CryptoContract;
import com.mindklik.cryptospy.model.CryptoEntry;
import com.mindklik.cryptospy.controllers.netQueries.CryptoQuery;
import com.mindklik.cryptospy.CryptoSpyActivity;
import com.mindklik.cryptospy.R;
import com.mindklik.cryptospy.controllers.fragmentControllers.FragmentBase;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;


public class FocusFragment extends FragmentBase {


    private AutoCompleteTextView input;
    private TextView focusPrice;
    private TextView focusChange;
    private ImageView favImage;
    private ImageView focusLogo;

    private InputMethodManager inputManager;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_focus, container, false);

        initializeVariables();

        return rootView;
    }


    @Override
    protected void initializeVariables() {
        super.initializeVariables();

        input = (AutoCompleteTextView) rootView.findViewById(R.id.focusInput);
        focusPrice = (TextView) rootView.findViewById(R.id.focusPrice);
        focusChange = (TextView) rootView.findViewById(R.id.focusChange);
        favImage = (ImageView) rootView.findViewById(R.id.focusLike);
        focusLogo = (ImageView) rootView.findViewById(R.id.focusLogo);

        inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        ArrayAdapter<String> adapter = new ArrayAdapter(rootView.getContext(), android.R.layout.simple_spinner_item, CryptoSpyActivity.getAvailableCoins());
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the input
        input.setAdapter(adapter);

        setInputListener();
        setFocusLikeListener();
    }

    private void setInputListener() {
        input.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (((CryptoSpyActivity) getContext()).isNetAvailable()) {
                    if (inputManager != null) {
                        inputManager.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                    }

                    refreshData();
                }
            }
        });
    }

    private void setFocusLikeListener() {
        favImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tempName = input.getText().toString().toUpperCase();

                if (CryptoSpyActivity.getAvailableCoins().contains(tempName)) {
                    SQLiteDatabase readableDB = cryptoDBHelper.getReadableDatabase();

                    ContentValues contentValues = new ContentValues();
                    contentValues.put(CryptoContract.CryptoDBEntry.COLUMN_LIKE_COIN, tempName);

                    Cursor cursor = readableDB.rawQuery("SELECT * FROM " + CryptoContract.CryptoDBEntry.TABLE_LIKE_NAME + " WHERE " + CryptoContract.CryptoDBEntry.COLUMN_LIKE_COIN + " = \"" + tempName + "\"", null);

                    if (cursor.getCount() <= 0) {
                        readableDB.insert(CryptoContract.CryptoDBEntry.TABLE_LIKE_NAME, null, contentValues);
                    } else {
                        readableDB.delete(CryptoContract.CryptoDBEntry.TABLE_LIKE_NAME, CryptoContract.CryptoDBEntry.COLUMN_LIKE_COIN + "=?", new String[]{tempName});
                    }

                    cursor.close();
                    readableDB.close();

                    checkCoinLike(tempName);
                }
            }
        });
    }

    @Override
    protected void refreshData() {

        favImage.setVisibility(View.GONE);

        String tempName = input.getText().toString().toUpperCase();

        if (((CryptoSpyActivity) getContext()).isNetAvailable()) {

            if (CryptoSpyActivity.getAvailableCoins().contains(tempName)) {
                new ImageDownloader(this).execute(CryptoQuery.URL_CRYPTO_BASE + CryptoQuery.LOGO_LINK + tempName);
                new CoinInfoDownloader(this).execute(CryptoQuery.URL_CRYPTO_BASE + CryptoQuery.METHOD_LIKED_COINS + tempName);

                checkCoinLike(tempName);
            } else {
                focusChange.setVisibility(View.GONE);
                focusLogo.setVisibility(View.GONE);
                focusPrice.setVisibility(View.GONE);
            }
        } else {
            ((CryptoSpyActivity) getContext()).refreshImage();
        }
    }

    private void checkCoinLike(String tempName) {

        SQLiteDatabase readableDB = cryptoDBHelper.getReadableDatabase();

        Cursor cursor = readableDB.rawQuery("SELECT * FROM " + CryptoContract.CryptoDBEntry.TABLE_LIKE_NAME + " WHERE " + CryptoContract.CryptoDBEntry.COLUMN_LIKE_COIN + " = \"" + tempName + "\"", null);

        if (cursor.getCount() <= 0) {
            favImage.setImageResource(android.R.drawable.star_big_off);
        } else {
            favImage.setImageResource(android.R.drawable.star_big_on);
        }

        cursor.close();
        readableDB.close();
    }


    /////////////////classess///////////////////////weak reference//////////////
    // ImageDownloader AsyncTask class
    private static class ImageDownloader extends AsyncTask<String, Void, Drawable> {

        private final WeakReference<FocusFragment> focusFragmentWeakReference;

        ImageDownloader(FocusFragment focusFragment) {
            focusFragmentWeakReference = new WeakReference<>(focusFragment);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Drawable doInBackground(String... url) {

            URL imageURL = null;

            try {
                imageURL = new URL(url[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            Drawable newDrawable = null;
            try {
                // Download logo
                InputStream inputStream = (InputStream) imageURL.getContent();
                // Decode downloaded logo
                newDrawable = Drawable.createFromStream(inputStream, "src");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return newDrawable;
        }

        @Override
        protected void onPostExecute(Drawable result) {
            // set logo Image after successful execution
            if (focusFragmentWeakReference.get() != null) {
                focusFragmentWeakReference.get().focusLogo.setVisibility(View.VISIBLE);
                focusFragmentWeakReference.get().focusLogo.setImageDrawable(result);
            }
        }
    }

    // CoinInfoDownloader AsyncTask class
    private static class CoinInfoDownloader extends AsyncTask<String, Void, CryptoEntry> {

        private final WeakReference<FocusFragment> focusFragmentWeakReference;

        CoinInfoDownloader(FocusFragment focusFragment) {
            focusFragmentWeakReference = new WeakReference<>(focusFragment);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected CryptoEntry doInBackground(String... url) {

            if (url != null) {
                return CryptoQuery.fetchFocusCrypto(url[0]);
            }

            return null;
        }

        @Override
        protected void onPostExecute(CryptoEntry result) {
            // set TEXTs after successful execution
            if (focusFragmentWeakReference.get() != null) {
                if (result != null) {
                    focusFragmentWeakReference.get().favImage.setVisibility(View.VISIBLE);
                    focusFragmentWeakReference.get().focusPrice.setVisibility(View.VISIBLE);
                    focusFragmentWeakReference.get().focusPrice.setText(formatCurrentRate(result.getCurrentRate()));
                    focusFragmentWeakReference.get().focusChange.setVisibility(View.VISIBLE);
                    focusFragmentWeakReference.get().focusChange.setText(formatChange(result.getChange()));
                    if (result.getChange() > 0) {
                        focusFragmentWeakReference.get().focusChange.setTextColor(Color.GREEN);
                    } else {
                        focusFragmentWeakReference.get().focusChange.setTextColor(Color.RED);
                    }
                } else {
                    focusFragmentWeakReference.get().focusPrice.setText(R.string.no_data_available);
                    focusFragmentWeakReference.get().focusChange.setText(R.string.no_data_available);
                }
            }
        }

        @NonNull
        private String formatCurrentRate(double currentRate) {

            DecimalFormat rateFormatter = new DecimalFormat("0.000##", new DecimalFormatSymbols(Locale.US));

            return rateFormatter.format(currentRate) + "$";
        }

        @NonNull
        private String formatChange(double change) {

            DecimalFormat rateFormatter = new DecimalFormat("0.00#", new DecimalFormatSymbols(Locale.US));

            StringBuilder changeSB = new StringBuilder(rateFormatter.format(change));

            if (change > 0) {
                changeSB.insert(0, "+");
            }

            changeSB.append("%");

            return changeSB.toString();
        }
    }
}
