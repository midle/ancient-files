package com.mindklik.cryptospy.views.fragments;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.mindklik.cryptospy.R;
import com.mindklik.cryptospy.controllers.data.CryptoContract;
import com.mindklik.cryptospy.controllers.fragmentControllers.FragmentWithAdapterBase;
import com.mindklik.cryptospy.model.CryptoEntry;
import com.mindklik.cryptospy.controllers.netQueries.CryptoQuery;

import java.lang.ref.WeakReference;


public class FavouriteFragment extends FragmentWithAdapterBase {

    @Override
    public void refreshDataInner() {

        super.refreshDataInner();

        SQLiteDatabase db = cryptoDBHelper.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + CryptoContract.CryptoDBEntry.TABLE_LIKE_NAME, null);

        entriesCount = cursor.getCount();

        if (cursor.moveToFirst()) {
            do {
                String entry = cursor.getString(cursor.getColumnIndex(CryptoContract.CryptoDBEntry.COLUMN_LIKE_COIN));
                new FavCoinsDownloader(this).execute(CryptoQuery.URL_CRYPTO_BASE + CryptoQuery.METHOD_LIKED_COINS + entry);

            } while (cursor.moveToNext());
        } else {
            hideProgressBar();
        }

        cursor.close();

        db.close();
    }

    @Override
    protected int setFragmentEmptyRes() {
        return R.string.no_fav_currencies_available;
    }

    //Downlaoder class
    private static class FavCoinsDownloader extends AsyncTask<String, Void, CryptoEntry> {

        private final WeakReference<FavouriteFragment> favFragmentWeakReference;

        private String tempName;

        FavCoinsDownloader(FavouriteFragment favouriteFragment) {
            favFragmentWeakReference = new WeakReference<>(favouriteFragment);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected CryptoEntry doInBackground(String... url) {
            if (url != null) {
                tempName = url[0].replace(CryptoQuery.URL_CRYPTO_BASE, "").replace(CryptoQuery.METHOD_LIKED_COINS, "");
                return CryptoQuery.fetchCryptoLikes(url[0]);
            }
            return null;
        }

        @Override
        protected void onPostExecute(CryptoEntry cryptoEntry) {
            if (favFragmentWeakReference.get() != null) {
                // set names
                favFragmentWeakReference.get().cryptoEntryAdapter.add(cryptoEntry);
                favFragmentWeakReference.get().entriesCount -= 1;

                if (favFragmentWeakReference.get().entriesCount == 0) {
                    favFragmentWeakReference.get().hideProgressBar();
                }
            } else {
                //set dummy coin
                favFragmentWeakReference.get().cryptoEntryAdapter.add(new CryptoEntry(tempName, 0, 0, 0, ""));
            }
        }
    }
}
