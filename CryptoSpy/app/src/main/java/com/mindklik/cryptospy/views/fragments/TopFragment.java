package com.mindklik.cryptospy.views.fragments;

import android.os.AsyncTask;

import com.mindklik.cryptospy.R;
import com.mindklik.cryptospy.controllers.fragmentControllers.FragmentWithAdapterBase;
import com.mindklik.cryptospy.controllers.netQueries.CryptoQuery;
import com.mindklik.cryptospy.model.CryptoEntry;

import java.lang.ref.WeakReference;
import java.util.ArrayList;


public class TopFragment extends FragmentWithAdapterBase {


    @Override
    public void refreshDataInner() {

        super.refreshDataInner();

        entriesCount = 1;

        new TopCoinsDownloader(this).execute(CryptoQuery.URL_CRYPTO_BASE + CryptoQuery.METHOD_TOP_COINS + sharedPreferences.getString(getString(R.string.crypto_number_key), getString(R.string.crypto_number)));
    }

    @Override
    protected int setFragmentEmptyRes() {
        return R.string.no_top_currencies_available;
    }

    // TopCoinsDownloader AsyncTask class
    private static class TopCoinsDownloader extends AsyncTask<String, Void, ArrayList<CryptoEntry>> {

        private final WeakReference<TopFragment> topFragmentWeakReference;

        TopCoinsDownloader(TopFragment topFragment) {
            topFragmentWeakReference = new WeakReference<>(topFragment);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<CryptoEntry> doInBackground(String... url) {
            if (url != null) {
                return CryptoQuery.fetchCryptos(url[0]);
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<CryptoEntry> result) {

            if (topFragmentWeakReference.get() != null) {

                topFragmentWeakReference.get().entriesCount -= 1;
                // set names
                topFragmentWeakReference.get().cryptoEntryAdapter.addAll(result);
                topFragmentWeakReference.get().hideProgressBar();
            }
        }
    }
}
