package com.mindklik.cryptospy.controllers.fragmentControllers;


import android.content.BroadcastReceiver;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.View;

import com.mindklik.cryptospy.CryptoSpyActivity;
import com.mindklik.cryptospy.controllers.data.CryptoDBHelper;

/**
 * Every fragment view shall have access to:
 * dataBase, rootview, sharedPreferences
 */


public abstract class FragmentBase extends Fragment {

    protected View rootView;

    protected SharedPreferences sharedPreferences;
    protected CryptoDBHelper cryptoDBHelper;
    protected BroadcastReceiver broadcastReceiverBack;

    protected abstract void refreshData();

    protected void initializeVariables() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(rootView.getContext());
        cryptoDBHelper = CryptoDBHelper.getCryptoDBHelper(rootView.getContext());
    }

    protected void hideProgressBar() {
        if (((CryptoSpyActivity) getActivity()) != null) {
            ((CryptoSpyActivity) getActivity()).progressBar.setVisibility(View.GONE);
        }
    }
}
