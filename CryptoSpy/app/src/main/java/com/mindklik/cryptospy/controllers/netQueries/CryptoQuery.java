package com.mindklik.cryptospy.controllers.netQueries;

import android.util.Log;

import com.mindklik.cryptospy.model.CryptoEntry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;


public class CryptoQuery {

    /**
     * URL Strings collection
     */
    public static final String URL_CRYPTO_BASE = "https://chasing-coins.com/api/v1";

    public static final String METHOD_TOP_COINS = "/top-coins/";
    public static final String COINS_NAME_LINK = "/coins";
    public static final String METHOD_LIKED_COINS = "/std/coin/";
    public static final String LOGO_LINK = "/std/logo/";

    public static final int CRYPTO_LOADER_TOP_ID = 6;


    //private constructor, so only static method works
    private CryptoQuery() {
    }


    //    creatingUrl in another method, for cleaning
    private static URL createUrl(String cryptoUrl) {

        URL newUrl = null;

        try {
            newUrl = new URL(cryptoUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return newUrl;
    }

    //    method for making http connection request
    private static String makeHttpReq(URL url) throws IOException {

        String json = "";

//        check the condition and return immediately if ==null
        if (url == null) {
            return json;
        }

        HttpURLConnection connection = null;
        InputStream inputStream = null;

        try {
            connection = (HttpURLConnection) url.openConnection();
            //for big lists (100+) it take quite long to get response from webAPI
            connection.setReadTimeout(15000);
            connection.setConnectTimeout(17500);
            connection.setRequestMethod("GET");
            connection.connect();

            // If the request was successful (response code 200),
            // then read the input stream and parse the response.
            if (connection.getResponseCode() == 200) {
                inputStream = connection.getInputStream();
                json = readFromStream(inputStream);
            } else {
                Log.i(CryptoQuery.class.toString(), "wrong response code");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }

        return json;
    }

    //    method for stream read
    private static String readFromStream(InputStream inputStream) throws IOException {

        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }


    public static ArrayList<CryptoEntry> fetchCryptos(String cryptoUrl) {

        URL newUrl = createUrl(cryptoUrl);

        String json = null;

        try {
            json = makeHttpReq(newUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return extractCurrencies(json);
    }

    //    method for extracting currencied from json
    private static ArrayList<CryptoEntry> extractCurrencies(String json) {

        ArrayList<CryptoEntry> cryptoList = new ArrayList<>();

        try {
            JSONObject cryptoObj = new JSONObject(json);

            for (int i = 1; i <= cryptoObj.length(); i++) {
                JSONObject currentObj = cryptoObj.getJSONObject(i + "");
                JSONObject currentChangeObj = currentObj.getJSONObject("change");

                String shortName = currentObj.getString("symbol");
                double cap = currentObj.getDouble("cap");
                double change = currentChangeObj.getDouble("day");
                double currentRate = currentObj.getDouble("price");
                String url = currentObj.getString("url");

                cryptoList.add(new CryptoEntry(shortName, cap, change, currentRate, url));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return cryptoList;
    }


    public static CryptoEntry fetchFocusCrypto(String cryptoUrl) {

        URL newUrl = createUrl(cryptoUrl);

        String json = null;

        try {
            json = makeHttpReq(newUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return extractFocusedCoin(json);
    }

    private static CryptoEntry extractFocusedCoin(String json) {

        CryptoEntry focusedCoin = null;

        try {
            JSONObject cryptoObj = new JSONObject(json);

            JSONObject currentChangeObj = cryptoObj.getJSONObject("change");

            String shortName = "";
            double cap = 0;
            double change = currentChangeObj.getDouble("day");
            double currentRate = cryptoObj.getDouble("price");
            String url = "";

            focusedCoin = new CryptoEntry(shortName, cap, change, currentRate, url);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return focusedCoin;
    }


    public static ArrayList<String> fetchNames(String cryptoUrl) {

        URL newUrl = createUrl(cryptoUrl);

        String json = null;

        try {
            json = makeHttpReq(newUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return extractNames(json);
    }

    private static ArrayList<String> extractNames(String json) {

        ArrayList<String> coinsList = new ArrayList<>();

        try {
            JSONArray cryptoObj = new JSONArray(json);

            for (int i = 0; i < cryptoObj.length(); i++) {

                String shortName = cryptoObj.get(i).toString();

                coinsList.add(shortName);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return coinsList;
    }


    public static Double fetchComparator(String cryptoUrl) {

        URL newUrl = createUrl(cryptoUrl);

        String json = null;

        try {
            json = makeHttpReq(newUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return extractComparator(json);
    }

    private static Double extractComparator(String json) {

        Double value = 1.0;

        try {
            JSONObject cryptoObj = new JSONObject(json);
            value = cryptoObj.getDouble("price");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return value;
    }


    public static CryptoEntry fetchCryptoLikes(String cryptoUrl) {

        URL newUrl = createUrl(cryptoUrl);

        String json = null;

        try {
            json = makeHttpReq(newUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String coinName = newUrl.toString().replace(URL_CRYPTO_BASE + METHOD_LIKED_COINS, "");

        return extractLikedCoins(json, coinName);
    }

    private static CryptoEntry extractLikedCoins(String json, String coinName) {

        CryptoEntry entry = null;

        try {
            JSONObject cryptoObj = new JSONObject(json);

            JSONObject currentChangeObj = cryptoObj.getJSONObject("change");

            double cap = 0;
            double change = currentChangeObj.getDouble("day");
            double currentRate = cryptoObj.getDouble("price");
            String url = "https://chasing-coins.com/coin/" + coinName;

            entry = new CryptoEntry(coinName, cap, change, currentRate, url);
        } catch (JSONException e) {
            e.printStackTrace();
            return new CryptoEntry(coinName, 0, 0, 0, "");
        }

        return entry;
    }


    public static CryptoEntry fetchPortfolio(String cryptoUrl) {

        URL newUrl = createUrl(cryptoUrl);

        String json = null;

        try {
            json = makeHttpReq(newUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String coinName = newUrl.toString().replace(URL_CRYPTO_BASE + METHOD_LIKED_COINS, "");

        return extractPortfolio(json, coinName);
    }


    private static CryptoEntry extractPortfolio(String json, String coinName) {

        CryptoEntry entry = null;

        try {
            JSONObject cryptoObj = new JSONObject(json);

            JSONObject currentChangeObj = cryptoObj.getJSONObject("change");

            double cap = 0;
            double change = currentChangeObj.getDouble("day");
            double currentRate = cryptoObj.getDouble("price");
            String url = "https://chasing-coins.com/coin/" + coinName;

            entry = new CryptoEntry(coinName, cap, change, currentRate, url);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return entry;
    }
}
