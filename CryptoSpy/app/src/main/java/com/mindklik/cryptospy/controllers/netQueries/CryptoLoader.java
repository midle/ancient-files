package com.mindklik.cryptospy.controllers.netQueries;

import android.content.AsyncTaskLoader;
import android.content.Context;

import java.util.ArrayList;


public class CryptoLoader extends AsyncTaskLoader<ArrayList<String>> {


    private String cryptoUrl;
    private int ID;

    public CryptoLoader(Context context, String url, int ID) {
        super(context);
        this.cryptoUrl = url;
        this.ID = ID;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    //fetch lists of coins from https://chasing-coins.com/
    @Override
    public ArrayList<String> loadInBackground() {

        if (ID == CryptoQuery.CRYPTO_LOADER_TOP_ID) {
            return CryptoQuery.fetchNames(cryptoUrl);
        }

        return null;
    }
}
