﻿
Shader "Custom/Sprites/Wavy"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_NoiseTex("Noise Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		
		_SizeX("_SizeX", Range(0, 0.01)) = 0.01
		_SizeY("_SizeY", Range(0, 0.01)) = 0.01
		_SpeedX("SpeedX", Range(0, 0.1)) = 0.1
		_SpeedY("SpeedY", Range(0, 0.1)) = 0.1
		_GlobalMulti("Global Multiplier", Range(0, 0.1)) = 0.1
	}

		SubShader
		{
			Tags
			{
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
			}

		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend SrcAlpha OneMinusSrcAlpha


			Pass
			{
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"
				#include "UnityUI.cginc"

				struct appdata_t
				{
					float4 vertex   : POSITION;
					float4 color    : COLOR;
					float2 texcoord : TEXCOORD0;
				};

				struct v2f
				{
					float4 vertex   : SV_POSITION;
					fixed4 color : COLOR;
					float2 texcoord  : TEXCOORD0;
					float4 worldPosition : TEXCOORD1;
				};

				fixed _SizeX;
				fixed _SizeY;
				fixed _SpeedX;
				fixed _SpeedY;
				fixed _GlobalMulti;

			fixed4 _Color;
			fixed4 _TextureSampleAdd;

				v2f vert(appdata_t IN)
				{
					v2f OUT;
					OUT.worldPosition = IN.vertex;
					OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);
					OUT.texcoord = IN.texcoord;
					OUT.color = IN.color * _Color;

					return OUT;
				}

				sampler2D _MainTex;
				sampler2D _NoiseTex;

				fixed4 SampleSpriteTexture(float2 uv)
				{
					fixed4 color = tex2D(_MainTex, uv);


					return color;
				}

				fixed4 frag(v2f IN) : COLOR
				{
					float2 displacedTexCoord = IN.texcoord + float2(
						tex2D(_NoiseTex, IN.vertex.xy * _SizeX + float2(_Time.w * _SpeedX, 0)).z -.5,
						tex2D(_NoiseTex, IN.vertex.xy * _SizeY + float2(0, _Time.w * _SpeedY)).z - .5
						) * _GlobalMulti;

					fixed4 result = (tex2D(_MainTex, displacedTexCoord)+ _TextureSampleAdd) * IN.color;
					return result;
				}
			ENDCG
			}
		}
}