﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMenu : MonoBehaviour
{

    [SerializeField] GameObject gameOverMenu, mainMenu, completedMenu;
    [SerializeField] GameObject[] stories;

    [SerializeField] Animator storyAnimator;

    int currentStory;

    [SerializeField] AudioSource audioSource;
    [SerializeField] GameObject skipButton;


    private void Awake()
    {
#if !UNITY_EDITOR
        mainMenu.SetActive(true);
        currentStory = 0;
#else
        mainMenu.SetActive(true);
        currentStory = 0;
#endif
    }

    public void ShowGameOver(bool show)
    {
        gameOverMenu.SetActive(show);
    }

    public void ShowGameCompleted()
    {
        audioSource.enabled = false;
        completedMenu.SetActive(true);
    }

    public bool IsPlayBlocked()
    {
        return gameOverMenu.activeSelf || mainMenu.activeSelf || completedMenu.activeSelf;
    }


    public void PlayStory()
    {
        stories[currentStory].SetActive(true);
        storyAnimator.SetTrigger("Flash");
        skipButton.SetActive(true);
    }

    [SerializeField] PlayerController pc;
    public void Play()
    {
        mainMenu.SetActive(false);
        pc.LevelUp();
    }

    public void Restart()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void NextSlide()
    {
        stories[currentStory].SetActive(false);
        currentStory++;
        stories[currentStory].SetActive(true);
        storyAnimator.SetTrigger("Flash");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene("GameScene");
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
