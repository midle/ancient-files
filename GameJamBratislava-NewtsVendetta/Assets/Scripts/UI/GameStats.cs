﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class GameStats : MonoBehaviour
{

    [SerializeField] Image powerBar, foodBar;


    public void SetPowerFill(float fillPercentage)
    {
        powerBar.fillAmount = fillPercentage;
    }

    public void DeactivatePowerBar()
    {
        powerBar.fillAmount = 0f;
    }

    public void SetFoodFill(float fillPoints)
    {
        foodBar.fillAmount = fillPoints;
    }

    public void DeactivateFoodBar()
    {
        foodBar.fillAmount = 0f;
    }

}
