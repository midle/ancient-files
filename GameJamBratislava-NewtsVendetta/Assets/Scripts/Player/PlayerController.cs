﻿using System.Collections;
using UnityEngine;

public enum StageLevel
{
    KIJANKA, KIJANKA_RAMBO, RAMBO_EVO, RAMBO_MASTER
}

public class PlayerController : MonoBehaviour
{

    [SerializeField] StageLevel currentStage;
    [SerializeField] int[] foodPointsToLevel;

    [SerializeField] GameObject playerModel;

    [SerializeField] Camera cam;

    [SerializeField] float radius;
    [SerializeField] float force = 1000f;

    Rigidbody2D rb;
    Vector2 startingPos;
    Vector2 direction;

    [SerializeField] GameStats gameStats;
    [SerializeField] EnviroController enviroController;

    [SerializeField] GameMenu gameMenu;

    PlayerCollider playerCollider;
    PlayerPowerLine playerPowerLine;

    int currentFoodPoints;


    [SerializeField] AnimatorOverrideController[] animators;
    public Animator animator;


    public AudioSource audioSource;
    [SerializeField] AudioClip jumpClip;
    [SerializeField] AudioClip laughEvil;
    [SerializeField] AudioClip gameWin;


    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        playerCollider = GetComponent<PlayerCollider>();
        playerPowerLine = GetComponent<PlayerPowerLine>();

        enviroController.Init(transform);
        playerCollider.Init(this);
        playerPowerLine.Init();

        gameStats.DeactivatePowerBar();
        gameStats.DeactivateFoodBar();

        startingPos = transform.position;

        //TODO CHANGE
        isIdle = true;
        //LevelUp();
        //currentStage++;
        //animator.SetTrigger("Set1");
    }

    //private void OnMouseDrag()
    //{
    //    if (isIdle && !gameMenu.IsPlayBlocked())
    //    {
    //        Vector2 camPoint = cam.ScreenToWorldPoint(Input.mousePosition);

    //        startingPos = transform.position;
    //        direction = camPoint - startingPos;
    //        if (direction.magnitude > radius)
    //            direction = direction.normalized * radius;

    //        float t = direction.magnitude / radius;
    //        gameStats.SetPowerFill(t);

    //        playerPowerLine.DrawPoints(t, startingPos, direction);
    //    }
    //}

    //private void OnMouseUp()
    //{
    //    if (isIdle && !gameMenu.IsPlayBlocked())
    //    {
    //        rb.bodyType = RigidbodyType2D.Dynamic;

    //        playerPowerLine.DeactivatePoints();
    //        gameStats.DeactivatePowerBar();

    //        rb.AddForce(-direction * force);
    //        isIdle = false;
    //        animator.SetTrigger("Jump");

    //        if (jumpClip != null)
    //        {
    //            audioSource.PlayOneShot(jumpClip);
    //        }
    //    }
    //}

    bool setBgPass;
    bool isIdle;
    private void Update()
    {
        if (!gameMenu.IsPlayBlocked())
        {
            if (setBgPass)
            {
                enviroController.SetYForBg();
                setBgPass = false;
            }

            //Debug.Log(rb.velocity);
            if (rb.velocity.magnitude < 0.01f)
            {

                if (!isIdle && animator.GetCurrentAnimatorStateInfo(0).IsName("Fly"))
                {
                    animator.SetTrigger("Idle");
                    isIdle = true;
                }
            }

            if (!isCoroStarted && (int)currentStage >= 1)
            {
                StartCoroutine(enemySpawnC());
                isCoroStarted = true;
            }

            if (Input.GetMouseButton(0))
            {
                if (isIdle)
                {
                    Vector2 camPoint = cam.ScreenToWorldPoint(Input.mousePosition);

                    if (!isStartingPosSubscribed)
                    {
                        startingPos = camPoint;
                        isStartingPosSubscribed = true;
                    }
                    direction = camPoint - startingPos;
                    if (direction.magnitude > radius)
                        direction = direction.normalized * radius;

                    float t = direction.magnitude / radius;
                    gameStats.SetPowerFill(t);

                    playerPowerLine.DrawPoints(t, startingPos, direction);
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                if (isIdle)
                {
                    rb.bodyType = RigidbodyType2D.Dynamic;

                    playerPowerLine.DeactivatePoints();
                    gameStats.DeactivatePowerBar();

                    rb.AddForce(-direction * force);
                    isIdle = false;
                    animator.SetTrigger("Jump");

                    if (jumpClip != null)
                    {
                        audioSource.PlayOneShot(jumpClip);
                    }

                    isStartingPosSubscribed = false;
                }
            }
        }
    }

    bool isStartingPosSubscribed;


    public void LevelUp()
    {
        if ((int)currentStage < 3)
        {
            if ((int)currentStage == 0)
            {
                rb.gravityScale = 1.5f;
                rb.mass = 1f;
            }
            if ((int)currentStage == 1)
            {
                //setBgPass = true;
                rb.gravityScale = 1.65f;
                rb.mass = 1.2f;
            }
            if ((int)currentStage == 2)
            {
                setBgPass = true;
                rb.gravityScale = 1.8f;
                rb.mass = 1.35f;
            }

            currentFoodPoints = 0;
            gameStats.SetFoodFill(currentFoodPoints);

            animator.runtimeAnimatorController = animators[(int)currentStage];

            currentStage++;

            if ((int)currentStage == 3)
            {
                Debug.Log("cstage");
                enviroController.SpawnTheNest(transform.position.y);
                setBgPass = true;
                rb.gravityScale = 2f;
                rb.mass = 1.5f;
            }

            //TODO CHANGE HERE
            //if ((int)currentStage == 1)
            //{
            //    setBgPass = true;
            //}
        }
    }

    public void Eat(Food food)
    {
        currentFoodPoints += food.foodPoints;

        if (currentFoodPoints >= foodPointsToLevel[(int)currentStage])
        {
            LevelUp();
        }

        float fillPercentage = (float)currentFoodPoints / foodPointsToLevel[(int)currentStage];
        gameStats.SetFoodFill(fillPercentage);

        Destroy(food.gameObject);
    }

    public void GameOver()
    {
        if (laughEvil != null)
        {
            audioSource.PlayOneShot(laughEvil);
        }
        gameMenu.ShowGameOver(true);
        playerModel.SetActive(false);
    }

    public void GameCompleted()
    {
        gameMenu.ShowGameCompleted();
        enviroController.DeactivateEnemies();

        if (gameWin != null)
        {
            audioSource.PlayOneShot(gameWin);
        }

        playerPowerLine.DeactivatePoints();
        gameStats.DeactivatePowerBar();


        rb.gravityScale = 0f;
        rb.simulated = false;
    }

    [SerializeField] Vector2 spawnTimeMinMax;
    bool isCoroStarted;
    //Coroutine enemyCoro;
    IEnumerator enemySpawnC()
    {
        while (true)
        {
            float timer = Random.Range(spawnTimeMinMax.x, spawnTimeMinMax.y);

            yield return new WaitForSeconds(timer);

            while (!isIdle || gameMenu.IsPlayBlocked())
            {
                yield return null;
            }


            if ((int)currentStage > 2)
            {
                enviroController.SpawnEnemy(1, transform.position.y);
            }
            else
            {
                enviroController.SpawnEnemy(0, transform.position.y);
            }
        }
    }
}
