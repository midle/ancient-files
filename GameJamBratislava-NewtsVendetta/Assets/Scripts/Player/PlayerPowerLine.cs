﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerPowerLine : MonoBehaviour
{

    [SerializeField] int plotPointsAmount;
    [SerializeField] SpriteRenderer plotPointPrefab;
    [SerializeField] Transform tempParent;
    [SerializeField] float minH;

    List<SpriteRenderer> plotPoints;
    Color[] colors;
    const float maxH = 360f;


    public void Init()
    {
        plotPoints = new List<SpriteRenderer>();
        colors = new Color[plotPointsAmount];

        for (int i = 0; i < plotPointsAmount; i++)
        {
            plotPoints.Add(Instantiate(plotPointPrefab, Vector3.zero, Quaternion.identity, tempParent));

            float h = Mathf.Lerp(minH / maxH, 0, (float)i / (plotPointsAmount - 1));
            colors[i] = Color.HSVToRGB(h, 1, 1);
        }

        DeactivatePoints();
    }

    public void DrawPoints(float t, Vector2 startingPos, Vector2 direction)
    {
        int points = Mathf.FloorToInt(Mathf.Lerp(1, plotPointsAmount, t));

        for (int i = 0; i < plotPointsAmount; i++)
        {
            if (i < points)
            {
                plotPoints[i].enabled = true;
                plotPoints[i].transform.position = startingPos + (direction / points * i);
                plotPoints[i].color = colors[i];
            }
            else
            {
                plotPoints[i].enabled = false;
            }
        }
    }

    public void DeactivatePoints()
    {
        for (int i = 0; i < plotPointsAmount; i++)
        {
            plotPoints[i].enabled = false;
        }
    }
}
