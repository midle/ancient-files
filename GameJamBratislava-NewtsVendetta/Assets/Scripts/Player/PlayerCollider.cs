﻿using UnityEngine;

public class PlayerCollider : MonoBehaviour
{

    [SerializeField] CameraShaker camShake;

    [SerializeField] CapsuleCollider2D ac, acFull;
    CapsuleCollider2D playerCollider;
    PlayerController playerController;

    public AudioSource audioSource;
    [SerializeField] AudioClip eatSound, obstacleSound;

    [SerializeField] GameObject collectParticles;


    public void Init(PlayerController playerController)
    {
        playerCollider = GetComponent<CapsuleCollider2D>();
        this.playerController = playerController;

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Obstacle") || collision.transform.CompareTag("Ground") || collision.transform.CompareTag("Wall"))
        {
            camShake.Shake();
            if (obstacleSound != null)
            {
                audioSource.PlayOneShot(obstacleSound);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Food"))
        {
            Food food = collision.gameObject.GetComponent<Food>();
            playerController.Eat(food);
            if (eatSound != null)
            {
                audioSource.PlayOneShot(eatSound);
            }

            var obj = Instantiate(collectParticles);
            obj.transform.position = collision.transform.position;
            Destroy(obj, 3f);
        }


        if (collision.transform.CompareTag("Obstacle"))
        {
            playerCollider.enabled = false;
            acFull.enabled = true;
        }

        if (collision.transform.CompareTag("Enemy"))
        {
            camShake.Shake(1f, 0.06f, 1f);
            playerController.GameOver();
        }

        if (collision.transform.CompareTag("Nest"))
        {
            camShake.Shake(1f, 0.06f, 5f);
            collision.gameObject.SetActive(false);
            playerController.GameCompleted();
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Obstacle"))
        {
            if (!acFull.IsTouchingLayers())
            {
                acFull.enabled = false;
                //ac.enabled = false;

                playerCollider.enabled = true;
            }
        }
    }

}
