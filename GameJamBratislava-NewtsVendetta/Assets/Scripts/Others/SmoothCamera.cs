﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothCamera : MonoBehaviour
{
    [SerializeField]
    float FollowSpeed = 2f;
    [SerializeField]
    public Transform target;
    [SerializeField]
    public Vector2 cameraMinMax;

    [SerializeField] GameObject postProcessObj;

    float zPos;

    private void Awake()
    {
#if UNITY_ANDROID || UNITY_IOS
        Application.targetFrameRate = 60;
        postProcessObj.SetActive(false);
#endif
        zPos = transform.position.z;
    }

    private void Update()
    {
        Vector3 newPosition = target.position;
        newPosition.x = Mathf.Clamp(target.position.x, cameraMinMax.x, cameraMinMax.y);
        newPosition.z = zPos;

        if (newPosition.y < -3f)
        {
            newPosition.y = -3f;
        }

        transform.position = Vector3.Slerp(transform.position, newPosition, FollowSpeed * Time.deltaTime);
    }
}
