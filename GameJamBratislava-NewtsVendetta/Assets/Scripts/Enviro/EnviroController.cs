﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnviroController : MonoBehaviour
{

    [SerializeField] EnviroBgController bgController;

    [SerializeField] Transform wallParent;
    [SerializeField] Transform[] wallPrefab;
    int wallNo;
    [SerializeField] float wallDist = 13.8f, wallRX, wallLX;

    Transform playerTr;

    List<Transform> wallsR = new List<Transform>();
    List<Transform> wallsL = new List<Transform>();


    [SerializeField] Transform obstacledParent;
    [SerializeField] Obstacle[] obstacles;
    [SerializeField] float obstacleDist;
    int obstaclesCount;
    [SerializeField] Transform fish;


    [SerializeField] Transform foodParent;
    [SerializeField] Food[] foods;
    [SerializeField] int maxFoodsPerIteration;


    [SerializeField] Transform enemyParent;
    [SerializeField] Enemy[] enemies;


    public void Init(Transform playerTr)
    {
        this.playerTr = playerTr;

        for (int i = 0; i < 2; i++)
        {
            SpawnWallR();
            SpawnWallL();
        }

        for (int i = 0; i < 3; i++)
        {
            SpawnRandomFood();
            SpawnRandomObstacle();
        }

        SpawnFish();
    }

    public void SetYForBg()
    {
        bgController.SetPlayerTransform(playerTr.position.y);
        wallNo = 1;
    }

    void SpawnWallR()
    {
        float y = wallsR.Count * wallDist;
        wallsR.Add(Instantiate(wallPrefab[wallNo], new Vector3(0, 0, 0), Quaternion.identity, wallParent));

        var wallTr = wallsR[wallsR.Count - 1].transform;
        wallTr.localPosition = new Vector3(wallRX, y, 0);
    }

    void SpawnWallL()
    {
        float y = wallsL.Count * wallDist;

        wallsL.Add(Instantiate(wallPrefab[wallNo], new Vector3(0, 0, 0), Quaternion.identity, wallParent));


        var wallTr = wallsL[wallsL.Count - 1].transform;
        wallTr.localPosition = new Vector3(wallLX, y, 0);
        wallTr.localScale = new Vector3(-wallTr.localScale.x, wallTr.localScale.y, wallTr.localScale.z);
    }


    void SpawnRandomObstacle()
    {
        int obstacleNo = Random.Range(0, obstacles.Length);

        obstacles[obstacleNo].Spawn(obstacledParent, obstaclesCount * obstacleDist);

        obstaclesCount++;
    }

    void SpawnRandomFood()
    {
        int randomNo = Random.Range(0, maxFoodsPerIteration + 1);
        int foodNo = Random.Range(0, foods.Length);

        for (int i = 0; i < randomNo; i++)
        {
            foods[foodNo].Spawn(foodParent, obstaclesCount * obstacleDist);
        }
    }

    void SpawnFish()
    {
        if (wallNo == 0)
        {
            fishSpawn++;
            if (fishSpawn == 3)
            {
                Vector2 newPos = new Vector2(Random.Range(-15, 15), playerTr.position.y + 15f);
                Instantiate(fish, newPos, Quaternion.identity, obstacledParent);

                fishSpawn = 0;
            }
        }
    }


    private void Update()
    {
        var x = wallsR.Count * wallDist;
        if (playerTr.position.y + 2 * wallDist > x)
        {
            SpawnWallR();
            SpawnWallL();
        }

        var y = obstaclesCount * obstacleDist;
        if (playerTr.position.y + 4 * obstacleDist > y)
        {
            SpawnRandomObstacle();
            SpawnRandomFood();

            SpawnFish();
        }
    }
    int fishSpawn;

    public void SpawnEnemy(int enemyNo, float playerY)
    {
        enemies[enemyNo].Spawn(playerY, enemyNo == 1);
    }

    [SerializeField] TheNest theNest;
    public void SpawnTheNest(float playerY)
    {
        theNest.Spawn(playerY);
    }

    public void DeactivateEnemies()
    {
        foreach (var enemy in enemies)
        {
            enemy.gameObject.SetActive(false);
        }
    }

}
