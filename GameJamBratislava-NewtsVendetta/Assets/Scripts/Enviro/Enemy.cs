﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    [SerializeField] Vector2 spawnYMinMax, spawnXMinMax, speedMinMax;

    [SerializeField] float speed = 5f;
    [SerializeField] bool flipped;

    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip spawnClip;


    float startingX;

    public void Spawn(float y, bool idioticBirdFlippedWhyNot)
    {
        speed = Random.Range(speedMinMax.x, speedMinMax.y);
        flipped = Random.Range(0, 2) == 1;

        float newX = Random.Range(spawnXMinMax.x, spawnXMinMax.y);
        float newY = Random.Range(spawnYMinMax.x, spawnYMinMax.y) + y;

        newX = flipped ? newX : -newX;

        Vector3 newPos = transform.position;
        if (idioticBirdFlippedWhyNot)
        {
            newPos.x = -newX;
        }
        else
        {
            newPos.x = newX;
        }
        newPos.y = newY;
        transform.position = newPos;



        Vector3 newScale = transform.localScale;
        if (flipped)
        {
            if (newScale.x > 0)
            {
                newScale.x = -newScale.x;
            }
            transform.localScale = newScale;
        }
        else
        {
            if (newScale.x < 0)
            {
                newScale.x = -newScale.x;
            }
            transform.localScale = newScale;
        }

        if (idioticBirdFlippedWhyNot)
        {
            speed = -speed;
        }

        startingX = transform.position.x;

        gameObject.SetActive(true);

        if (spawnClip!=null)
        {
            audioSource.PlayOneShot(spawnClip);
        }
    }


    private void Update()
    {
        Move();
    }

    void Move()
    {
        if (flipped)
        {
            transform.Translate(Vector3.right * speed * Time.deltaTime);
        }
        else
        {
            transform.Translate(-Vector3.right * speed * Time.deltaTime);
        }

        if (startingX < 0 && transform.position.x > 30)
        {
            gameObject.SetActive(false);
        }
        else if (startingX > 0 && transform.position.x < -30)
        {
            gameObject.SetActive(false);
        }
    }
}
