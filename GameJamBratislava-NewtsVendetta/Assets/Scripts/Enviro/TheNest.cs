﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheNest : MonoBehaviour
{

    [SerializeField] Vector2 xMinMax, yMinMax;

    public void Spawn(float playerY)
    {
        float xVar = Random.Range(xMinMax.x, xMinMax.y);
        float yVar = Random.Range(yMinMax.x, yMinMax.y) + playerY;

        Vector3 newPos = new Vector3(xVar, yVar, 0);
        transform.position = newPos;

        gameObject.SetActive(true);
    }

}
