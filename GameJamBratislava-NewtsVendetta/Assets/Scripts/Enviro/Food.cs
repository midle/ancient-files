﻿using UnityEngine;

public class Food : MonoBehaviour
{

    [SerializeField] Vector2 xMinMax, yminMax;

    public int foodPoints;

    public void Spawn(Transform parent, float y)
    {
        float xPos = Random.Range(xMinMax.x, xMinMax.y);

        float randomYVar = Random.Range(yminMax.x, yminMax.y);

        if (Random.Range(0, 2) == 1)
        {
            randomYVar = -randomYVar;
        }

        Vector2 newPos = new Vector2(xPos, y + randomYVar);

        var obj = Instantiate(this, newPos, Quaternion.identity, parent);
    }
}
