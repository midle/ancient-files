﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public const float yVar = 1f;
    [SerializeField] Vector2 xMinMax, scaleMinMaxX, scaleMinMaxY;


    public void Spawn(Transform parent, float y)
    {
        float xPos = Random.Range(xMinMax.x, xMinMax.y);

        float randomXScale = Random.Range(scaleMinMaxX.x, scaleMinMaxX.y);
        float randomYScale = Random.Range(scaleMinMaxY.x, scaleMinMaxY.y);
        float randomYVar = Random.Range(-yVar, yVar);

        if (Random.Range(0, 2) == 1)
        {
            xPos = -xPos;
            randomXScale = -randomXScale;
        }

        Vector2 newPos = new Vector2(xPos, y + randomYVar);

        var obj = Instantiate(this, newPos, Quaternion.identity, parent);

        Vector3 newScale = new Vector3(randomXScale, randomYScale, 1f);
        obj.transform.localScale = newScale;
    }
}
