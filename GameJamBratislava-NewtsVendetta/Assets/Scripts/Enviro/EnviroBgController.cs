﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnviroBgController : MonoBehaviour
{

    [SerializeField] Vector2 bgMinMax;
    [SerializeField] Transform playerPos;

    float playerY;
    bool canScroll;

    [SerializeField] SpriteRenderer rend;


    public void SetPlayerTransform(float x)
    {
        playerY = x;
        canScroll = true;

        //rend.sortingOrder = 10;
    }

    public void ScrollBg()
    {
        if (canScroll)
        {
            float newY = Mathf.Clamp(playerPos.position.y - playerY, bgMinMax.x, bgMinMax.y);

            transform.localPosition = new Vector3(transform.localPosition.x, -newY, transform.localPosition.z);
        }
    }

    private void Update()
    {
        ScrollBg();
    }
}
