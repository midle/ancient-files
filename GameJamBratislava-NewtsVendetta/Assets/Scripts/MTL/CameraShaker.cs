﻿using System.Collections;
using UnityEngine;

public class CameraShaker : MonoBehaviour
{

    Transform camTransform;
    Vector3 originalPos;
    Coroutine shakeCoroutine;

    public float shakeStrength = 5.0f;
    public float shakeRelaxationMin = 0.05f;
    public float shakeTime = 1.0f;
    public bool onEnableShake;

    void Awake()
    {
        GetRefs();
    }

    private void OnEnable()
    {
        if (onEnableShake)
        {
            Shake();
        }
    }

    void GetRefs()
    {
        if (camTransform == null)
        {
            camTransform = GetComponent(typeof(Transform)) as Transform;
        }

        originalPos = camTransform.localPosition;
    }

    public void Shake()
    {
        shakeCoroutine = StartCoroutine(ShakeC());
    }

    IEnumerator ShakeC()
    {
        float shakeTimer = shakeTime;
        float shakeRelaxationFactor = 1;
        originalPos = camTransform.localPosition;
        while (shakeTimer > 0)
        {
            camTransform.localPosition = originalPos + Random.insideUnitSphere * shakeStrength * shakeRelaxationFactor;

            float t = (shakeTimer / shakeTime);
            shakeRelaxationFactor = Mathf.Lerp(shakeRelaxationMin, 1, t);

            shakeTimer -= Time.deltaTime;

            yield return null;
        }

        camTransform.localPosition = originalPos;
    }

    public void Shake(float shakeStrength, float shakeRelaxationMin, float shakeTime)
    {
        shakeCoroutine = StartCoroutine(ShakeC(shakeStrength, shakeRelaxationMin, shakeTime));
    }

    public void StopShaking()
    {
        if (shakeCoroutine != null)
        {
            StopCoroutine(shakeCoroutine);
        }

        camTransform.localPosition = originalPos;
    }

    IEnumerator ShakeC(float _shakeStrength, float _shakeRelaxationMin, float _shakeTime)
    {
        float shakeTimer = _shakeTime;
        float shakeRelaxationFactor = 1;
        originalPos = camTransform.localPosition;
        while (shakeTimer > 0)
        {
            camTransform.localPosition = originalPos + Random.insideUnitSphere * _shakeStrength * shakeRelaxationFactor;

            float t = (shakeTimer / _shakeTime);
            shakeRelaxationFactor = Mathf.Lerp(_shakeRelaxationMin, 1, t);

            shakeTimer -= Time.deltaTime;

            yield return null;
        }

        camTransform.localPosition = originalPos;
    }

}
