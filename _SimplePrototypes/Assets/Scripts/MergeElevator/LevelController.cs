﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MergeElevator
{
    public class LevelController : MonoBehaviour
    {

        public int levelNo;

#pragma warning disable 0649
        [SerializeField] Level levelPrefab;
        [SerializeField] Destiny destinyPrefab;
        [SerializeField] SpawnableBall targetPrefab;
        [SerializeField] Player player;
        [SerializeField] Enemy enemyPrefab;
        [SerializeField] Vector2 xPosMinMax, zPosMinMax;
        [SerializeField] float initialPlayerVol = 1f, targetTimeToMaxVol = 3f;
        [SerializeField] Vector2 initialTargetVolMinMax = new Vector2(0.5f, 3f);
        [SerializeField] Vector2 forceMinMax;
#pragma warning restore 0649

        float lastTargetPlacementTime;

        Transform levelsParent, targetParent, currentLevel, enemiesParent;
        Destiny currentDestiny;

        Queue<SpawnableItem> targetsQueue;
        List<Enemy> enemies;

        public UnityAction<Vector3> OnTargetSpawn;
        public UnityAction OnTargetCollected, OnPlayerAtDestiny, OnReset;

        bool isPlayerAtDestiny;


        //Initialize what need to be inited before all
        private void Awake()
        {
            targetsQueue = new Queue<SpawnableItem>();
            enemies = new List<Enemy>();

            lastTargetPlacementTime = Time.time;
        }


        public void Init(UnityAction resetAction)
        {
            SubscribeActions();

            SpawnParents();
            SpawnLevel();
            SpawnPlayer();
            SpawnDestiny();
            OnReset += resetAction;
        }

        void SubscribeActions()
        {
            OnTargetSpawn += SpawnTarget;
            OnTargetCollected += CollectTarget;
            OnPlayerAtDestiny += PlayerArrivedAtDestiny;
        }


        #region Spawning
        void SpawnParents()
        {
            levelsParent = new GameObject("LevelsParent").transform;
            targetParent = new GameObject("TargetParent").transform;
            targetParent.parent = levelsParent;
            enemiesParent = new GameObject("EnemiesParent").transform;
            enemiesParent.parent = levelsParent;
        }

        void SpawnLevel()
        {
            currentLevel = Instantiate(levelPrefab, levelsParent).transform;

            //give ref to the floor with input check
            InputFloor inputFloor = currentLevel.GetComponentInChildren<InputFloor>();
            if (inputFloor != null)
            {
                inputFloor.Init(OnTargetSpawn);
            }
            else
            {
                Debug.LogWarning("No InputFloor script attached to LEVEL PREFAB!");
            }
        }

        void SpawnPlayer()
        {
            //randomize x&z position + check model scale and adjust to a point contact on y
            float newX = Random.Range(xPosMinMax.x, xPosMinMax.y);
            float newZ = Random.Range(zPosMinMax.x, zPosMinMax.y);
            float newY = player.transform.localScale.y / 2;
            Vector3 newPos = new Vector3(newX, newY, newZ);

            player = player.Spawn(newPos);
            player.SetVolume(true, initialPlayerVol);
            player.SetParent(levelsParent);
            player.SetCollectAction(CollectTarget);
        }

        void SpawnDestiny()
        {
            currentDestiny = Instantiate(destinyPrefab, GetDestinyPos(), Quaternion.identity, levelsParent);
            currentDestiny.Init(OnPlayerAtDestiny);
        }

        Vector3 GetDestinyPos()
        {
            int destinyNo = ChooseDestinySpawnPoint();
            float newY = currentLevel.position.y + destinyPrefab.transform.localScale.y / 2;
            Vector3 newPos = new Vector3(levelPrefab.destinySpawnPoints[destinyNo].position.x, newY, levelPrefab.destinySpawnPoints[destinyNo].position.z);
            return newPos;
        }

        void RelocateDestiny()
        {
            currentDestiny.transform.position = GetDestinyPos();
        }

        int ChooseDestinySpawnPoint()
        {
            int spawnPointNo;
            do
            {
                spawnPointNo = Random.Range(0, levelPrefab.destinySpawnPoints.Length);
            } while ((levelPrefab.destinySpawnPoints[spawnPointNo].position.x < 0 && player.transform.position.x < 0) || (levelPrefab.destinySpawnPoints[spawnPointNo].position.x >= 0 && player.transform.position.x >= 0));


            return spawnPointNo;
        }

        void SpawnTarget(Vector3 pos) //x and z coords, y from floor
        {
            float newY = currentLevel.position.y + targetPrefab.transform.localScale.y / 2;
            Vector3 newPos = new Vector3(pos.x, newY, pos.z);

            float t = Mathf.Clamp01((Time.time - lastTargetPlacementTime) / targetTimeToMaxVol);
            float targetVol = Mathf.Lerp(initialTargetVolMinMax.x, initialTargetVolMinMax.y, t);
            Debug.Log("t: " + t);
            Debug.Log("targetVol: " + targetVol);
            var targetObj = targetPrefab.Spawn(newPos);
            targetObj.SetVolume(true, targetVol);
            targetObj.SetParent(targetParent);
            targetsQueue.Enqueue(targetObj);

            if (targetsQueue.Count == 1) //means its first target & player is not moving
            {
                player.ReferenceTarget(targetsQueue.Peek());
            }

            lastTargetPlacementTime = Time.time;
        }

        void SpawnEnemy(Vector3 pos, float volume)
        {
            var enemyObj = enemyPrefab.Spawn(pos) as Enemy;
            enemyObj.Init(OnReset);
            enemyObj.SetVolume(true, volume);
            enemyObj.SetParent(enemiesParent);
            enemyObj.Reposition();
            enemies.Add(enemyObj);
        }

        #endregion

        void CollectTarget()
        {
            //dequeue ONLY after collection
            if (targetsQueue.Count > 0)
            {
                targetsQueue.Dequeue();

                if (CheckNextLevelCondition())
                {
                    NextLevel();
                    return;
                }
            }

            //if there are targets in queue -> go to them
            if (targetsQueue.Count > 0)
            {
                player.ReferenceTarget(targetsQueue.Peek());
            }
        }

        void PlayerArrivedAtDestiny()
        {
            isPlayerAtDestiny = true;
        }

        bool CheckNextLevelCondition()
        {
            if (isPlayerAtDestiny)
            {
                isPlayerAtDestiny = false;
                return true;
            }

            return false;
        }


        void NextLevel()
        {
            Debug.Log("NEXT LEVEL GO!");

            levelNo++;

            RelocateDestiny();
            DestroyAllTargetsLeft();
            DeleteAllEnemies();

            CreateEnemies();
            AddForceToEnemies();

            player.Reset();
        }

        void DestroyAllTargetsLeft()
        {
            while (targetsQueue.Count > 0)
            {
                var targetObj = targetsQueue.Dequeue();
                Destroy(targetObj.gameObject);
            }
        }

        void DeleteAllEnemies()
        {
            foreach (var enemy in enemies)
            {
                Destroy(enemy.gameObject);
            }

            //clear the List after deletion
            enemies.Clear();
        }

        void CreateEnemies()
        {
            //-1 cuz player is 1
            int enemiesNo = player.GetDissolvedNumber() - 1 + levelNo;
            float enemyVolume = player.GetDissolvedVolume();

            for (int i = 0; i < enemiesNo; i++)
            {
                SpawnEnemy(player.transform.position, enemyVolume);
            }
        }

        void AddForceToEnemies()
        {
            foreach (var enemy in enemies)
            {
                float posX = Random.Range(-1f, 1f);
                float posZ = Random.Range(-1f, 1f);
                Vector3 forceDir = new Vector3(posX, 0, posZ);

                float force = Random.Range(forceMinMax.x, forceMinMax.y);

                enemy.ApplyForce(forceDir, force);
            }
        }
    }
}

