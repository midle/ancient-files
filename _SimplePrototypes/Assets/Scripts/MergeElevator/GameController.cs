﻿using UnityEngine;
using UnityEditor.SceneManagement;

namespace MergeElevator
{

    // clean-up & 1 controlling obj for proto purpose
    [RequireComponent(typeof(LevelController))]
    [RequireComponent(typeof(InputController))]
    public class GameController : MonoBehaviour
    {

        LevelController levelController;
        InputController inputController;

        bool isGameStarted;


        private void Awake()
        {
            levelController = GetComponent<LevelController>();
            inputController = GetComponent<InputController>();
        }


        void Start()
        {
            levelController.Init(ResetGame);
            inputController.Init(this);
        }

        public void StartGame()
        {
            if (!isGameStarted)
            {
                isGameStarted = true;
                // todo: implement starting
                Debug.Log("START");
            }
        }

        public void ResetGame()
        {
            // todo: implement resetting
            Debug.Log("RESET");
            EditorSceneManager.LoadScene("MergeElevator");
        }

    }

}