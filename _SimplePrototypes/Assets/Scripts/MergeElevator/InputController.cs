﻿using UnityEngine;

namespace MergeElevator
{
    public class InputController : MonoBehaviour
    {

        GameController gameController;


        public void Init(GameController _gc)
        {
            gameController = _gc;
        }


        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                gameController.StartGame();
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                gameController.ResetGame();
            }
        }

    }
}