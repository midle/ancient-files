﻿using UnityEngine;
using UnityEngine.Events;

namespace MergeElevator
{
    public class InputFloor : MonoBehaviour
    {

        UnityAction<Vector3> OnClickAction;


        int layer_mask;


        private void Awake()
        {
            layer_mask = LayerMask.GetMask("Ground");
        }


        public void Init(UnityAction<Vector3> action)
        {
            OnClickAction = action;
        }


        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out RaycastHit hit, 300f, layer_mask))
                    OnClickAction?.Invoke(hit.point);
            }
        }
    }
}
