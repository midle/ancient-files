﻿using UnityEngine;

namespace MergeElevator
{
    public abstract class SpawnableItem : MonoBehaviour, ISpawnable<SpawnableItem>
    {
        public SpawnableItem Spawn(Vector3 pos)
        {
            return Instantiate(this, pos, Quaternion.identity);
        }

        public void SetParent(Transform parent)
        {
            transform.parent = parent;
        }
        public abstract float CheckVolume();
        public abstract void SetVolume(bool isInitial, float volume);
    }
}