﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace MergeElevator
{
    public class Player : SpawnableBall
    {

        [SerializeField] float speed = 6f;

        SpawnableItem target;
        UnityAction collectAction;

        int collectedSpawns;

        [SerializeField] Collider col;
        Coroutine playerStartingCoro;
        WaitForSeconds playerWaitingTime = new WaitForSeconds(0.69f);


        private void Awake()
        {
            col = GetComponent<Collider>();
        }


        //Got to know we are spawning THE Player
        public new Player Spawn(Vector3 pos)
        {
            return Instantiate(this, pos, Quaternion.identity);
        }

        public void SetCollectAction(UnityAction _collectAction)
        {
            collectAction = _collectAction;
        }

        public void ReferenceTarget(SpawnableItem _target)
        {
            target = _target;
        }


        private void Update()
        {
            if (target != null)
            {
                float maxStep = speed * Time.deltaTime;

                //adjust Y-height for moving and checking
                Vector3 targetAdjustedTransform = target.transform.position;
                targetAdjustedTransform.y = transform.position.y;

                MoveToTarget(maxStep, targetAdjustedTransform);

                if (CheckIfCollected(maxStep, targetAdjustedTransform))
                {
                    CollectTarget();
                }
            }
        }

        void MoveToTarget(float maxStep, Vector3 targetAdjustedTransform)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetAdjustedTransform, maxStep);
        }

        bool CheckIfCollected(float maxStep, Vector3 targetAdjustedTransform)
        {
            return Vector3.Distance(transform.position, targetAdjustedTransform) <= maxStep;
        }

        void CollectTarget()
        {
            collectedSpawns++;

            SetVolume(false, target.CheckVolume());
            RepositionPlayer();

            Destroy(target.gameObject);

            collectAction?.Invoke();
        }

        void RepositionPlayer()
        {
            Vector3 curPos = transform.localPosition;
            curPos.y = r;
            transform.localPosition = curPos;
        }

        public int GetDissolvedNumber()
        {
            //+1 cuz THE Player is one of them too!
            return collectedSpawns + 1;
        }

        public float GetDissolvedVolume()
        {
            return CheckVolume() / GetDissolvedNumber();
        }

        public void Reset()
        {
            TurnColliderOnOff(false);
            SetVolume(true, GetDissolvedVolume());
            RepositionPlayer();
            collectedSpawns = 0;

            playerStartingCoro = StartCoroutine(PlayerWaitC());
        }

        IEnumerator PlayerWaitC()
        {
            yield return playerWaitingTime;
            TurnColliderOnOff(true);
        }

        public void TurnColliderOnOff(bool isOn)
        {
            col.enabled = isOn;
        }
    }
}
