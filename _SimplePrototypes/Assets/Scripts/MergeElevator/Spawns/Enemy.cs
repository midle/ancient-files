﻿using UnityEngine;
using UnityEngine.Events;

namespace MergeElevator
{
    [RequireComponent(typeof(Rigidbody))]
    public class Enemy : SpawnableBall
    {

        Rigidbody rb;
        [SerializeField] Collider col;

        UnityAction OnReset;


        private void Awake()
        {
            rb = GetComponent<Rigidbody>();
            col = GetComponent<Collider>();
        }

        public void Init(UnityAction _onReset)
        {
            OnReset = _onReset;
        }

        public void Reposition()
        {
            Vector3 curPos = transform.localPosition;
            curPos.y = r;
            transform.localPosition = curPos;
        }

        public void ApplyForce(Vector3 dir, float force)
        {
            rb.AddForce(dir * force);
        }

        private void OnTriggerEnter(Collider other)
        {
            Debug.Log("GameOver!");
            OnReset?.Invoke();
        }

    }
}