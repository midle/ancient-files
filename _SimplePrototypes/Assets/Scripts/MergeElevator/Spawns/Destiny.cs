﻿using UnityEngine;
using UnityEngine.Events;

namespace MergeElevator
{
    public class Destiny : MonoBehaviour
    {

        UnityAction OnPlayerEnter;

        public void Init(UnityAction _onPlayerEnter)
        {
            OnPlayerEnter = _onPlayerEnter;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                OnPlayerEnter?.Invoke();
            }
        }

    }
}