﻿using UnityEngine;

namespace MergeElevator
{
    public class SpawnableBall : SpawnableItem
    {

        protected float r;

        public override float CheckVolume()
        {
            return 4 * Mathf.PI * Mathf.Pow(r, 3) / 3;
        }

        public override void SetVolume(bool isInitial, float volume)
        {
            float newVolume = isInitial ? volume : CheckVolume() + volume;
            //it is sphere, so ...
            float newR = Mathf.Pow(3f / 4f * newVolume / Mathf.PI, 1f / 3f);
            r = newR;
            Vector3 newScale = new Vector3(r * 2, r * 2, r * 2);
            transform.localScale = newScale;
        }
    }
}