﻿using UnityEngine;

namespace MergeElevator
{
    public interface ISpawnable<T>
    {
        T Spawn(Vector3 pos);

        void SetParent(Transform parent);
        void SetVolume(bool isInitial, float volume);
    }

}
