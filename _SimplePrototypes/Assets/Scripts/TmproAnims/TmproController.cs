﻿using UnityEngine;
using TMPro;
using System.Collections;

namespace TmproAnims
{

    public enum TMProAnimations
    {
        CHANGE_COLORS, HANG
    }

    public struct VertexInfo
    {
        public float maxAngle, speed, currentAngle;
    }


    [RequireComponent(typeof(TextMeshProUGUI))]
    [DisallowMultipleComponent]
    public class TmproController : MonoBehaviour
    {

        [SerializeField] string customText;
        [SerializeField] TMProAnimations animStyle;
        [SerializeField] Vector2 speedMinMax, rangeMinMax;

        TextMeshProUGUI testText;

        Coroutine animationCoroutine;


        #region BaseInitConfig
        private void Awake()
        {
            testText = GetComponent<TextMeshProUGUI>();

            TryReplaceText();

            ConfigureText();
        }

        void TryReplaceText()
        {
            if (!string.IsNullOrEmpty(customText))
            {
                testText.text = customText;
            }
        }

        void ConfigureText()
        {
            testText.enableWordWrapping = true;
            testText.colorGradient = new VertexGradient(Color.white, Color.black, Color.yellow, Color.red);
            testText.enableVertexGradient = true;

            testText.ForceMeshUpdate(); // mesh before render!
        }


        void Start()
        {
            StartAnim();
        }


        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.C))
            {
                StartAnim();
            }
        }

        void StartAnim()
        {
            if (animationCoroutine != null)
            {
                StopCoroutine(animationCoroutine);
            }

            animationCoroutine = StartCoroutine(ChooseAnim());
        }

        IEnumerator ChooseAnim()
        {
            switch (animStyle)
            {
                case TMProAnimations.CHANGE_COLORS:
                    Debug.Log("AnimColors");
                    return AnimColors();
                case TMProAnimations.HANG:
                    Debug.Log("AnimHang");
                    return AnimHang();
                default:
                    return null;
            }
        }
        #endregion


        #region Methods
        IEnumerator AnimColors()
        {

            CanvasRenderer uiRenderer = testText.canvasRenderer;
            var textInfo = testText.textInfo;
            int charCount = textInfo.characterCount;
            int currentChar = 0;

            Color32[] newColors = textInfo.meshInfo[0].colors32;

            while (true)
            {
                for (int i = 0; i < charCount; i++)
                {
                    currentChar = (++currentChar) % charCount;
                    int vertexNo = textInfo.characterInfo[currentChar].vertexIndex;

                    newColors[vertexNo] = RandomizeColor();
                    newColors[vertexNo + 1] = RandomizeColor();
                    newColors[vertexNo + 2] = RandomizeColor();
                    newColors[vertexNo + 3] = RandomizeColor();

                    testText.mesh.vertices = textInfo.meshInfo[0].vertices;
                    testText.mesh.uv = textInfo.meshInfo[0].uvs0;
                    testText.mesh.uv2 = textInfo.meshInfo[0].uvs2;
                    testText.mesh.colors32 = textInfo.meshInfo[0].colors32;

                    uiRenderer.SetMesh(testText.mesh);
                }

                yield return null;
            }
        }

        Color32 RandomizeColor()
        {
            return new Color32((byte)Random.Range(0, 255), (byte)Random.Range(0, 255), (byte)Random.Range(0, 255), 127);
        }

        IEnumerator AnimHang()
        {
            CanvasRenderer uiRenderer = testText.canvasRenderer;
            Matrix4x4 matrix;
            Mesh mesh = testText.mesh;
            Vector3[] vertices = testText.mesh.vertices;
            int charCount = testText.textInfo.characterCount;

            VertexInfo[] vertexInfos = PredefineVertexInfo(charCount);

            while (true)
            {
                testText.ForceMeshUpdate();

                for (int i = 0; i < charCount; i++)
                {
                    VertexInfo vertexInfo = vertexInfos[i];
                    var charInfo = testText.textInfo.characterInfo[i];

                    int vertexNo = charInfo.vertexIndex;

                    Vector2 midTopPos = new Vector2((vertices[vertexNo].x + vertices[vertexNo + 2].x) / 2, charInfo.topLeft.y);
                    Vector3 offset = midTopPos;

                    //each char = 4 verts
                    vertices[vertexNo] -= offset;
                    vertices[vertexNo + 1] -= offset;
                    vertices[vertexNo + 2] -= offset;
                    vertices[vertexNo + 3] -= offset;

                    vertexInfo.currentAngle = Mathf.SmoothStep(-vertexInfo.maxAngle, vertexInfo.maxAngle, Mathf.PingPong(Time.frameCount * vertexInfo.speed, 1f));

                    matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, 0, vertexInfo.currentAngle), Vector3.one);

                    vertices[vertexNo] = matrix.MultiplyPoint3x4(vertices[vertexNo]);
                    vertices[vertexNo + 1] = matrix.MultiplyPoint3x4(vertices[vertexNo + 1]);
                    vertices[vertexNo + 2] = matrix.MultiplyPoint3x4(vertices[vertexNo + 2]);
                    vertices[vertexNo + 3] = matrix.MultiplyPoint3x4(vertices[vertexNo + 3]);

                    vertices[vertexNo] += offset;
                    vertices[vertexNo + 1] += offset;
                    vertices[vertexNo + 2] += offset;
                    vertices[vertexNo + 3] += offset;

                    vertexInfos[i] = vertexInfo;
                }

                mesh.vertices = vertices;

                uiRenderer.SetMesh(mesh);

                yield return null;
            }
        }

        VertexInfo[] PredefineVertexInfo(int vertices)
        {
            VertexInfo[] vertexInfos = new VertexInfo[vertices];

            for (int i = 0; i < vertices; i++)
            {
                vertexInfos[i].speed = Random.Range(speedMinMax.x, speedMinMax.y);
                vertexInfos[i].maxAngle = Random.Range(rangeMinMax.x, rangeMinMax.y);
            }

            return vertexInfos;
        }
        #endregion

    }

}
